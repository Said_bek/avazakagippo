<?php

namespace app\controllers;

use Yii;
use app\models\AboutUs;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * AboutUsController implements the CRUD actions for AboutUs model.
 */
class AboutUsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
            if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AboutUs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => AboutUs::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AboutUs model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AboutUs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AboutUs();

        if ($model->load(Yii::$app->request->post())) {
            $model->type = 'text';
            $model->imageFile = UploadedFile::getInstance($model, 'file_id');
            if ($model->imageFile) {
                $model->file_id = rand(1,100).strtotime(date('Y-m-d H:i:s')).'.'.$model->imageFile->extension;
                $model->type = 'photo';
            }

            if ($model->save()) {
                if ($model->imageFile) {
                    $model->imageFile->saveAs('uploads/'.$model->file_id);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AboutUs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->type = 'text';
            $model->imageFile = UploadedFile::getInstance($model, 'file_id');
            if ($model->imageFile) {
                $data = AboutUs::findOne($id);
                if (isset($data->file_id) and !empty($data->file_id)) {
                    unlink(dirname(__FILE__).'/../web/uploads/'.$data->file_id);
                }
                $model->file_id = rand(1,100).strtotime(date('Y-m-d H:i:s')).'.'.$model->imageFile->extension;
                $model->type = 'photo';
            }

            if ($model->save()) {
                if ($model->imageFile) {
                    $model->imageFile->saveAs('uploads/'.$model->file_id);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AboutUs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (isset($model->file_id) and !empty($model->file_id)) {
            unlink(dirname(__FILE__).'/../web/uploads/'.$model->file_id);
        }
        if ($model->delete()) {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the AboutUs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AboutUs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AboutUs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
