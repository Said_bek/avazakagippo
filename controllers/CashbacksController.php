<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Cashbacks;
use app\models\Clients;
use app\models\ClientCashback;
use app\models\CashbackControl;

/**
 * CashbacksController implements the CRUD actions for Cashbacks model.
 */
class CashbacksController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cashbacks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Cashbacks::find(),
        ]);
        $cashback_status = 0;
        $model = CashbackControl::find()->one();
        if (isset($model) && !empty($model)) {
            $cashback_status = $model->status;
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'cashback_status' => $cashback_status
        ]);
    }

    /**
     * Displays a single Cashbacks model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $sql = "select cl.full_name, cl.id from clients  as cl where id not in (select client_id from client_cashback as cc where cc.cashback_id = ".$id.")";
        // echo $sql;
        $clients = Clients::findBySql($sql)->all();
        $cashback_client = ClientCashback::find()->where("cashback_id = :cashback_id", [":cashback_id" => $id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'clients' => $clients,
            'id' => $id,
            'cashback_client' => $cashback_client
        ]);
    }

    /**
     * Creates a new Cashbacks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cashbacks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model,
        ]);


    }

    /**
     * Updates an existing Cashbacks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $cash_pr = $post["Cashbacks"]["cashback"];
            $sql_update = "UPDATE client_cashback SET cashback_parcent = ".$cash_pr." WHERE cashback_id = ".$id;
            $result = Yii::$app->db->createCommand($sql_update)->execute();
            if ($result) {
                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cashbacks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cashbacks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cashbacks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cashbacks::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCreateUser()
    {
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $cashback_id = $post["Cashback"];
            $has_cashback = Cashbacks::findOne($cashback_id);
            if (isset($has_cashback) && !empty($has_cashback)) {
                $cashback_pr = $has_cashback->cashback;
                $sql = "INSERT INTO client_cashback (cashback_id, client_id, cashback_parcent) VALUES "; 
                if (isset($post["All"])) {
                    $delete_sql = "DELETE FROM client_cashback WHERE cashback_id = ".$cashback_id;
                    $result = Yii::$app->db->createCommand($delete_sql)->execute();

                    $sql_values = "";
                    $clients = Clients::find()->all();
                    if (isset($has_cashback) && !empty($clients)) {
                        $i = 0;
                        foreach ($clients as $key => $value) {
                            if ($i == 0) {
                                $sql_values = $sql_values . "(".$cashback_id.",".$value["id"].",".$cashback_pr.")";
                            } else {
                                $sql_values = $sql_values . ", (".$cashback_id.",".$value["id"].",".$cashback_pr.")";
                            }
                            $i++;
                        }
                    }
                } else if (isset($post["client_id"])) {
                    $count_for = count($post["client_id"]);
                    $arr_client = $post["client_id"];
                    $sql_values = "";
                    for ($i = 0; $i < $count_for; $i++) { 
                        if ($i == 0) {
                            $sql_values = $sql_values . "(".$cashback_id.",".$arr_client[$i].",".$cashback_pr.")";
                        } else {
                            $sql_values = $sql_values . ", (".$cashback_id.",".$arr_client[$i].",".$cashback_pr.")";
                        }
                    } 
                }
                $sql = $sql.$sql_values;
                $result = Yii::$app->db->createCommand($sql)->execute();
                if ($result) {
                    return $this->redirect(["view","id" => $cashback_id]);
                }
            }
        }
    }

    public function actionDeleteCashback()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $delete_id = $_GET["data_id"];
            $client_cashbacks = ClientCashback::find()->where("id = :delete_id",[":delete_id" => $delete_id])->one();
            if (isset($client_cashbacks) && !empty($client_cashbacks)) {
                if ($client_cashbacks->delete()) {
                    return ["status" => "success"];
                }
            }
        }
    }
    public function actionOnCashback()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = CashbackControl::find()->one();
            if (isset($model) && !empty($model)) {
                $model->status = 1;
                if ($model->save()) {
                    return ["status" => "success"];
                }
            }        
        }
    }
    public function actionOffCashback()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = CashbackControl::find()->one();
            if (isset($model) && !empty($model)) {
                $model->status = 0;
                if ($model->save()) {
                    return ["status" => "success"];
                }
            }        
        }
    }
}
