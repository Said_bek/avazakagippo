-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Окт 20 2021 г., 19:05
-- Версия сервера: 10.3.31-MariaDB-cll-lve
-- Версия PHP: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `gippobab_shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `title_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_uz` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `about_us`
--

INSERT INTO `about_us` (`id`, `title_uz`, `title_ru`, `file_id`, `description_uz`, `description_ru`, `type`) VALUES
(1, 'GIPPO kompaniyasi – tabiiy bolalar bo\'tqasining milliy ishlab chiqaruvchisi', 'Без ГМО, без растительного и пальмового масла, без консервантов', '711633661940.jpg', '', '', 'photo');

-- --------------------------------------------------------

--
-- Структура таблицы `arrive_orders`
--

CREATE TABLE `arrive_orders` (
  `id` int(11) NOT NULL,
  `order_code_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sell_price` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `order_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `arrive_orders`
--

INSERT INTO `arrive_orders` (`id`, `order_code_id`, `product_id`, `sell_price`, `count`, `created_date`, `order_id`) VALUES
(1, 100000, 1, 30000, 1, '2021-09-06 12:41:53', 39),
(2, 100000, 1, 150000, 5, '2021-09-06 12:41:53', 40),
(3, 100000, 1, 420000, 14, '2021-09-06 12:41:53', 44),
(4, 100000, 1, 30000, 1, '2021-09-06 12:41:53', 45),
(5, 100001, 1, 300000, 10, '2021-09-06 14:35:12', 46),
(6, 100002, 1, 180000, 6, '2021-09-07 10:17:01', 48),
(7, 100003, 1, 1000, 1, '2021-09-07 10:18:02', 49),
(8, 100003, 1, 2000, 2, '2021-09-07 10:18:02', 50),
(9, 100004, 1, 2000, 2, '2021-09-07 10:24:00', 51),
(10, 100004, 1, 1000, 1, '2021-09-07 10:24:00', 52),
(11, 100004, 1, 1000, 1, '2021-09-07 10:24:00', 53),
(12, 100004, 1, 1000, 1, '2021-09-07 10:24:00', 54),
(13, 100005, 1, 5000, 5, '2021-09-07 10:27:01', 55),
(14, 100006, 3, 5000, 1, '2021-09-07 11:09:16', 58),
(15, 100007, 3, 5000, 1, '2021-09-07 11:24:59', 60),
(16, 100008, 3, 5000, 1, '2021-09-07 11:47:31', 61),
(17, 100009, 3, 5000, 1, '2021-09-20 10:19:14', 63),
(18, 100010, 3, 5000, 1, '2021-09-27 04:53:37', 64),
(19, 100011, 3, 25000, 5, '2021-09-27 05:16:17', 68),
(20, 100011, 4, 490000, 7, '2021-09-27 05:16:17', 69),
(21, 100012, 3, 40000, 8, '2021-09-27 05:18:38', 70),
(22, 100013, 3, 5000, 1, '2021-09-27 05:21:11', 71),
(23, 100014, 13, 119000, 7, '2021-10-04 03:10:02', 86),
(24, 100014, 13, 238000, 14, '2021-10-04 03:10:02', 87),
(25, 100015, 13, 85000, 5, '2021-10-07 04:12:11', 93),
(26, 100016, 13, 238000, 14, '2021-10-07 04:18:14', 94),
(27, 100017, 13, 136000, 8, '2021-10-09 04:28:13', 101),
(28, 100017, 12, 34000, 2, '2021-10-09 04:28:13', 102),
(29, 100018, 6, 17000, 1, '2021-10-09 13:24:22', 105),
(31, 100019, 13, 85000, 5, '2021-10-14 04:08:19', 107);

-- --------------------------------------------------------

--
-- Структура таблицы `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `title_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_uz` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `enter_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `carusel`
--

CREATE TABLE `carusel` (
  `id` int(11) NOT NULL,
  `title_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_uz` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `type` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cashbacks`
--

CREATE TABLE `cashbacks` (
  `id` int(10) NOT NULL,
  `cashback` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `cashbacks`
--

INSERT INTO `cashbacks` (`id`, `cashback`) VALUES
(1, 10),
(5, 12),
(6, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `cashback_control`
--

CREATE TABLE `cashback_control` (
  `id` int(10) NOT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `cashback_control`
--

INSERT INTO `cashback_control` (`id`, `status`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `step` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `imgs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `title_uz`, `title_ru`, `status`, `step`, `category_id`, `imgs`, `title_en`) VALUES
(1, 'Kategoriya 1', 'Kategoriya 1 ru', 2, NULL, NULL, NULL, NULL),
(2, 'sadasd', 'asdasdsa', 2, NULL, NULL, NULL, NULL),
(3, 'Bolalar bo\'tqasi', 'Детские каши', 1, NULL, NULL, NULL, NULL),
(4, 'Pampers', 'Pampers', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `click_transactions`
--

CREATE TABLE `click_transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `click_trans_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `sign_time` int(11) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `chat_id` int(11) DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tg_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tg_username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms_code` int(1) DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `referal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referal_tgbot` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referal_ball` int(11) DEFAULT NULL,
  `referal_status` int(11) DEFAULT NULL,
  `referal_id` int(10) DEFAULT NULL,
  `gender` int(1) DEFAULT NULL,
  `old` int(3) DEFAULT NULL,
  `cashback_sum` int(10) DEFAULT 0,
  `is_send` int(4) DEFAULT NULL,
  `birth_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `chat_id`, `phone_number`, `tg_name`, `tg_username`, `password`, `sms_code`, `full_name`, `status`, `referal`, `referal_tgbot`, `referal_ball`, `referal_status`, `referal_id`, `gender`, `old`, `cashback_sum`, `is_send`, `birth_date`) VALUES
(1, 1033542488, 'OTk4OTk4Nzg1OTA2', 'QWJkdWwtQXppeg==', 'QmFkYWxvdl9h', NULL, NULL, 'QWJkdWwtQXppeg==', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2000, 0, NULL),
(2, 284914591, 'Kzk5ODk5Nzc3MjIxMQ==', 'U2FpZGJlaw==', 'U1NTYWlkYmVr', NULL, NULL, 'c2FkYXNk', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(3, 700341659, 'OTk4OTkwMDAxNjc2', '2LnYqNivINin2YTYutmB2ZHYp9ix', 'YWJkdWthcmltb3ZfOTk=', NULL, NULL, 'QWJkdWdhZmZvcg==', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(4, 434795664, 'OTk4OTk4MzAzMzMw', 'QmVoem9k', 'QmVoem9kX01ha2htdWRvdmljaA==', NULL, NULL, 'QmVoem9kIE1vdmxhbm92', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(5, 28200456, 'OTk4OTA5MzI2NjMz', '0KHQsNC50LTQuNC60L7Qsg==', 'RXhjZWxsZW50RGFuaWs=', NULL, NULL, '0KHQsNC50LTQuNC60L7QsiDQlNCw0L3QuNC10LvRjA==', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(6, 112391617, 'OTk4OTM1MDA5OTg4', '0K3Qu9GR0YAg0JDQu9C40LzQvtCy', 'YWxpbW92ZWx5b3I=', NULL, NULL, '0K3Qu9GR0YAg0JDQu9C40LzQvtCy', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(7, 1900834951, 'OTk4OTM1MDEyMDEx', 'UmF2c2hhbg==', 'cmF2c2hhbmJlazIz', NULL, NULL, 'UmF2c2hhbg==', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(8, 398187848, 'OTk4OTQ2OTU5OTE3', '0JvQsNC30LjQtw==', 'dGhlbGF6aXo=', NULL, NULL, 'PGI+', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(9, 1487971938, 'Kzk5ODU1NTU1NTU1NQ==', 'SXNsb21pZGRpbg==', 'VXJpbmJvZXZfSQ==', NULL, NULL, 'cXFxcXFxcXFxcXFxcQ==', 1, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
(10, 782542086, 'OTk4OTk3MjczNjM3', 'SXNsb21pZGRpbg==', 'SXNsb21pZGRpbl91emI=', NULL, NULL, 'SXNsb21pZGRpbg==', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(11, 299716746, 'OTk4OTM1MDUxNTIw', 'U2FuamFy', 'U2FuamFyNTA1MTUyMA==', NULL, NULL, 'Tml5YXpvdiBzYW5uYXI=', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(12, 180114547, NULL, '0prQsNGA0LDSm9Cw0LvQv9Cw0ps=', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(13, 924296764, NULL, 'L011aGFtbWFkaW1yb24gX1hvbmRhbWlyX011aGFtbWFkc2FpZG9saW14b24v', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(14, 936662497, 'OTk4OTk3MjA0MDEy', 'T2xpbUpvaG4=', 'T2FraG1hZGpvbm92', NULL, NULL, 'YXNk', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(15, -760453873, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `clients_address`
--

CREATE TABLE `clients_address` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `region_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `clients_wishlist`
--

CREATE TABLE `clients_wishlist` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `client_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `client_cashback`
--

CREATE TABLE `client_cashback` (
  `id` int(10) NOT NULL,
  `cashback_id` int(10) DEFAULT NULL,
  `client_id` int(10) DEFAULT NULL,
  `cashback_parcent` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `client_cashback`
--

INSERT INTO `client_cashback` (`id`, `cashback_id`, `client_id`, `cashback_parcent`) VALUES
(2, 4, 2, 12),
(3, 1, 1, 10),
(4, 1, 2, 10),
(6, 5, 1, 12),
(7, 5, 2, 12),
(8, 5, 3, 12),
(9, 5, 4, 12),
(10, 5, 5, 12),
(11, 5, 6, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `client_cashbacks`
--

CREATE TABLE `client_cashbacks` (
  `id` int(10) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `cashback_parcent` int(10) DEFAULT NULL,
  `cashback_summ` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `client_order_camments`
--

CREATE TABLE `client_order_camments` (
  `id` int(10) NOT NULL,
  `order_code_id` int(20) DEFAULT NULL,
  `client_id` int(10) DEFAULT NULL,
  `text_camment` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `client_order_camments`
--

INSERT INTO `client_order_camments` (`id`, `order_code_id`, `client_id`, `text_camment`) VALUES
(3, 5, 1, 'Y2hvdGtpIGNob3RraSBjaG90a2k=');

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `chat_id` int(11) DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_uz` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `currency_price`
--

CREATE TABLE `currency_price` (
  `id` int(11) NOT NULL,
  `sum` int(11) DEFAULT NULL,
  `dollar` double DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `details`
--

CREATE TABLE `details` (
  `id` int(11) NOT NULL,
  `title_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `details`
--

INSERT INTO `details` (`id`, `title_uz`, `title_ru`, `status`, `type`, `title_en`) VALUES
(1, 'Brand', 'Brand', 1, 2, NULL),
(2, 'Rang', 'Rang', 1, 3, NULL),
(3, 'O`lchami', 'Размер', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `last_id`
--

CREATE TABLE `last_id` (
  `id` int(10) NOT NULL,
  `chat_id` int(20) NOT NULL,
  `last_id` int(10) DEFAULT NULL,
  `region_id` int(10) DEFAULT NULL,
  `lang` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `last_id`
--

INSERT INTO `last_id` (`id`, `chat_id`, `last_id`, `region_id`, `lang`, `lat`) VALUES
(1, 1033542488, 5, 1, '69.277062', '41.310748'),
(2, 284914591, 0, 1, '69.276886', '41.31065'),
(3, 700341659, 0, NULL, NULL, NULL),
(4, 434795664, 0, NULL, NULL, NULL),
(5, 28200456, 0, NULL, NULL, NULL),
(6, 112391617, 0, NULL, NULL, NULL),
(7, 1900834951, 0, 1, NULL, NULL),
(8, 398187848, 0, NULL, NULL, NULL),
(9, 1487971938, 0, 6, '69.276773', '41.310617'),
(10, 782542086, 0, NULL, NULL, NULL),
(11, 299716746, 0, NULL, NULL, NULL),
(12, 180114547, 0, NULL, NULL, NULL),
(13, 924296764, 0, NULL, NULL, NULL),
(14, 936662497, 0, NULL, NULL, NULL),
(15, -760453873, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `log_messages`
--

CREATE TABLE `log_messages` (
  `id` int(11) NOT NULL,
  `message_id` int(11) DEFAULT NULL,
  `chat_id` int(10) DEFAULT NULL,
  `log_text` tinytext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `log_messages`
--

INSERT INTO `log_messages` (`id`, `message_id`, `chat_id`, `log_text`) VALUES
(58, 3381, 700341659, ''),
(59, 3382, 700341659, ''),
(131, 3628, 434795664, ''),
(181, 3796, 28200456, ''),
(487, 4418, 112391617, ''),
(488, 4419, 112391617, ''),
(584, 4637, 299716746, ''),
(585, 4638, 299716746, ''),
(586, 4639, 299716746, ''),
(587, 4640, 299716746, ''),
(588, 4641, 299716746, ''),
(589, 4642, 299716746, ''),
(590, 4643, 299716746, ''),
(591, 4644, 299716746, ''),
(592, 4645, 299716746, ''),
(593, 4646, 299716746, ''),
(986, 5361, 1900834951, ''),
(987, 5362, 1900834951, ''),
(988, 5365, 1900834951, ''),
(989, 5366, 1900834951, ''),
(1087, 5565, 180114547, ''),
(1088, 5570, 924296764, ''),
(1217, 5819, 1487971938, ''),
(1249, 5903, 936662497, ''),
(1250, 0, -760453873, '');

-- --------------------------------------------------------

--
-- Структура таблицы `multi_details`
--

CREATE TABLE `multi_details` (
  `id` int(11) NOT NULL,
  `title_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `details_id` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `color_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `multi_details`
--

INSERT INTO `multi_details` (`id`, `title_uz`, `title_ru`, `details_id`, `status`, `color_name`, `title_en`) VALUES
(1, 'lacost', 'lacoste', 1, 1, NULL, NULL),
(2, 'gucci', 'gucci', 1, 1, NULL, NULL),
(3, 'oq', 'oq', 2, 1, NULL, NULL),
(4, 'qora', 'qora', 2, 1, NULL, NULL),
(5, 'yashil', 'yashil', 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_uz` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title_uz`, `title_ru`, `file_id`, `description_uz`, `description_ru`, `type`, `status`) VALUES
(1, 'yenglikni biri', 'yenglikni biri', '101630936126.jpg', 'asd', 'asd', 'photo', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `chat_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT 0,
  `summ` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(1) DEFAULT NULL,
  `status_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `chat_id`, `product_id`, `count`, `summ`, `create_date`, `status`, `status_description`, `client_id`) VALUES
(59, 434795664, 3, 1, NULL, '2021-09-07 11:24:35', 2, NULL, NULL),
(75, 398187848, 4, 5, NULL, '2021-09-27 05:56:49', 3, NULL, NULL),
(76, 398187848, 3, 8, NULL, '2021-09-27 06:02:06', 3, NULL, NULL),
(77, 398187848, 3, 9, NULL, '2021-09-27 06:17:19', 3, NULL, NULL),
(78, 1900834951, 9, 2, NULL, '2021-09-27 17:51:05', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `order_address`
--

CREATE TABLE `order_address` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `longs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chat_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `order_code`
--

CREATE TABLE `order_code` (
  `id` int(11) NOT NULL,
  `order_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `stage` int(1) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `order_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `address_id` int(11) DEFAULT NULL,
  `delivery_price` int(11) DEFAULT NULL,
  `delivery` int(1) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay` int(11) DEFAULT NULL,
  `billing_status` int(1) DEFAULT NULL,
  `bot` int(1) DEFAULT NULL,
  `lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_id` int(10) DEFAULT NULL,
  `type` int(10) DEFAULT NULL,
  `file_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `order_code`
--

INSERT INTO `order_code` (`id`, `order_code`, `created_date`, `stage`, `client_id`, `order_date`, `address_id`, `delivery_price`, `delivery`, `description`, `description_ru`, `pay`, `billing_status`, `bot`, `lat`, `lang`, `region_id`, `type`, `file_id`) VALUES
(1, '100000', '2021-09-06 13:41:58', 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 1, 8, NULL, '41.310748', '69.277062', 1, NULL, NULL),
(5, '100004', '2021-09-07 11:05:15', 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 1, 8, NULL, '41.310748', '69.277062', 1, NULL, NULL),
(7, '100006', '2021-09-27 18:00:19', 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 1, 8, NULL, '41.310748', '69.277062', 1, NULL, NULL),
(8, '100007', '2021-09-27 18:00:46', 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 1, 5, NULL, '41.310748', '69.277062', 11, NULL, NULL),
(9, '100008', '2021-09-07 11:49:20', 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 1, 4, NULL, '41.310748', '69.277062', 11, NULL, NULL),
(10, '100009', '2021-09-20 10:19:20', 1, 2, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '41.259589', '69.223834', 1, NULL, NULL),
(11, '100010', '2021-09-27 04:53:43', 1, 9, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '41.310617', '69.276773', 7, NULL, NULL),
(12, '100011', '2021-09-27 05:16:47', 1, 9, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '41.310617', '69.276773', 1, NULL, NULL),
(13, '100012', '2021-09-27 05:18:40', 1, 9, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, '41.310617', '69.276773', 1, NULL, NULL),
(14, '100013', '2021-09-27 05:21:12', 1, 9, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, '41.310617', '69.276773', 1, NULL, NULL),
(15, '100014', '2021-10-04 03:10:02', 1, 9, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '41.310617', '69.276773', 1, NULL, NULL),
(16, '100015', '2021-10-07 04:12:11', 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '41.310182', '69.276605', 5, NULL, NULL),
(17, '100016', '2021-10-07 04:18:14', 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '41.310174', '69.276577', 5, NULL, NULL),
(18, '100017', '2021-10-09 04:28:15', 1, 9, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, '41.310617', '69.276773', 6, NULL, NULL),
(19, '100018', '2021-10-09 13:24:22', 1, 2, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '41.31065', '69.276886', 11, NULL, NULL),
(21, '100019', '2021-10-14 04:09:21', 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, '41.310748', '69.277062', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `details_id` int(11) DEFAULT NULL,
  `multi_details_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `title_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_uz` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `main_category` int(11) DEFAULT NULL,
  `sell_count` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT 0,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `made` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `dollar` double DEFAULT NULL,
  `sold_product` int(11) DEFAULT 0,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_en` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `made_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `made_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sku_api` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `sub_category_id`, `title_uz`, `title_ru`, `price`, `img`, `description_uz`, `description_ru`, `status`, `count`, `type`, `main_category`, `sell_count`, `view`, `sku`, `made`, `quantity`, `dollar`, `sold_product`, `title_en`, `link`, `description_en`, `made_uz`, `made_ru`, `sku_api`) VALUES
(3, 6, 'Guruchli sutsiz bo‘tqa', 'Рисовая безмолочная гипоаллергенная каша', 17000, '171632745355.jpg', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 6, 'Grechkali sutsiz bo‘tqa', 'Безмолочная гречневая гипоаллергенная каша', 17000, '661632745443.jpg', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, 'Olma qo‘shilgan 3 boshoq multiboshoqli bo‘tqa', 'Мультизлаковая 3 злаков с яблоком', 17000, '681632763595.jpg', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 6, 'Olma va banan qo‘shilgan 5 boshoq multiboshoqli sutli bo‘tqa', 'Мультизлаковая 5 злаков с яблоком и бананом', 17000, '251632763756.jpg', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 6, '5 boshoq multiboshoqli sutli bo‘tqa', 'Мультизлаковая  молочная каша', 17000, '771632763797.jpg', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 6, 'Guruchli sutli bo‘tqa', 'Рисовая молочная каша', 17000, '961632766184.png', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 6, 'Olma qo‘shilgan guruchli sutli bo‘tqa', 'Рисовая молочная каша с яблоком', 17000, '501632763910.jpg', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 6, 'Jo‘xorili sutli bo‘tqa', 'Кукурузная молочная каша', 17000, '591632763951.jpg', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 6, 'Olma qo‘shilgan sulili sutli bo‘tqa', 'Овсяная молочная каша с яблоком', 17000, '301632763992.jpg', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 6, 'Bug‘doyli sutli bo‘tqa', 'Пшеничная молочная каша', 17000, '31632764038.jpg', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 6, 'Grechkali sutli bo‘tqa', 'Гречневая молочная каша', 17000, '351632766428.png', '', '', 1, 1000, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `product_details_`
--

CREATE TABLE `product_details_` (
  `id` int(10) NOT NULL,
  `product_id` int(10) DEFAULT NULL,
  `details_id` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `multi_details_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product_details_code`
--

CREATE TABLE `product_details_code` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `multi_detail_id` int(11) DEFAULT NULL,
  `detail_id` int(11) DEFAULT NULL,
  `detail_code` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_details_code`
--

INSERT INTO `product_details_code` (`id`, `product_id`, `multi_detail_id`, `detail_id`, `detail_code`) VALUES
(1, 1, 3, 2, 1),
(2, 1, 4, 2, 1),
(3, 1, 5, 2, 1),
(4, 1, 1, 1, 1),
(5, 1, 3, 2, 2),
(6, 1, 4, 2, 2),
(7, 1, 5, 2, 2),
(8, 1, 2, 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `product_img`
--

CREATE TABLE `product_img` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `multi_detail_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_img`
--

INSERT INTO `product_img` (`id`, `product_id`, `img`, `status`, `multi_detail_id`) VALUES
(5, 3, '551632745332.jpg', 1, NULL),
(6, 4, '531632745461.jpg', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `product_sale`
--

CREATE TABLE `product_sale` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sale_price` double DEFAULT NULL,
  `enter_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sale_pracent` double DEFAULT NULL,
  `sale_dollar` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `title_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `region`
--

INSERT INTO `region` (`id`, `title_uz`, `title_ru`, `price`) VALUES
(1, 'Toshkent shahri', 'г.Ташкент', 20000),
(2, 'Toshkent vil.', 'Тошкент обл.', 20000),
(3, 'Jizzax', 'Джизакская обл.', 20000),
(4, 'Sirdaryo', 'Сырдарьинская обл.', 20000),
(5, 'Namangan', 'Наманганская обл.', 20000),
(6, 'Andijon', 'Андижанская обл.', 20000),
(7, 'Farg\'ona', 'Ферганская обл.', 20000),
(8, 'Navoiy', 'Навоийская обл.', 20000),
(9, 'Samarqand', 'Самаркандская обл.', 20000),
(10, 'Buxoro', 'Бухарская обл.', 20000),
(11, 'Qashqadaryo', 'Кашкадарьинская обл.', 20000),
(12, 'Surxondaryo', 'Сурхандарьинская обл.', 20000),
(13, 'Xorazm', 'Хорезмская обл.', 20000),
(14, 'Qoraqalpog\'iston', 'Каракалпакстан', 20000);

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_post` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `star` int(1) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(1) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `step`
--

CREATE TABLE `step` (
  `id` int(11) NOT NULL,
  `chat_id` int(11) DEFAULT NULL,
  `step_1` int(11) DEFAULT NULL,
  `step_2` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `step`
--

INSERT INTO `step` (`id`, `chat_id`, `step_1`, `step_2`) VALUES
(1, 1033542488, 1, 2),
(2, 284914591, 1, 2),
(3, 700341659, 1, 2),
(4, 434795664, 1, 7),
(5, 28200456, 2, 5),
(6, 112391617, 1, 2),
(7, 1900834951, 2, 2),
(8, 398187848, 1, 20),
(9, 1487971938, 1, 20),
(10, 782542086, 2, 20),
(11, 299716746, 2, 5),
(12, 180114547, 1, 0),
(13, 924296764, 1, 0),
(14, 936662497, 1, 2),
(15, -760453873, 2, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(10) NOT NULL,
  `category_id` int(10) DEFAULT NULL,
  `title_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `step` int(10) DEFAULT NULL,
  `sub_category_id` int(10) DEFAULT NULL,
  `type` int(10) DEFAULT NULL,
  `main_category` int(10) DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `title_uz`, `title_ru`, `img`, `status`, `step`, `sub_category_id`, `type`, `main_category`, `title_en`) VALUES
(1, 1, 'Sub category 1', 'sub_category 1', '351630938698.jpg', 2, NULL, NULL, 1, 1, NULL),
(2, 1, 'sub_category 2', 'sub_category 2', NULL, 2, NULL, NULL, 1, 1, NULL),
(3, 1, 'sub_category 3', 'sub_category 3 ru', NULL, 2, NULL, NULL, 1, 1, NULL),
(4, 2, 'qqqqq', 'qqqqq', NULL, 2, NULL, NULL, 1, 2, NULL),
(5, 2, '234ed3222', '3ed23d3', NULL, 2, NULL, NULL, 1, 2, NULL),
(6, 3, 'GIPPO ', 'Гиппо', NULL, 1, NULL, NULL, 1, 3, NULL),
(7, 3, 'Grechkali sutsiz bo\'tqa', 'Безмолочная гречневая гипоаллергенная каша', NULL, 2, NULL, NULL, 1, 3, NULL),
(8, 4, '1', '1', NULL, 1, NULL, NULL, 1, 4, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `arrive_orders`
--
ALTER TABLE `arrive_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `carusel`
--
ALTER TABLE `carusel`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cashbacks`
--
ALTER TABLE `cashbacks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cashback_control`
--
ALTER TABLE `cashback_control`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `click_transactions`
--
ALTER TABLE `click_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `clients_address`
--
ALTER TABLE `clients_address`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `clients_wishlist`
--
ALTER TABLE `clients_wishlist`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `client_cashback`
--
ALTER TABLE `client_cashback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `client_cashbacks`
--
ALTER TABLE `client_cashbacks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `client_order_camments`
--
ALTER TABLE `client_order_camments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currency_price`
--
ALTER TABLE `currency_price`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `last_id`
--
ALTER TABLE `last_id`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `log_messages`
--
ALTER TABLE `log_messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `multi_details`
--
ALTER TABLE `multi_details`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_address`
--
ALTER TABLE `order_address`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_code`
--
ALTER TABLE `order_code`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_details_`
--
ALTER TABLE `product_details_`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_details_code`
--
ALTER TABLE `product_details_code`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_img`
--
ALTER TABLE `product_img`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_sale`
--
ALTER TABLE `product_sale`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `step`
--
ALTER TABLE `step`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `arrive_orders`
--
ALTER TABLE `arrive_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `carusel`
--
ALTER TABLE `carusel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cashbacks`
--
ALTER TABLE `cashbacks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `cashback_control`
--
ALTER TABLE `cashback_control`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `click_transactions`
--
ALTER TABLE `click_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `clients_address`
--
ALTER TABLE `clients_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `clients_wishlist`
--
ALTER TABLE `clients_wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `client_cashback`
--
ALTER TABLE `client_cashback`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `client_cashbacks`
--
ALTER TABLE `client_cashbacks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `client_order_camments`
--
ALTER TABLE `client_order_camments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currency_price`
--
ALTER TABLE `currency_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `details`
--
ALTER TABLE `details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `last_id`
--
ALTER TABLE `last_id`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `log_messages`
--
ALTER TABLE `log_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1251;

--
-- AUTO_INCREMENT для таблицы `multi_details`
--
ALTER TABLE `multi_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT для таблицы `order_address`
--
ALTER TABLE `order_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `order_code`
--
ALTER TABLE `order_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `product_details_`
--
ALTER TABLE `product_details_`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `product_details_code`
--
ALTER TABLE `product_details_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `product_img`
--
ALTER TABLE `product_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `product_sale`
--
ALTER TABLE `product_sale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `step`
--
ALTER TABLE `step`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
