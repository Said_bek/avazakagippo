<?php

use yii\db\Migration;

/**
 * Class m210622_065722_tables
 */
class m210622_065722_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('about_us', [
            'id' => $this->primaryKey(),
            'title_uz' => $this->string(),
            'title_ru' => $this->string(),
            'file_id' => $this->string(),
            'description_uz' => $this->text(),
            'description_ru' => $this->text(),
            'type' => $this->string(),
        ]);
        // CREATE TABLE `about_us` (
        //   `id` int(10) NOT NULL,
        //   `title_uz` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `title_ru` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `file_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `description_uz` text COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $sql_insert = "
            INSERT INTO about_us (title_uz, title_ru, file_id, description_uz, description_ru, type) 
                VALUES
            ('Bizning Kampaniya', 'Наша Кампания', 'https://image.shutterstock.com/image-illustration/about-us-text-on-blackboard-600w-729406153.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'photo'),
            ('Bizning hodimlar', 'Наш персонал', 'https://previews.123rf.com/images/nd3000/nd30001906/nd3000190600416/124407262-successful-company-with-happy-workers-in-office.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'photo');";
        $this->execute($sql_insert);

        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'title_uz' => $this->string(),
            'title_ru' => $this->string(),
            'status' => $this->smallInteger()
        ]);

        // CREATE TABLE `category` (
        //   `id` int(11) NOT NULL,
        //   `title_uz` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `title_ru` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `status` int(1) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $sql_insert = "
            INSERT INTO category (title_uz, title_ru, status) VALUES
            ('Kiyimlar', 'Одежда', 1),
            ('Bosh kiyimlar', 'Шляпы', 1),
            ('Futbolkalar', 'Футболки', 1),
            ('Kastyumlar', 'Костюмы', 1),
            ('Shimlar', 'Штаны', 1),
            ('Ko`z oynaklar', 'Oчки', 1),
            ('Ko`ylaklar', 'Платья', 1);";
        $this->execute($sql_insert);

        $this->createTable('click_transactions', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'click_trans_id' => $this->integer(),
            'service_id' => $this->integer(),
            'status' => $this->integer(),
            'sign_time' => $this->integer(),
            'create_time' => $this->integer()
        ]);
        // CREATE TABLE `click_transactions` (
        //   `id` int(10) NOT NULL,
        //   `user_id` int(11) DEFAULT NULL,
        //   `click_trans_id` int(11) DEFAULT NULL,
        //   `amount` int(11) DEFAULT NULL,
        //   `click_paydoc_id` int(11) DEFAULT NULL,
        //   `service_id` int(11) DEFAULT NULL,
        //   `status` int(11) DEFAULT NULL,
        //   `sign_time` int(11) DEFAULT NULL,
        //   `create_time` int(11) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(),
            'phone_number' => $this->string(),
            'tg_name' => $this->string(),
            'tg_username' => $this->string(),
            'password' => $this->string(),
            'sms_code' => $this->smallInteger(),
            'full_name' => $this->string(),
            'status' => $this->smallInteger(),
        ]);
        // CREATE TABLE `clients` (
        //   `id` int(11) NOT NULL,
        //   `chat_id` int(20) DEFAULT NULL,
        //   `phone_number` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `tg_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `tg_username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `password` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `sms_code` int(5) DEFAULT NULL,
        //   `full_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `status` int(1) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $this->createTable('contact', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(),
            'phone_number' => $this->string(),
            'phone_number1' => $this->string(),
            'phone_number2' => $this->string(),
            'phone_number3' => $this->string(),
            'email' => $this->string(),
            'description_uz' => $this->text(),
            'description_ru' => $this->text(),
        ]);
        // CREATE TABLE `contact` (
        //   `id` int(10) NOT NULL,
        //   `phone_number` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
        //   `phone_number1` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `phone_number2` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `phone_number3` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `description_uz` text COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $sql_insert = "
        INSERT INTO contact ( phone_number, phone_number1, phone_number2, phone_number3, email, description_uz, description_ru) VALUES
        ('+998712021202', '+998712021203', '+998712021204', NULL, 'badalov@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
        ";
        $this->execute($sql_insert);


        $this->createTable('details', [
            'id' => $this->primaryKey(),
            'title_uz' => $this->string(),
            'title_ru' => $this->string(),
            'status' => $this->smallInteger(),
        ]);

        // CREATE TABLE `details` (
        //   `id` int(10) NOT NULL,
        //   `title_uz` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `title_ru` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `status` int(1) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


        $sql_insert = "
            INSERT INTO details (title_uz, title_ru, status) VALUES
            ('Rangi', 'Цвет', 1),
            ('O`lchami', 'Размеры', 1),
            ('Brend', 'Бренд', 1),
            ('Yil', 'Год', 1);
        ";
        $this->execute($sql_insert);
        

        // CREATE TABLE `last_id` (
        //   `id` int(11) NOT NULL,
        //   `chat_id` int(20) DEFAULT NULL,
        //   `last_id` int(10) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $this->createTable('log_messages', [
            'id' => $this->primaryKey(),
            'message_id' => $this->integer(),
            'chat_id' => $this->integer()
        ]);
        // CREATE TABLE `log_messages` (
        //   `id` int(11) NOT NULL,
        //   `message_id` int(20) DEFAULT NULL,
        //   `chat_id` int(20) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $this->createTable('multi_details', [
            'id' => $this->primaryKey(),
            'title_uz' => $this->string(),
            'title_ru' => $this->string(),
            'details_id' => $this->integer(),
            'status' => $this->smallInteger()
        ]);

        // CREATE TABLE `multi_details` (
        //   `id` int(10) NOT NULL,
        //   `title_uz` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `title_ru` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `details_id` int(10) DEFAULT NULL,
        //   `status` int(1) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $sql_insert = "
           INSERT INTO multi_details (title_uz, title_ru, details_id, status) VALUES
            ('Oq', 'Белый', 1, 1),
            ('Qora', 'Черный', 1, 1),
            ('Pushti', 'Розовый', 1, 1),
            ('Qizil', 'красный', 1, 1),
            ('Kulrang', 'Серый', 1, 1),
            ('XL', 'XL', 2, 1),
            ('L', 'L', 2, 1),
            ('S', 'S', 2, 1),
            ('K', 'L', 2, 1),
            ('M', 'M', 2, 1),
            ('lacoste', 'lacoste', 3, 1),
            ('Nike', 'Nike', 3, 1),
            ('Puma', 'Puma', 3, 1),
            ('Adidas', 'Adidas', 3, 1),
            ('Asics', 'Asics', 3, 1),
            ('2020', '2020', 5, 1),
            ('2014', '2014', 5, 1),
            ('2016', '2016', 5, 1),
            ('2012', '2012', 5, 1),
            ('2010', '2010', 5, 1),
            ('Sariq', 'Желтый', 1, 1),
            ('1999', '1999', 5, 1),
            ('2000', '2000', 5, 1);
        ";
        $this->execute($sql_insert);
        

        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(),
            'product_id' => $this->integer(),
            'count' => $this->integer(),
            'summ' => $this->integer(),
            'create_date' => $this->timestamp(),
            'status' => $this->smallInteger(),
            'status_description' => $this->string(),
            'client_id' => $this->integer(),
        ]);

        // CREATE TABLE `orders` (
        //   `id` int(11) NOT NULL,
        //   `chat_id` int(10) DEFAULT NULL,
        //   `product_id` int(10) DEFAULT NULL,
        //   `count` int(11) DEFAULT 0,
        //   `summ` int(11) DEFAULT NULL,
        //   `create_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
        //   `status` int(1) DEFAULT NULL,
        //   `status_description` int(11) DEFAULT NULL,
        //   `payment_type` int(1) DEFAULT NULL,
        //   `client_id` int(10) DEFAULT NULL,
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $this->createTable('order_address', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'longs' => $this->string(),
            'lat' => $this->string(),
            'address' => $this->string(),
            'chat_id' => $this->integer()
        ]);

        // CREATE TABLE `order_address` (
        //   `id` int(10) NOT NULL,
        //   `order_id` int(10) DEFAULT NULL,
        //   `longs` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `lat` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `chat_id` int(20) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


        $this->createTable('order_code', [
            'id' => $this->primaryKey(),
            'order_code' => $this->string(),
            'created_date' => $this->timestamp(),
            'stage' => $this->smallInteger(),
            'client_id' => $this->integer(),
            'order_date' => $this->timestamp(),
            'address_id' => $this->integer(),
            'delivery_price' => $this->integer(),
            'delivery' => $this->smallInteger(),
            'description' => $this->text(),
            'description_ru' => $this->text(),
            'pay' => $this->integer(),
            'billing_status' => $this->smallInteger(),
            'bot' => $this->smallInteger()
        ]);

        // CREATE TABLE `order_code` (
        //   `id` int(10) NOT NULL,
        //   `order_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `created_date` datetime DEFAULT NULL,
        //   `stage` smallInteger(6) DEFAULT NULL,
        //   `client_id` int(11) DEFAULT NULL,
        //   `order_date` datetime DEFAULT NULL,
        //   `address_id` int(11) DEFAULT NULL,
        //   `delivery_price` int(11) DEFAULT NULL,
        //   `delivery` smallInteger(6) DEFAULT NULL,
        //   `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `pay` int(11) DEFAULT NULL,
        //   `billing_status` smallInteger(6) DEFAULT NULL,
        //   `bot` smallInteger(6) DEFAULT 0
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $this->createTable('order_details', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'details_id' => $this->integer(),
            'multi_details_id' => $this->integer(),
        ]);
        

        // CREATE TABLE `order_details` (
        //   `id` int(10) NOT NULL,
        //   `order_id` int(10) DEFAULT NULL,
        //   `details_id` int(10) DEFAULT NULL,
        //   `multi_details_id` int(10) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'sub_category_id' => $this->integer(),
            'title_uz' => $this->string(),
            'title_ru' => $this->string(),
            'price' => $this->integer(),
            'img' => $this->string(),
            'description_uz' => $this->text(),
            'description_ru' => $this->text(),
            'status' => $this->smallInteger()
        ]);

        // CREATE TABLE `products` (
        //   `id` int(11) NOT NULL,
        //   `sub_category_id` int(10) DEFAULT NULL,
        //   `title_uz` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `title_ru` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `price` int(20) DEFAULT NULL,
        //   `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `description_uz` text COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `description_ru` text COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `status` int(1) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $sql_insert = "
           INSERT INTO products (sub_category_id, title_uz, title_ru, price, img, description_uz, description_ru, status) VALUES
        (1, 'Futbolkalar', 'Футболки', 50000, 'https://st-levis.mncdn.com/mnresize/1500/402/Content/media/ProductImg/original/637473534130540452.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna ', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna ', 1),
        (1, 'Kepkalar', 'Колпачки', 30000, 'https://apollo-olx.cdnvideo.ru/v1/files/ibdrkek04yr91-UZ/image;s=1000x750', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna ', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna ', 1),
        (1, 'Shimlar', 'Штаны\r\n', 60000, 'https://shoptextile.uz/image/cache/catalog/product/ST1121/ST1121_1-1000x1000.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna ', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna ', 1);
        ";
        $this->execute($sql_insert);


        $this->createTable('product_details', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'title_uz' => $this->string(),
            'title_ru' => $this->string(),
            'status' => $this->smallInteger()
        ]);


        // CREATE TABLE `product_details` (
        //   `id` int(11) NOT NULL,
        //   `product_id` int(10) DEFAULT NULL,
        //   `title_uz` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `title_ru` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `status` int(1) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $sql_insert = "
           INSERT INTO product_details ( product_id, title_uz, title_ru, status) VALUES
            (1, 'detail 1', 'detail 1', 1),
            (1, 'detail 2', 'detail 2', 1),
            (1, 'detail 3', 'detail 3', 1),
            (1, 'detail 4', 'detail 4', 1),
            (2, 'detail 1', 'detail 1', 1),
            (2, 'detail 2', 'detail 2', 1),
            (2, 'detail 3', 'detail 3', 1),
            (2, 'detail 4', 'detail 4', 1),
            (3, 'detail 1', 'detail 1', 1),
            (3, 'detail 2', 'detail 2', 1),
            (3, 'detail 3', 'detail 3', 1);
        ";
        $this->execute($sql_insert);
       

        $this->createTable('product_details_', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'details_id' => $this->integer(),
            'status' => $this->smallInteger()
        ]);


        // CREATE TABLE product_details_` (
        //   `id` int(10) NOT NULL,
        //   `product_id` int(10) NOT NULL,
        //   `details_id` int(20) NOT NULL,
        //   `status` int(1) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $sql_insert = "
           INSERT INTO product_details_ (product_id, details_id, status) VALUES
            (1, 1, NULL),
            (1, 5, NULL),
            (1, 3, NULL),
            (2, 3, NULL),
            (2, 2, NULL),
            (2, 5, NULL),
            (3, 5, NULL),
            (3, 1, NULL),
            (3, 3, NULL);
        ";
        $this->execute($sql_insert);

        $this->createTable('product_img', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'img' => $this->string(),
            'status' => $this->smallInteger()
        ]);

        // CREATE TABLE `product_img` (
        //   `id` int(11) NOT NULL,
        //   `product_id` int(10) DEFAULT NULL,
        //   `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `status` int(1) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $sql_insert = "
            INSERT INTO product_img (product_id, img, status) VALUES
            (1, 'https://sc04.alicdn.com/kf/Ua8289c9a517f40ffb29dec5c55d9563fX.jpg', 1),
            (1, 'https://sc04.alicdn.com/kf/U5ae3a03b63334b1788f80bfb80504cc6v.jpg', 1),
            (1, 'https://sc04.alicdn.com/kf/U143f839bbd3040f3a7b07125cd53c7d0d.jpg', 1),
            (1, 'https://sc04.alicdn.com/kf/Uca83faa52453435eb24c1ac58f8d20f6U.jpg\r\n', 1);
        ";
        $this->execute($sql_insert);

        $this->createTable('step', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(),
            'step_1' => $this->integer(),
            'step_2' => $this->integer()
        ]);

        // CREATE TABLE `step` (
        //   `id` int(11) NOT NULL,
        //   `chat_id` int(20) DEFAULT NULL,
        //   `step_1` int(11) DEFAULT NULL,
        //   `step_2` int(11) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


        $this->createTable('sub_category', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'title_uz' => $this->string(),
            'title_ru' => $this->string(),
            'img' => $this->string(),
            'status' => $this->smallInteger(),
        ]);

        // CREATE TABLE `sub_category` (
        //   `id` int(11) NOT NULL,
        //   `category_id` int(10) DEFAULT NULL,
        //   `title_uz` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `title_ru` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        //   `status` int(1) DEFAULT NULL
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        $sql_insert = "
            INSERT INTO sub_category (category_id, title_uz, title_ru, img, status) VALUES
            (1, 'Kiyim 1', 'Одежда 1', 'img 1', 1),
            (1, 'Kiyim 2', 'Одежда 2', 'img 2', 1),
            (1, 'Kiyim 3', 'Одежда 3', 'img 3', 1),
            (1, 'Kiyim 4', 'Одежда 4', 'img 4', 1),
            (1, 'Kiyim 5', 'Одежда 5', 'img 5', 1),
            (1, 'Kiyim 6', 'Одежда 6', 'img 6', 1),
            (1, 'Kiyim 7', 'Одежда 7', 'img 7', 1),
            (1, 'Kiyim 8', 'Одежда 8', 'img 8', 1);
        ";
        $this->execute($sql_insert);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210622_065722_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210622_065722_tables cannot be reverted.\n";

        return false;
    }
    */
}
