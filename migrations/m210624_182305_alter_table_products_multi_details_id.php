<?php

use yii\db\Migration;

/**
 * Class m210624_182305_alter_table_products_multi_details_id
 */
class m210624_182305_alter_table_products_multi_details_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE product_details_ ADD COLUMN multi_details_id integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210624_182305_alter_table_products_multi_details_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210624_182305_alter_table_products_multi_details_id cannot be reverted.\n";

        return false;
    }
    */
}
