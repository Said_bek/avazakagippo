<?php

use yii\db\Migration;

/**
 * Class m210625_135551_new_migration_table
 */
class m210625_135551_new_migration_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('arrive_orders', [
            'id' => $this->primaryKey(),
            'order_code_id' => $this->integer(),
            'product_id' => $this->integer(),
            'sell_price' => $this->integer(),
            'count' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210625_135551_new_migration_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210625_135551_new_migration_table cannot be reverted.\n";

        return false;
    }
    */
}
