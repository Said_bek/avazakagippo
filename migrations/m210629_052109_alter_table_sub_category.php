<?php

use yii\db\Migration;

/**
 * Class m210629_052109_alter_table_sub_category
 */
class m210629_052109_alter_table_sub_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE sub_category ADD COLUMN step integer";
        $this->execute($sql);

        $sql = "ALTER TABLE sub_category ADD COLUMN sub_category_id integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210629_052109_alter_table_sub_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210629_052109_alter_table_sub_category cannot be reverted.\n";

        return false;
    }
    */
}
