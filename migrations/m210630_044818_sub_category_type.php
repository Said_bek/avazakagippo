<?php

use yii\db\Migration;

/**
 * Class m210630_044818_sub_category_type
 */
class m210630_044818_sub_category_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE sub_category ADD COLUMN type integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210630_044818_sub_category_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210630_044818_sub_category_type cannot be reverted.\n";

        return false;
    }
    */
}
