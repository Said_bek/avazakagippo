<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%new}}`.
 */
class m210711_175137_create_new_Table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $this->createTable('blogs', [
            'id' => $this->primaryKey(),
            'title_uz' => $this->string(),
            'title_ru' => $this->string(),
            'img' => $this->string(),
            'description_uz' => $this->text(),
            'description_ru' => $this->text(),
            'created_date' => $this->timestamp(),
            'enter_date' => $this->timestamp()
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%new}}');
    }
}
