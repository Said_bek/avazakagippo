<?php

use yii\db\Migration;

/**
 * Class m210713_060927_product_details_code
 */
class m210713_060927_product_details_code extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_details_code', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'multi_detail_id' => $this->integer(),
            'detail_id' => $this->integer(),
            'detail_code' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210713_060927_product_details_code cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210713_060927_product_details_code cannot be reverted.\n";

        return false;
    }
    */
}
