<?php

use yii\db\Migration;

/**
 * Class m210714_044459_type_for_details
 */
class m210714_044459_type_for_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE details ADD COLUMN type integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210714_044459_type_for_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210714_044459_type_for_details cannot be reverted.\n";

        return false;
    }
    */
}
