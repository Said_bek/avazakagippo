<?php

use yii\db\Migration;

/**
 * Class m210721_074309_alter_tables
 */
class m210721_074309_alter_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE multi_details ADD COLUMN color_name varchar";
        $this->execute($sql);

        $sql = "ALTER TABLE product_img ADD COLUMN multi_detail_id integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210721_074309_alter_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210721_074309_alter_tables cannot be reverted.\n";

        return false;
    }
    */
}
