<?php

use yii\db\Migration;

/**
 * Class m210723_054006_alter_tables_sub_Category_products
 */
class m210723_054006_alter_tables_sub_Category_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE sub_category  ADD COLUMN main_category integer";
        $this->execute($sql);
        $sql = "ALTER TABLE products  ADD COLUMN main_category integer";
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210723_054006_alter_tables_sub_Category_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210723_054006_alter_tables_sub_Category_products cannot be reverted.\n";

        return false;
    }
    */
}
