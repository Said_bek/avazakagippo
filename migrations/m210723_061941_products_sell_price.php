<?php

use yii\db\Migration;

/**
 * Class m210723_061941_products_sell_price
 */
class m210723_061941_products_sell_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE products  ADD COLUMN sell_count integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210723_061941_products_sell_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210723_061941_products_sell_price cannot be reverted.\n";

        return false;
    }
    */
}
