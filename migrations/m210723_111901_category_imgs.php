<?php

use yii\db\Migration;

/**
 * Class m210723_111901_category_imgs
 */
class m210723_111901_category_imgs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE category  ADD COLUMN imgs varchar";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210723_111901_category_imgs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210723_111901_category_imgs cannot be reverted.\n";

        return false;
    }
    */
}
