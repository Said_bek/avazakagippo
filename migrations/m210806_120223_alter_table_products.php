<?php

use yii\db\Migration;

/**
 * Class m210806_120223_alter_table_products
 */
class m210806_120223_alter_table_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE products ADD COLUMN sku varchar";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210806_120223_alter_table_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210806_120223_alter_table_products cannot be reverted.\n";

        return false;
    }
    */
}
