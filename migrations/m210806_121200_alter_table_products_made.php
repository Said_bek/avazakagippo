<?php

use yii\db\Migration;

/**
 * Class m210806_121200_alter_table_products_made
 */
class m210806_121200_alter_table_products_made extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE products ADD COLUMN made varchar";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210806_121200_alter_table_products_made cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210806_121200_alter_table_products_made cannot be reverted.\n";

        return false;
    }
    */
}
