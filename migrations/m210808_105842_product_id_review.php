<?php

use yii\db\Migration;

/**
 * Class m210808_105842_product_id_review
 */
class m210808_105842_product_id_review extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE reviews ADD COLUMN product_id integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210808_105842_product_id_review cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210808_105842_product_id_review cannot be reverted.\n";

        return false;
    }
    */
}
