<?php

use yii\db\Migration;

/**
 * Class m210812_064627_country
 */
class m210812_064627_country extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {  
        $this->createTable('region', [
            'id' => $this->primaryKey(),
            'title_uz' => $this->string(),
            'title_ru' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210812_064627_country cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210812_064627_country cannot be reverted.\n";

        return false;
    }
    */
}
