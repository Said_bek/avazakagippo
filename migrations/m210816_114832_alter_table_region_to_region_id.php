<?php

use yii\db\Migration;

/**
 * Class m210816_114832_alter_table_region_to_region_id
 */
class m210816_114832_alter_table_region_to_region_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE clients_address RENAME COLUMN region to  region_id;";
        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210816_114832_alter_table_region_to_region_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210816_114832_alter_table_region_to_region_id cannot be reverted.\n";

        return false;
    }
    */
}
