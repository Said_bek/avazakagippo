<?php

use yii\db\Migration;

/**
 * Class m210817_062514_referal_count
 */
class m210817_062514_referal_count extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE clients ADD COLUMN referal_ball integer";
        $this->execute($sql);
        $sql = "ALTER TABLE clients ADD COLUMN referal_step integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210817_062514_referal_count cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210817_062514_referal_count cannot be reverted.\n";

        return false;
    }
    */
}
