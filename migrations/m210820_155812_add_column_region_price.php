<?php

use yii\db\Migration;

/**
 * Class m210820_155812_add_column_region_price
 */
class m210820_155812_add_column_region_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE region ADD COLUMN price integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210820_155812_add_column_region_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210820_155812_add_column_region_price cannot be reverted.\n";

        return false;
    }
    */
}
