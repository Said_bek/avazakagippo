<?php

use yii\db\Migration;

/**
 * Class m210821_080011_create_table_currency_price
 */
class m210821_080011_create_table_currency_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "
        CREATE TABLE IF NOT EXISTS public.currency_price
        (
            id serial NOT NULL,
            sum INTEGER,
            dollar DOUBLE PRECISION,
            start_date DATE,
            end_date DATE,
            status SMALLINT,
            PRIMARY KEY (id)
        );";

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210821_080011_create_table_currency_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210821_080011_create_table_currency_price cannot be reverted.\n";

        return false;
    }
    */
}
