<?php

use yii\db\Migration;

/**
 * Class m210821_163210_drop_table_client_address2
 */
class m210821_163210_drop_table_client_address2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "DROP TABLE clients_address2";
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210821_163210_drop_table_client_address2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210821_163210_drop_table_client_address2 cannot be reverted.\n";

        return false;
    }
    */
}
