<?php

use yii\db\Migration;

/**
 * Class m210822_074304_add_column_to_category_and_sub_category
 */
class m210822_074304_add_column_to_category_and_sub_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE category ADD COLUMN title_en character varying";
        $this->execute($sql);

        $sql = "ALTER TABLE sub_category ADD COLUMN title_en character varying";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210822_074304_add_column_to_category_and_sub_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210822_074304_add_column_to_category_and_sub_category cannot be reverted.\n";

        return false;
    }
    */
}
