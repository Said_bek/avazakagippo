<?php

use yii\db\Migration;

/**
 * Class m210904_083549_client_cashback
 */
class m210904_083549_client_cashback extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('client_cashback', [
            'id' => $this->primaryKey(),
            'cashback_id' => $this->integer(),
            'client_id' => $this->integer(),
            'cashback_parcent' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210904_083549_client_cashback cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210904_083549_client_cashback cannot be reverted.\n";

        return false;
    }
    */
}
