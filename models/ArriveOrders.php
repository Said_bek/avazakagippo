<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "arrive_orders".
 *
 * @property int $id
 * @property int|null $order_code_id
 * @property int|null $product_id
 * @property int|null $sell_price
 * @property int|null $count
 */
class ArriveOrders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'arrive_orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_code_id', 'product_id', 'sell_price', 'count'], 'default', 'value' => null],
            [['order_code_id', 'product_id', 'sell_price', 'count'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_code_id' => 'Order Code ID',
            'product_id' => 'Product ID',
            'sell_price' => 'Sell Price',
            'count' => 'Count',
        ];
    }
    public function getProducts()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
    // public function getDetails()
    // {
    //     return $this->hasMany(OrderDetails::className(),['order_id' => 'order_id']);
    // }
}
