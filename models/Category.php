<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string|null $title_uz
 * @property string|null $title_ru
 * @property int|null $status
 */

class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_uz', 'title_ru'], 'required'],
            [['status'], 'integer'],
            [['title_uz', 'title_ru'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_uz' => 'Kategoriya nomi',
            'title_ru' => 'Kategoriya nomi ru',
            'status' => 'Status',
        ];
    }

    public function getSub()
    {
        return $this->hasMany(SubCategory::className(),["category_id" => "id"]);
    }

    public function getSubtwo()
    {
        return $this->hasMany(SubCategory::className(),["category_id" => "id"])->all();
    }
    
    public function getCount()
    {   
        $id = $this->id;
        $product = Products::find()->where("main_category = :id", [":id" => $id])->count();
        return $product;
    }
    public function SortingProducts($id,$word) 
    {  
        $id = intval($id);
        $session = Yii::$app->session;
        $category = [];
        $categoryId = 0;
        if ($word == "category") {
            if (isset($session["list"]["selected"]) and !empty($session["list"]["selected"])) {
                if (isset($session["list"]["selected"]["category"]) and !empty($session["list"]["selected"]["category"])) {
                    $category_model = $this::find()->where("id = :id",[":id" => $id])->all();
                    foreach ($category_model as $key => $value) {
                        $session["list"]["selected"]["category"][$value["id"]] = [
                            "title_uz" => $value["title_uz"],
                            "title_ru" =>$value["title_ru"],
                            "unique_id" => $value["id"]
                        ];
                    }
                    $category_id = [];
                    foreach ($session["list"]["selected"]["category"] as $key => $value) {
                        $category_id[] = $value["unique_id"];
                    }
                } else {
                    $categoryId = $id;
                    $category_model = $this::find()->where("id = :id",[":id" => $id])->all();
                    foreach ($category_model as $key => $value) {
                        $session["list"]["selected"]["category"][$value["id"]] = [
                            "title_uz" => $value["title_uz"],
                            "title_ru" =>$value["title_ru"],
                            "unique_id" => $value["id"]
                        ];
                    }
                }
                if (isset($session["list"]["selected"]["sub_category"]) and !empty($session["list"]["selected"]["sub_category"]))  {
                    $sub_id = [];
                    foreach ($session["list"]["selected"]["sub_category"] as $key => $value) {
                        $sub_id[] = $value["unique_id"];
                    }

                    $count = count($sub_id);
                    $sub_category_id = [];
                    for ($q=0; $q < $count; $q++) { 
                        $SubCategory = SubCategory::find()->where("sub_category_id = :id",[":id" => $sub_id[$q]])->all();
                        if (isset($SubCategory) and !empty($SubCategory)) {
                            foreach ($SubCategory as $key => $value) {
                                if($value["main_category"] != $id) {
                                    $sub_category_id[] = $value["id"];
                                }
                            }
                        }
                    }
                }
            } else {
                $category = $this::find()->where("id = :id",["id" => $id])->one();
                if (isset($category) and !empty($category)) {
                    $product = Products::find()->where("main_category = :category_id",[":category_id" => $id])->orderBy(["id" => SORT_ASC])->limit(2)->all();
                    $session["list"] = new \ArrayObject();
                    $session["list"]["selected"]["category"][$category->id] = [
                        "title_uz" => $category->title_uz,
                        "title_ru" =>$category->title_ru,
                        "unique_id" => $category->id
                    ];
                    // $session["list"]["product"] = [];
                    $session["list"]["product"] = $product;
                    $count_product = count($product);
                    $key = $count_product - 1;
                    $session["list"]["last_id"] = $product[$key]->id;
                    if ((isset($session["list"]["product"]) and !empty($session["list"]["product"])) and (isset($session["list"]["selected"]) and !empty($session["list"]["selected"]))) {
                        $sql = "main_category = ".$id;
                        $session["list"]["sql"] = $sql;
                        return $product;
                    } else {
                        die("fail");
                    }
                }
            }
        }

        if ($word == "sub") {
            if (isset($session["list"]["selected"]) and !empty($session["list"]["selected"])) {
                if (isset($session["list"]["selected"]["sub_category"]) and !empty($session["list"]["selected"]["sub_category"])) {
                    $sub_id = [];
                    foreach ($session["list"]["selected"]["sub_category"] as $key => $value) {
                        $sub_id[] = $value["unique_id"];
                    }
                    $categoryOne =  SubCategory::find()->where("id = :sub_category_id",[":sub_category_id" => $id])->one();
                    if (isset($categoryOne) and !empty($categoryOne)) {
                        $session["list"]["selected"]["sub_category"][$categoryOne["id"]] = [
                            "title_uz" => $categoryOne->title_uz,
                            "title_ru" =>$categoryOne->title_ru,
                            "unique_id" => $categoryOne->id
                        ];
                    }

                    $sub_id[] = $id;
                    $count_sub_id = count($sub_id);
                    $sub_category_id_product = [];
                    $sub_id_two = [];
                    for ($i=0; $i < $count_sub_id ; $i++) { 
                        $d = 0;
                        $true = false;
                        $sub_category_id = [];
                        while ($true != true) {
                            if (isset($sub_category_id) and !empty($sub_category_id)) {
                                $sub_category = SubCategory::find()->where(["in","sub_category_id",$sub_category_id])->all();
                                if (isset($sub_category) and !empty($sub_category)) {
                                    $sub_category_id = [];
                                    foreach ($sub_category as $key => $value) {
                                        $sub_category_id[] = $value["id"];
                                    }
                                } else {
                                    $sub_category_id_product[$sub_id[$i]][0] = $sub_category_id;
                                    $true = true;
                                }
                            } else {
                                $category = SubCategory::find()->where("sub_category_id = :sub_category_id",[":sub_category_id" => $sub_id[$i]])->orderBy(["id" => SORT_ASC])->all();
                                if (isset($category) and !empty($category)) {
                                    foreach ($category as $key => $value) {
                                        $sub_category_id[] = $value["id"]; 
                                    }
                                } else {
                                    $sub_id_two[] = $sub_id[$i];
                                    $true = true;
                                }
                            }
                            if ($d == 10) {
                                $true == true;
                            }

                            $d++;
                        }
                    }

                    $sub_category_id = [];
                    if (isset($sub_category_id_product) and !empty($sub_category_id_product)) {
                        foreach ($sub_category_id_product as $key => $value) {
                            foreach ($value as $key2 => $value2) {
                                $count = count($value2);
                                for ($s=0; $s < $count ; $s++) { 
                                    $sub_category_id[] = $value2[$s];
                                } 
                            }
                        }
                    }
                    if (isset($sub_id_two) and !empty($sub_id_two)) {
                        $count = count($sub_id_two);
                        for ($i=0; $i < $count ; $i++) { 
                            $sub_category_id[] = $sub_id_two[$i];
                        }
                    }
                    // print_r($sub_category_id);
                } else {
                    $true = false;
                    $i = 1;
                    $sub_id = [];
                    while ($true != true) {
                        // if ($i == 3) {
                        //     $true = true;
                        // }
                        if (isset($sub_id) and !empty($sub_id)) {
                            $sub_category = SubCategory::find()->where(["in","sub_category_id",$sub_id])->all();
                            if (isset($sub_category) and !empty($sub_category)) {
                                foreach ($sub_category as $key => $value) {
                                    $sub_id[] = $value["id"];
                                }
                            } else {
                                $true = true;
                            }
                        } else {
                            $categoryOne =  SubCategory::find()->where("id = :sub_category_id",[":sub_category_id" => $id])->one();
                            if (isset($categoryOne) and !empty($categoryOne)) {
                                $session["list"]["selected"]["sub_category"][$categoryOne["id"]] = [
                                    "title_uz" => $categoryOne->title_uz,
                                    "title_ru" =>$categoryOne->title_ru,
                                    "unique_id" => $categoryOne->id
                                ];
                            }
                            $category = SubCategory::find()->where("sub_category_id = :sub_category_id",[":sub_category_id" => $id])->all();
                            if (isset($category) and !empty($category)) {
                                foreach ($category as $key => $value) {
                                    $sub_id[] = $value["id"]; 
                                }
                            }
                        }
                        $i++;
                    }
                    $sub_category_id = $sub_id;
                }
                if (isset($session["list"]["selected"]["category"]) and !empty($session["list"]["selected"]["category"])) {
                    // print_r();
                    $category_id = [];
                    foreach ($session["list"]["selected"]["category"] as $key => $value) {
                        $category = SubCategory::find()->where("main_category = :main_category",[":main_category" => $value["unique_id"]])->andWhere(["in","id",$sub_category_id])->all();
                        if (!(isset($category) and !empty($category))) {
                            $category_id[] = $value["unique_id"]; 
                        }
                    }
                }
            } else {
                $session["list"] = new \ArrayObject();
                $true = false;
                $i = 1;
                $sub_id = [];
                while ($true != true) {
                    // if ($i == 3) {
                    //     $true = true;
                    // }
                    if (isset($sub_id) and !empty($sub_id)) {
                        $sub_category = SubCategory::find()->where(["in","sub_category_id",$sub_id])->all();
                        if (isset($sub_category) and !empty($sub_category)) {
                            foreach ($sub_category as $key => $value) {
                                $sub_id[] = $value["id"];
                            }
                        } else {
                            $product = Products::find()->where(["in","sub_category_id",$sub_id])->orderBy(["id" => SORT_ASC])->limit(2)->all();

                            $count_product = count($product);
                            $key = $count_product - 1;
                            $session["list"]["last_id"] = $product[$key]->id;

                            $count = count($sub_id);
                            $txt = '(';
                            for ($i=0; $i < $count; $i++) { 
                                if ($i == 0) {
                                    $txt = $txt.' '.$sub_id[$i];
                                } else {
                                    $txt = $txt.', '.$sub_id[$i];
                                }
                            }
                            $txt = $txt.')';
                            $sql = " pr.sub_category_id in ".$txt."";
                            $session["list"]["sql"] = $sql;
                            return $product;
                        }
                    } else {
                        
                        $categoryOne =  SubCategory::find()->where("id = :sub_category_id",[":sub_category_id" => $id])->one();
                        if (isset($categoryOne) and !empty($categoryOne)) {
                            $session["list"]["selected"]["sub_category"][$categoryOne["id"]] = [
                                "title_uz" => $categoryOne->title_uz,
                                "title_ru" =>$categoryOne->title_ru,
                                "unique_id" => $categoryOne->id
                            ];
                        }
                        $category = SubCategory::find()->where("sub_category_id = :sub_category_id",[":sub_category_id" => $id])->all();
                        if (isset($category) and !empty($category)) {
                            foreach ($category as $key => $value) {
                                $sub_id[] = $value["id"]; 
                            }
                        } else {
                            $sub_category_id[] = $id;
                            $true = true;
                        }
                    }
                    $i++;
                }
            }            
        }

        if ($word == "sub_secondary") {
            $sub_category_secondary = SubCategory::find()->where("id = :id",[":id"=>$id])->one();
            if (isset($sub_category_secondary) and !empty($sub_category_secondary)) {
                $sub_category_id = [];
                if (!(isset($session["list"]["selected"]) and !empty($session["list"]["selected"]))) {
                    $session["list"] = new \ArrayObject();
                }
                $session["list"]["selected"]["sub_category_second"][$sub_category_secondary["id"]] = [
                    "title_uz" => $sub_category_secondary->title_uz,
                    "title_ru" =>$sub_category_secondary->title_ru,
                    "unique_id" => $sub_category_secondary->id
                ];

                if (isset($session["list"]["selected"]["category"]) and !empty($session["list"]["selected"]["category"])) {
                    $category_id = [];
                    foreach ($session["list"]["selected"]["category"] as $key => $value) {
                        if ($value["unique_id"] != $sub_category_secondary->main_category) {
                            $category_id[] = $value["unique_id"];
                        }
                    }
                }

                if (isset($session["list"]["selected"]["sub_category"]) and !empty($session["list"]["selected"]["sub_category"])) {
                    $sub_id = [];
                    foreach ($session["list"]["selected"]["sub_category"] as $key => $value) {
                        $sub_id[] = $value["unique_id"];
                    }
                }
                if (isset($sub_id) and !empty($sub_id)) {
                    $count_sub_id = count($sub_id);
                    $sub_category_id_product = [];
                    $sub_id_two = [];
                    for ($i=0; $i < $count_sub_id ; $i++) { 
                        $d = 0;
                        $true = false;
                        $sub_category_id = [];
                        while ($true != true) {
                            if (isset($sub_category_id) and !empty($sub_category_id)) {
                                $sub_category = SubCategory::find()->where(["in","sub_category_id",$sub_category_id])->all();
                                if (isset($sub_category) and !empty($sub_category)) {
                                    $sub_category_id = [];
                                    foreach ($sub_category as $key => $value) {
                                        $sub_category_id[] = $value["id"];
                                    }
                                } else {
                                    $sub_category_id_product[$sub_id[$i]][0] = $sub_category_id;
                                    $true = true;
                                }
                            } else {
                                $category = SubCategory::find()->where("sub_category_id = :sub_category_id",[":sub_category_id" => $sub_id[$i]])->all();
                                if (isset($category) and !empty($category)) {
                                    foreach ($category as $key => $value) {
                                        $sub_category_id[] = $value["id"]; 
                                    }
                                } else {
                                    $sub_id_two[] = $sub_id[$i];
                                    $true = true;
                                }
                            }
                            if ($d == 10) {
                                $true == true;
                            }

                            $d++;
                        }
                    }
                    $sub_category_id = [];
                    if (isset($sub_category_id_product) and !empty($sub_category_id_product)) {
                        foreach ($sub_category_id_product as $key => $value) {
                            foreach ($value as $key2 => $value2) {
                                $count = count($value2);
                                for ($s=0; $s < $count ; $s++) { 
                                    $sub_category_id[] = $value2[$s];
                                } 
                            }
                        }
                    }
                    
                    $sub_id = $sub_category_id;
                    $count_sub_id = count($sub_id);
                    $sub_category_id = [];
                    for ($i=0; $i < $count_sub_id; $i++) { 
                        $sub_category_check = SubCategory::find()->where("id = :id",[":id" => $sub_id[$i]])->one();
                        if (isset($sub_category_check) and !empty($sub_category_check)) {
                            if ($sub_category_check->main_category != $sub_category_secondary->main_category) {
                                $sub_category_id[] = $sub_id[$i];
                            }
                        }
                    }
                }

                $sub_category_id[] = $id;
            }
        }

        if ($word == "multi_details") {
            if (isset($session["list"]["selected"]["category"]) and !empty($session["list"]["selected"]["category"])) {
                // print_r();
                $category_id = [];
                foreach ($session["list"]["selected"]["category"] as $key => $value) {
                    if (!(isset($category) and !empty($category))) {
                        $category_id[] = $value["unique_id"]; 
                    }
                }
            }

            if (isset($session["list"]["selected"]["sub_category"]) and !empty($session["list"]["selected"]["sub_category"])) {
                $sub_id = [];
                foreach ($session["list"]["selected"]["sub_category"] as $key => $value) {
                    $sub_id[] = $value["unique_id"];
                }

                $count_sub_id = count($sub_id);
                $sub_category_id_product = [];
                $sub_id_two = [];
                for ($i=0; $i < $count_sub_id ; $i++) { 
                    $d = 0;
                    $true = false;
                    $sub_category_id = [];
                    while ($true != true) {
                        if (isset($sub_category_id) and !empty($sub_category_id)) {
                            $sub_category = SubCategory::find()->where(["in","sub_category_id",$sub_category_id])->all();
                            if (isset($sub_category) and !empty($sub_category)) {
                                $sub_category_id = [];
                                foreach ($sub_category as $key => $value) {
                                    $sub_category_id[] = $value["id"];
                                }
                            } else {
                                $sub_category_id_product[$sub_id[$i]][0] = $sub_category_id;
                                $true = true;
                            }
                        } else {
                            $category = SubCategory::find()->where("sub_category_id = :sub_category_id",[":sub_category_id" => $sub_id[$i]])->all();
                            if (isset($category) and !empty($category)) {
                                foreach ($category as $key => $value) {
                                    $sub_category_id[] = $value["id"]; 
                                }
                            } else {
                                $sub_id_two[] = $sub_id[$i];
                                $true = true;
                            }
                        }
                        if ($d == 10) {
                            $true == true;
                        }

                        $d++;
                    }
                }

                $sub_category_id = [];
                if (isset($sub_category_id_product) and !empty($sub_category_id_product)) {
                    foreach ($sub_category_id_product as $key => $value) {
                        foreach ($value as $key2 => $value2) {
                            $count = count($value2);
                            for ($s=0; $s < $count ; $s++) { 
                                $sub_category_id[] = $value2[$s];
                            } 
                        }
                    }
                }
                if (isset($sub_id_two) and !empty($sub_id_two)) {
                    $count = count($sub_id_two);
                    for ($i=0; $i < $count ; $i++) { 
                        $sub_category_id[] = $sub_id_two[$i];
                    }
                }
            }
       

            if (isset($session["list"]["selected"]["multi_details"]) and !empty($session["list"]["selected"]["multi_details"])) {
                $multiDetail = MultiDetails::find()->where("id = :id",[":id" => $id])->one();
                if (isset($multiDetail) and !empty($multiDetail)) {
                    $session["list"]["selected"]["multi_details"][$id] = [
                        "title_uz" => $multiDetail->title_uz,
                        "title_ru" =>$multiDetail->title_ru,
                        "unique_id" => $multiDetail->id
                    ];
                }
            } else {
                // die("123456987654");
                if (!(isset($session["list"]["selected"]) and !empty($session["list"]["selected"]))) {
                    $session["list"] = new \ArrayObject();
                }
                $multiDetail = MultiDetails::find()->where("id = :id",[":id" => $id])->one();
                if (isset($multiDetail) and !empty($multiDetail)) {
                    $session["list"]["selected"]["multi_details"][$id] = [
                        "title_uz" => $multiDetail->title_uz,
                        "title_ru" =>$multiDetail->title_ru,
                        "unique_id" => $multiDetail->id
                    ];
                }
            }
            $multi_detail_id = [];
            foreach ($session["list"]["selected"]["multi_details"] as $key => $value) {
                $multi_detail_id[] = $value["unique_id"];
            }
        }
        if (isset($multi_detail_id) and !empty($multi_detail_id)) {
            $sql = "
                SELECT 
                    pr.*
                FROM products as pr
                INNER JOIN product_details_code as pdc on pr.id = pdc.product_id
                WHERE
            ";    
        } else {
            $sql = "
                SELECT * FROM products as pr WHERE ";
        }
        $wherePart = '';
        if ($categoryId != 0) {
            $wherePart = $wherePart." main_category = ".$categoryId;
        }
        if (isset($category_id) and !empty($category_id)) {
            $count = count($category_id);
            $in = '(';
            for ($i=0; $i < $count; $i++) { 
                if ($i == 0) {
                    $in = $in." ".$category_id[$i];
                } else {
                    $in = $in.", ".$category_id[$i];
                }
            }
            $in = $in." )";
            $wherePart = $wherePart." pr.main_category in ".$in;
        }
  
        if (isset($sub_category_id) and !empty($sub_category_id)) {
            $count = count($sub_category_id);
            $in = '(';
            for ($i=0; $i < $count; $i++) { 
                if ($i == 0) {
                    $in = $in." ".$sub_category_id[$i];
                } else {
                    $in = $in.", ".$sub_category_id[$i];
                }
            }
            $in = $in." )";
            if (isset($category_id) and !empty($category_id)) {
                $wherePart = $wherePart." or pr.sub_category_id in ".$in;
            } else {
                $wherePart = $wherePart." pr.sub_category_id in ".$in;
            }
        }

        if (isset($multi_detail_id) and !empty($multi_detail_id)) {
            $count = count($multi_detail_id);
            $in = '(';
            for ($i=0; $i < $count; $i++) { 
                if ($i == 0) {
                    $in = $in." ".$multi_detail_id[$i];
                } else {
                    $in = $in.", ".$multi_detail_id[$i];
                }
            }
            $in = $in." )";
            if (!(isset($category_id) and !empty($category_id)) and !(isset($sub_category_id) and !empty($sub_category_id))) {
                $wherePart = $wherePart." pdc.multi_detail_id in ".$in;
            } else {
                $wherePart = $wherePart." and pdc.multi_detail_id in ".$in;
            }
        }


        $sql = $sql.$wherePart." limit ".Products::LIMIT_PRODUCT;

        $session["list"]["sql"] = $wherePart;
        $product = Products::findBySql($sql)->all();
        $session["list"]["product"] = [];
        return $product;
    }

    public static function ClearSorting() 
    {
        $session = Yii::$app->session;
        if (isset($session["list"]["selected"]) and !empty($session["list"]["selected"]) or isset($session["list"]["type"]) and !empty($session["list"]["type"]))  {
            if (isset($session["list"]["selected"]["category"]) and !empty($session["list"]["selected"]["category"])) {
                $category_id = [];
                foreach ($session["list"]["selected"]["category"] as $key => $value) {
                    $category_id[] = $value["unique_id"];
                }
            }
            if (isset($session["list"]["selected"]["sub_category"]) and !empty($session["list"]["selected"]["sub_category"])) {
                $sub_id = [];
                foreach ($session["list"]["selected"]["sub_category"] as $key => $value) {
                    $sub_id[] = $value["unique_id"];
                }   
                if (isset($sub_id) and !empty($sub_id)) {
                    $count_sub_id = count($sub_id);
                    $sub_category_id_product = [];
                    $sub_id_two = [];
                    for ($i=0; $i < $count_sub_id ; $i++) { 
                        $d = 0;
                        $true = false;
                        $sub_category_id = [];
                        while ($true != true) {
                            if (isset($sub_category_id) and !empty($sub_category_id)) {
                                $sub_category = SubCategory::find()->where(["in","sub_category_id",$sub_category_id])->all();
                                if (isset($sub_category) and !empty($sub_category)) {
                                    $sub_category_id = [];
                                    foreach ($sub_category as $key => $value) {
                                        $sub_category_id[] = $value["id"];
                                    }
                                } else {
                                    $sub_category_id_product[$sub_id[$i]][0] = $sub_category_id;
                                    $true = true;
                                }
                            } else {
                                $category = SubCategory::find()->where("sub_category_id = :sub_category_id",[":sub_category_id" => $sub_id[$i]])->all();
                                if (isset($category) and !empty($category)) {
                                    foreach ($category as $key => $value) {
                                        $sub_category_id[] = $value["id"]; 
                                    }
                                } else {
                                    $sub_id_two[] = $sub_id[$i];
                                    $true = true;
                                }
                            }
                            if ($d == 10) {
                                $true == true;
                            }

                            $d++;
                        }
                    }
                    $sub_category_id = [];
                    if (isset($sub_category_id_product) and !empty($sub_category_id_product)) {
                        foreach ($sub_category_id_product as $key => $value) {
                            foreach ($value as $key2 => $value2) {
                                $count = count($value2);
                                for ($s=0; $s < $count ; $s++) { 
                                    $sub_category_id[] = $value2[$s];
                                } 
                            }
                        }
                    }
                    
                    $sub_id = $sub_category_id;
                    $count_sub_id = count($sub_id);
                    $sub_category_id = [];
                    for ($i=0; $i < $count_sub_id; $i++) { 
                        $sub_category_check = SubCategory::find()->where("id = :id",[":id" => $sub_id[$i]])->one();
                        if (isset($sub_category_check) and !empty($sub_category_check)) {
                            $sub_category_id[] = $sub_id[$i];
                        }
                    }
                }

                $sub_category_id = $sub_id;
            }
            if (isset($session["list"]["selected"]["multi_details"]) and !empty($session["list"]["selected"]["multi_details"])) {
                $multi_detail_id = [];
                foreach ($session["list"]["selected"]["multi_details"] as $key => $value) {
                    $multi_detail_id[] = $value["unique_id"];
                }   
            }
            if (isset($multi_detail_id) and !empty($multi_detail_id)) {
                $sql = "
                    SELECT 
                        pr.*
                    FROM products as pr
                    INNER JOIN product_details_code as pdc on pr.id = pdc.product_id
                    
                ";    
            } else {
 
                $sql = "
                    SELECT * FROM products as pr  ";
            }
            if (isset($session["list"]["type"]) and !empty($session["list"]["type"])) {
                if ($session["list"]["type"] == 10) {
                    $sql = $sql.PHP_EOL."INNER JOIN product_sale as ps on pr.id = ps.product_id";
                }
            }
            $wherePart = ' WHERE ';
           
            if (isset($category_id) and !empty($category_id)) {
                $count = count($category_id);
                $in = '(';
                for ($i=0; $i < $count; $i++) { 
                    if ($i == 0) {
                        $in = $in." ".$category_id[$i];
                    } else {
                        $in = $in.", ".$category_id[$i];
                    }
                }
                $in = $in." )";
                $wherePart = $wherePart." pr.main_category in ".$in;
            }
      
            if (isset($sub_category_id) and !empty($sub_category_id)) {
                $count = count($sub_category_id);
                $in = '(';
                for ($i=0; $i < $count; $i++) { 
                    if ($i == 0) {
                        $in = $in." ".$sub_category_id[$i];
                    } else {
                        $in = $in.", ".$sub_category_id[$i];
                    }
                }
                $in = $in." )";
                if (isset($category_id) and !empty($category_id)) {
                    $wherePart = $wherePart." or pr.sub_category_id in ".$in;
                } else {
                    $wherePart = $wherePart." pr.sub_category_id in ".$in;
                }
            }

            // print_r($session["list"]["selected"]["multi_details"]);
            // die();
            if (isset($multi_detail_id) and !empty($multi_detail_id)) {
                $count = count($multi_detail_id);
                $in = '(';
                for ($i=0; $i < $count; $i++) { 
                    if ($i == 0) {
                        $in = $in." ".$multi_detail_id[$i];
                    } else {
                        $in = $in.", ".$multi_detail_id[$i];
                    }
                }
                $in = $in." )";
                if (!(isset($category_id) and !empty($category_id)) and !(isset($sub_category_id) and !empty($sub_category_id))) {
                    $wherePart = $wherePart." pdc.multi_detail_id in ".$in;
                } else {
                    $wherePart = $wherePart." and pdc.multi_detail_id in ".$in;
                }
            }

            if (isset($session["list"]["type"]) and !empty($session["list"]["type"]) and $session["list"]["type"] != 10) {
                $count = count($session["list"]["type"]);
                $in = '(';
                for ($i=0; $i < $count; $i++) { 
                    if ($i == 0) {
                        $in = $in." ".$session["list"]["type"][$i];
                    } else {
                        $in = $in.", ".$session["list"]["type"][$i];
                    }
                }
                $in = $in." )";
                if (isset($session["list"]["selected"]) and !empty($session["list"]["selected"]))  {
                    $wherePart = $wherePart." and status in ".$in;
                } else {
                    $wherePart = $wherePart." status in ".$in;
                }
            }
            if (isset($session["list"]["limit"]) and !empty($session["list"]["limit"])) {
                $wherePart = $wherePart." limit ".$session["list"]["limit"];
            }
            if ($wherePart != " WHERE ") {
                $sql = $sql.$wherePart;
            }
            $sql = $sql." limit ".Products::LIMIT_PRODUCT;
            
            $session["list"]["sql"] = $wherePart;
            $product = $customers = Products::findBySql($sql)->all();
            
            return $product;
        }
    }
}