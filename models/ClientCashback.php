<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_cashback".
 *
 * @property int $id
 * @property int|null $cashback_id
 * @property int|null $client_id
 * @property int|null $cashback_parcent
 */
class ClientCashback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_cashback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cashback_id', 'client_id', 'cashback_parcent'], 'default', 'value' => null],
            [['cashback_id', 'client_id', 'cashback_parcent'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cashback_id' => 'Cashback ID',
            'client_id' => 'Client ID',
            'cashback_parcent' => 'Cashback Parcent',
        ];
    }
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }
}
