<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_order_camments".
 *
 * @property int $id
 * @property int|null $order_code_id
 * @property int|null $client_id
 * @property string|null $text_camment
 */
class ClientOrderCamments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_order_camments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_code_id', 'client_id'], 'default', 'value' => null],
            [['order_code_id', 'client_id'], 'integer'],
            [['text_camment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_code_id' => 'Order Code ID',
            'client_id' => 'Client ID',
            'text_camment' => 'Text Camment',
        ];
    }
}
