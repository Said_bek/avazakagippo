<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "details".
 *
 * @property int $id
 * @property string|null $title_uz
 * @property string|null $title_ru
 * @property int|null $status
 */
class Details extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'default', 'value' => null],
            [['status'], 'integer'],
            [['title_uz', 'title_ru','title_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_uz' => 'Qo`shimcha ma`lumot nomi ',
            'title_ru' => 'Qo`shimcha ma`lumot nomi ru',
            'status' => 'Status',
        ];
    }
    public function getMultiDetails()
    {
        return $this->hasMany(MultiDetails::class, ['details_id' => 'id']);
    }
}


