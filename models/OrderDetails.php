<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_details".
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $details_id
 * @property int|null $multi_details_id
 */
class OrderDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'details_id', 'multi_details_id'], 'default', 'value' => null],
            [['order_id', 'details_id', 'multi_details_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'details_id' => 'Details ID',
            'multi_details_id' => 'Multi Details ID',
        ];
    }
    public function getMultidetails()
    {
        return $this->hasMany(MultiDetails::className(),['id' => 'multi_details_id']);
    }
}
