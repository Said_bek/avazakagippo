<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_details_".
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $details_id
 * @property int|null $status
 * @property int|null $multi_details_id
 */
class ProductDetailsMini extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_details_';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'details_id', 'status', 'multi_details_id'], 'default', 'value' => null],
            [['product_id', 'details_id', 'status', 'multi_details_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'details_id' => 'Details ID',
            'status' => 'Status',
            'multi_details_id' => 'Multi Details ID',
        ];
    }
    public function getDetails()
    {
        return $this->hasOne(Details::className(), ['id' => 'details_id']);
    }
    public function getMulti()
    {
        return $this->hasOne(MultiDetails::className(), ['id' => 'multi_details_id']);
    }
}
