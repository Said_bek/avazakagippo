<?php 
    
namespace app\models;

use yii\web\UploadedFile;


class ProjectHelper
{

    public static function upload(?UploadedFile $file, $oldPhoto, $path)
    {
        if (!$file) {
            return null;
        }
        $microTime = md5(uniqid());
        if ($oldPhoto and file_exists('../web' . $path . $oldPhoto)) {
            unlink('../web' . $path . $oldPhoto);
        }
        $file->saveAs('../web' . $path . $microTime . '.' . $file->extension);
        return $microTime . '.' . $file->extension;
    }
    
}


?>