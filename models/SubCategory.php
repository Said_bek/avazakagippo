<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_category".
 *
 * @property int $id
 * @property int|null $category_id
 * @property string|null $title_uz
 * @property string|null $title_ru
 * @property string|null $img
 * @property int|null $status
 */
class SubCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $imageFile;
 
    public static function tableName()
    {
        return 'sub_category';
    }

    /**
     * {@inheritdoc}
     */
    
    public function rules()
    {
        return [
            [['category_id', 'status'], 'default', 'value' => null],
            [['category_id', 'status'], 'integer'],
            [['title_uz', 'title_ru','title_en','img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'title_uz' => 'Title Uz',
            'title_ru' => 'Title Ru',
            'img' => 'Img',
            'status' => 'Status',
        ];
    }
    
    public function getSubcategory()
    {
        return $this->hasMany(SubCategory::className(),["sub_category_id" => "id"])->limit(3);
    }
    
    public function getCount()
    {
        $id = $this->id;
        $sub_Category = SubCategory::find()->where("id = :id",[":id" => $id])->all();
        if (isset($sub_Category) and !empty($sub_Category)) {
            $sub_id = [];
            foreach ($sub_Category as $key => $value) {
                $sub_id[] = $value["id"];
            }
            $sub_category_second = SubCategory::find()->where(['in', 'sub_category_id', $sub_id])->all();
            if (isset($sub_category_second) and !empty($sub_category_second)) {
                $sub_id = [];
                foreach ($sub_category_second as $key => $value) {
                    $sub_id[] = $value["id"];
                }
                $countProduct = Products::find()->where(['in', 'sub_category_id', $sub_id])->count();
                return $countProduct;
            } else {
                $countProduct = Products::find()->where(['in', 'sub_category_id', $sub_id])->count();
                return $countProduct;
            }
        } else {
            $countProduct = Products::find()->where("sub_category_id = :id", [":sub_category_id" => $id])->count();
            return $countProduct;
        }
    }
}
