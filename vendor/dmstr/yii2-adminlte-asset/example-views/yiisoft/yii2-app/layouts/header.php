<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<style>
    .modal-backdrop {
        z-index: 1!important;
    }
</style>
<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">AdminPanel</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                
                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <?= Html::a(
                        'Chiqish',
                        ['/site/logout'],
                        [
                            'data-method' => 'post', 
                            'class' => 'btn btn-danger btn-flat',
                            'style' => 'padding: 10px;margin:4px'
                        ]
                    ) ?>
                </li>
            </ul>
        </div>
    </nav>
</header>