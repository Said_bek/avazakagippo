<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AboutUs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="about-us-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
            
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'description_uz')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'description_ru')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'file_id')->fileInput(['multiple'=>'multiple']); ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
