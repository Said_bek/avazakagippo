<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cashbacks */

$this->title = $model->cashback;
$this->params['breadcrumbs'][] = ['label' => 'Cashbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>

    .input {
    position: relative !important;
    appearance: none;
    margin: 8px;
    box-sizing: content-box;
    overflow: hidden;
    }
    .input:before {
    content: '';
    display: block;
    box-sizing: content-box;
    width: 16px;
    height: 16px;
    border: 2px solid #ccc;
    transition: 0.2s border-color ease;
    }
    .input:checked:before {
    border-color: #12CBC4;
    transition: 0.5s border-color ease;
    }
    .input:disabled:before {
    border-color: #ccc;
    background-color: #ccc;
    }
    .input{
    outline: none!important;
    }
    .input:after {
    content: '';
    display: block;
    position: absolute;
    box-sizing: content-box;
    top: 50%;
    left: 50%;
    transform-origin: 50% 50%;
    background-color: #12CBC4;
    width: 16px;
    height: 16px;
    border-radius: 100vh;
    transform: translate(-50%,-50%) scale(0);
    }
    .input:before {
    border-radius: 16px/4;
    }
    .input:after {
    width: 9.6px;
    height: 16px;
    border-radius: 0;
    transform: translate(-50%,-85%) scale(0) rotate(45deg);
    background-color: transparent;
    box-shadow: 4px 4px 0px 0px #12CBC4;
    }
    .input:checked:after {
    animation: toggleOnCheckbox 0.2s ease forwards;
    }
    .input.filled:before {
    border-radius: 16px/4;
    transition: 0.2s border-color ease, 0.2s background-color ease;
    }
    .input.filled:checked:not(:disabled):before {
    background-color: #12CBC4;
    }
    .input.filled:not(:disabled):after {
    box-shadow: 4px 4px 0px 0px white;
    }
    @keyframes toggleOnCheckbox {
    0% {
    opacity: 0;
    transform: translate(-50%,-85%) scale(0) rotate(45deg);
    }
    70% {
    opacity: 1;
    transform: translate(-50%,-85%) scale(0.9) rotate(45deg);
    }
    100% {
    transform: translate(-50%,-85%) scale(0.8) rotate(45deg);
    }
    }
    @keyframes toggleOnRadio {
    0% {
    opacity: 0;
    transform: translate(-50%,-50%) scale(0);
    }
    70% {
    opacity: 1;
    transform: translate(-50%,-50%) scale(0.9);
    }
    100% {
    transform: translate(-50%,-50%) scale(0.8);
    }

    }
    .yii-debug-toolbar {
        display: none!important;
    }

</style>
<div class="cashbacks-view">

    <div class="row">
        <div class="col-md-6">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-md-6">
            <p>
                <?= Html::a('O`zgartirish', ['update', 'id' => $model->id], ['class' => 'pull-right btn btn-primary']) ?>
                <?= Html::a('O`chirish', ['delete', 'id' => $model->id], [
                    'class' => ' pull-right btn btn-danger',
                    'data' => [
                        'confirm' => 'Cashbackni o`chirib yuborishga rog`zimisz ?',
                        'method' => 'post',
                        'method' => 'post',
                    ],
                    'style' => 'margin-right:10px;',
                ]) ?>
            </p>
        </div>
    </div>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cashback',
        ],
    ]) ?>

     <table class='table table-bordered table-striped mt-20'>
        <thead>
        <tr>
            <th>#</th>
            <th>Mijoz ismi</th>
            <th>O`chirish</th>
        </tr>
        </thead>
        <tbody>
            <?php if (isset($cashback_client) && !empty($cashback_client)): ?>  
                <?php $i = 1; ?>
                <?php foreach ($cashback_client as $key => $value): ?>
                    <tr>
                        <td>
                            <?php echo $i; ?>
                        </td>
                        <td>
                            <?php echo base64_decode($value->client->full_name) ?>
                        </td> 
                        <td>
                            <a href='#' class='ml-3 delete_cashback' title='Delete' data-delete='<?php echo $value["id"] ?>'><span class='fa fa-trash'></span></a>
                        </td>
                    </tr>
                <?php $i++; ?>
                <?php endforeach ?>
            <?php else: ?>
                <tr>
                <td class='text-center' colspan='3'>Ma'lumot yo'q</td>
                </tr>
            <?php endif ?>    
        </tbody>
    </table>


    <?php if (isset($clients) && !empty($clients)): ?>
        
        <div class="row">
            <h4 style="padding-left: 20px;">Foydalanuvchilarga cashback kiritish</h4>
            <?php $form = ActiveForm::begin(['action' => '/cashbacks/create-user']) ?>

                <div class="col-md-6">
                    <div class="form-group">
                        <select name="client_id[]" multiple="multiple" class="form-control select2" id="category" data-placeholder="Kategoriya tanlang" style="width: 100%">
                            <?php foreach ($clients as $key => $value): ?>
                                <option value="<?php echo $value["id"] ?>">
                                    <?php
                                        echo base64_decode($value["full_name"]);
                                    ?>
                                </option>
                            <?php endforeach ?>
                        </select>
                    </div>        
                </div>
                <div class="col-md-6" style="display: flex; align-items: center;">
                    <label style="margin: 0; padding-right: 5px;" for="all">Hamma uchun</label>
                    <input class="input" type="hidden" name="Cashback" value="<?php echo $id; ?>">
                    <input class="input" name="All" id="all" type="checkbox">
                </div>        
                <div class="col-md-12">
                    <button class="btn btn-success">
                        Saqlash
                    </button>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    <?php endif ?>


</div>
