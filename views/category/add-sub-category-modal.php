<div class="form-group">
    <label class="control-label" for="title_uz">Tovar nomi</label>
    <input type="text" id="title_uz" class="form-control">
    <small class='hidden name_error'>Maydon bo`sh bo`lishi mumkin emas</small>
    <br>
    <label class="control-label" for="title_ru">Tovar nomi ru</label>
    <input type="text" id="title_ru" class="form-control">
    <small class='hidden name_error_ru'>Maydon bo`sh bo`lishi mumkin emas</small>
    <div class="help-block"></div>
</div>
<style type="text/css">
    .hidden{
        display: none;
    }
    .name_error {
        color: red;
        font-size:15px;

    }
    .name_error_ru {
        color: red;
        font-size:15px;
        
    }
    .border-red{
        border:1px solid red;
        border-radius:3px;
    }

</style>

