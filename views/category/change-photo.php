<?php  
    
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin(); ?>
        
        <?= $form->field($model, 'imageFile')->fileInput(['class' => '', 'multiple' => ''])->label(false)?>
    
    <div class="form-group">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>