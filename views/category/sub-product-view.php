
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\SubCategory;
/* @var $this yii\web\View */
/* @var $model app\models\Category */
// echo "<pre>";
// print_r($product);
// die();
?>
<div class="category-view row">
    <div class="col-md-6">
        <h3 style="margin: 0;">
            <?php 
                echo $sub_category->title_uz . " Sub Kategoriyasi";
            ?>
        </h3>    
    </div>
    <div class="row">
    	<div class="col-md-12" style="padding-left: 30px;padding-right: 30px;">
			<!-- /.box-body --> 
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						<b><?php echo $sub_category->title_uz ?></b> tegishli mahsulotlar
					</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body no-padding">
					<table class="table table-striped">
						<tr>
							<th style="width: 10px">#</th>
							<th>Mahsulot nomi</th>
							<th>Mahsulot nomi ru</th>
							<th>Narxi</th>
							<th>Soni</th>
							<th>Holati</th>
						</tr>
						<?php 
							$i = 1;
							foreach ($product as $key => $value) {
						?>
						<tr>
							<td><?php echo $i ?></td>	
							<td><?php echo $value->title_uz ?></td>	
							<td><?php echo $value->title_ru ?></td>	
							<td><?php echo $value->price ?></td>	
							<td>
								<?php
									if ($value->count) {
										echo $value->count;
									} else {
										echo '0';
									}
								?>
								
							</td>	
							<td> 
								<?php 
									if ($value->status == 1) { ?>
										<span class="badge bg-blue">Active</span>
									<?php }  else { ?>
										<span class="badge bg-secondary">Arxiv</span>
									<?php }
								?>
							</td>	
						</tr>
						<?php
							$i++; 
							}
						?>
					</table>
				</div>
			</div>
    	</div>
    </div>
</div>
