<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Yangilikar';
?>
<div class="news-index">
    <div class="row">
        <div class="col-md-6">
            <h3 style="margin: 0;"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-md-6">
            <p>
                <?= Html::a('Yangilik Kiritish', ['create'], ['class' => 'pull-right btn btn-success']) ?>
            </p>
        </div>
    </div>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'img',

                'format' => 'html',

                'label' => 'Rasm',

                'value' => function ($data) {

                    return Html::img("/web/uploads/".$data['file_id'], // folder need

                        ['width' => '80px']);
                },

            ],
            'title_uz',
            'title_ru',
            'description_uz:ntext',
            //'description_ru:ntext',
            //'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
