<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->title_uz;
\yii\web\YiiAsset::register($this);
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('O`zgatirish', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('O`chirish', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ma`lumotni o`chirib yuborishingizga ishonchingiz komilmi ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title_uz',
            [
                'attribute' => 'img',

                'format' => 'html',

                'label' => 'Rasm',

                'value' => function ($data) {

                    return Html::img("/web/uploads/".$data['file_id'], // folder need

                        ['width' => '80px']);
                },

            ],
            'description_uz:ntext',
        ],
    ]) ?>

</div>
