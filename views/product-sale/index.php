<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chegirmalar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-sale-index">
    <div class="row">
        <div class="col-md-6">
            <h1 style="margin: 0;"><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-md-6">
            <p>
                <?= Html::a('Chegirma kiritish', ['create'], ['class' => 'pull-right btn btn-success']) ?>
            </p>
        </div>
    </div>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [ 
                'label'  => 'Mahsulot nomi:',
                'value' => function ($data) {
                    return  $data->product->title_uz;
                }
            ],
            [ 
                'label'  => 'Chegirma summasi:',
                'value' => function ($data) {
                    return  number_format($data->sale_price,0," "," ")." so'm";
                }
            ],
            [ 
                'label'  => 'Chegirma foizi',
                'value' => function ($data) {
                    return round($data->sale_pracent);
                }
            ],
            [ 
                'label'  => 'Chegirma boshlanish vaqti',
                'value' => function ($data) {
                    return $date = date("Y-m-d H:i",strtotime($data->enter_date));
                }
            ],
            [ 
                'label'  => 'Chegirma tugash vaqti',
                'value' => function ($data) {
                    return $date = date("Y-m-d H:i",strtotime($data->end_date));
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update} {delete}'
            ],
        ],
    ]); ?>


</div>
