<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$checked_details = [];
$this->title = 'Rasm biriktirish';
?>
<div class="add-img">

    <h1><?= Html::encode($this->title) ?></h1>
   
    <?=
	 $this->render('_form_img', [
        'model' => $model,
    ]); ?>

</div>
