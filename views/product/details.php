<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SubCategory;
/* @var $this yii\web\View */
/* @var $model app\models\Products */

$checked_details = [];
$this->title = 'Qo`shimcha Mahsulot Qo`shish';
?>
<div class="products-create">

    <h3><?= Html::encode($this->title) ?></h3>
        
        <?= $this->render('add_details', [
            'id' => $id,
            'details' => $details,
            'checked_details' => $checked_details   
        ]) ?>
        
</div>
