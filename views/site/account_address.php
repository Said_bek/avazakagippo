<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

?>
<div class="holder breadcrumbs-wrap mt-0">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="index.html">
                <?php 
                    echo Yii::t("app","home");
                ?>
            </a></li>
            <li><span>
                <?php 
                    echo Yii::t("app","my_cabinet");
                ?>
            </span></li>
        </ul>
    </div>
</div>
<div class="holder">
    <div class="container">
        <div class="row">
            <div class="col-md-4 aside aside--left">
                <div class="list-group">
                    <a href="account-details" class="list-group-item">
                        <?php 
                            echo Yii::t("app","my_informations");
                        ?>
                    </a>
                    <a href="account-address" class="list-group-item active">
                        <?php 
                            echo Yii::t("app","my_address")
                        ?>
                    </a>
                    <a href="account-wishlist" class="list-group-item">
                        <?php 
                            echo Yii::t("app","favorite_products");
                        ?>
                    </a>
                    <a href="account-history" class="list-group-item">
                        <?php 
                            echo Yii::t("app","order_history");
                        ?>
                    </a>  
                    <a href="account-referal" class="list-group-item">
                        <?php 
                            echo Yii::t("app","referal");
                        ?>
                    </a>  
                </div>
            </div>
            <div class="col-md-14 aside">
                <?php if (isset($client_address) and !empty($client_address)): ?>
                    <div class="col-md-14 aside">
                        <h1 class="mb-3">My Addresses</h1>
                        <div class="row">
                        <?php foreach ($client_address as $key => $value): ?>
                            <div class="col-sm-9">
                                <div class="card">
                                    <div class="card-body">
                                        <p><?php echo $value->region; ?>
                                            <br><?php echo $value["address"] ?></p>
                                        <div class="mt-2 clearfix">
                                            <a href="#" class="link-icn js-show-form" data-form="#updateAddress_<?php echo $value["id"] ?>"><i
                                                    class="icon-pencil"></i>
                                                    <?php echo Yii::t("app","update_address") ?>
                                                </a>
                                            <?php if (Yii::$app->language == "uz"): ?>
                                                <?php  $data_lang = 'uz'; ?> 
                                            <?php else: ?>
                                                <?php  $data_lang = 'ru'; ?> 

                                            <?php endif ?>
                                            <a  data-lang="<?php echo $data_lang ?>" type="button" id="delete_address_form" data-id="<?php echo $value["id"] ?>" class="link-icn ml-1 float-right"><i
                                                    class="icon-cross"></i>
                                                <?php 
                                                    echo Yii::t("app","delete");
                                                ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                        </div>
                        <?php foreach ($client_address as $key => $value): ?>
                            <div class="card mt-3 d-none" id="updateAddress_<?php echo $value["id"] ?>">
                                <div class="card-body">
                                    <h3>
                                        <?php echo Yii::t("app","create_address") ?>
                                    </h3>
                                    <label class="text-uppercase">
                                        <?php echo Yii::t("app","region") ?>
                                    </label>
                                    
                                    <?php $form = ActiveForm::begin(['action' => '/site/update-address'],'post'); ?>
                                        <?php 
                                            echo $form->field($model, 'client_id')->hiddenInput(['value'=> $client_id])->label(false);
                                            echo $form->field($model, 'address_id')->hiddenInput(['value'=> $value["id"]])->label(false);

                                        ?>
                                        <?php 
                                            if (Yii::$app->language == "uz") {
                                                echo $form->field($model, 'region_id')
                                                    ->dropDownList($region_uz,
                                                        ['options' =>
                                                            [                        
                                                              $value["region_id"] => ['selected' => true]
                                                            ]
                                                        ])->label(false);
                                            } else {
                                                echo $form->field($model, 'region_id')
                                                    ->dropDownList($region_ru)->label(false);
                                            }
                                        ?>
                                        <?= 
                                            $form->field($model, 'address')
                                            ->textInput(["value" => $value["address"],'maxlength' => 255, 'class' => 'form-control','id' => "empty_address_check"])
                                            ->label(Yii::t("app","full_address_text"),['class'=>'text-uppercase'])
                                        ?>
                                        <p class="d-none text-danger" id="input_emty_text">
                                            <?php echo Yii::t("app","cantblank") ?>
                                        </p>
                                        <div class="form-group">
                                            <button type="reset" class="btn btn--alt js-close-form" data-form="#updateAddress_<?php echo $value["id"] ?>">
                                                    <?php echo Yii::t("app","cancel"); ?>
                                            </button>
                                            <!-- <button type="submit" class="btn ml-1"></button> -->
                                            <?php 
                                            $label = Yii::t("app","full_address_text");
                                             ?>
                                            <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-primary update_address_form'])  ?>
                                        </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        <?php endforeach ?>

                    </div>
                <?php endif ?>
                <div class="row">
                    <div class="col-sm-9">
                        <div class="card" style="border:none!important;">
                            <div class="mt-2 clearfix">
                                <a href="#" class="link-icn js-show-form btn btn-primary" data-form="#createAddress"><i
                                        class="icon-pencil"></i>
                                    <?php 
                                        echo Yii::t("app","create");
                                    ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-3 d-none" id="createAddress">
                    <div class="card-body">
                        <h3>
                            <?php echo Yii::t("app","create_address") ?>
                        </h3>
                        <label class="text-uppercase">
                            <?php echo Yii::t("app","region") ?>
                        </label>
                        
                        <?php $form = ActiveForm::begin(['action' => '/site/create-address'],'post'); ?>
                            <?php 
                                echo $form->field($model, 'client_id')->hiddenInput(['value'=> $client_id])->label(false);
                            ?>
                            <?php 
                                if (Yii::$app->language == "uz") {
                                    echo $form->field($model, 'region_id')
                                        ->dropDownList($region_uz)->label(false);
                                } else {
                                    echo $form->field($model, 'region_id')
                                        ->dropDownList($region_ru)->label(false);
                                }
                            ?>
                            <?= $form->field($model, 'address')
                                        ->textInput(['maxlength' => 255, 'class' => 'form-control','id' => "empty_address_check"])
                                        ->label(Yii::t("app","full_address_text"),['class'=>'text-uppercase']) 
                            ?>
                            <p class="d-none text-danger" id="input_emty_text">
                                <?php echo Yii::t("app","cantblank") ?>
                            </p>
                            <div class="form-group">
                                <button type="reset" class="btn btn--alt js-close-form" data-form="#createAddress">
                                        <?php 
                                            echo Yii::t("app","cancel");
                                        ?>
                                </button>
                                <!-- <button type="submit" class="btn ml-1"></button> -->
                                <?php 
                                $label = Yii::t("app","full_address_text");
                                 ?>
                                <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-primary create_address'])  ?>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
