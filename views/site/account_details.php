<div class="holder breadcrumbs-wrap mt-0">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="index.html">
                <?php 
                    echo Yii::t("app","home");
                ?>
            </a></li>
            <li><span>
                <?php 
                    echo Yii::t("app","my_cabinet");
                ?>  
            </span></li>
        </ul>
    </div>
</div>
<div class="holder">
    <div class="container">
        <div class="row">
            <div class="col-md-4 aside aside--left">
                <div class="list-group">
                    <a href="account-details" class="list-group-item active">
                    	<?php 
                    		echo Yii::t("app","my_informations");
                    	?>
                    </a>
                    <a href="account-address" class="list-group-item">
                    	<?php 
                    		echo Yii::t("app","my_address");
                    	?>
                    </a>
                    <a href="account-wishlist" class="list-group-item">
                    	<?php 
                    		echo Yii::t("app","favorite_products");
                    	?>
                    </a>
                    <a href="account-history" class="list-group-item">
                    	<?php 
                    		echo Yii::t("app","order_history");
                    	?>
                    </a>	
                    <a href="account-referal" class="list-group-item">
                        <?php 
                            echo Yii::t("app","referal");
                        ?>
                    </a>
                </div>
            </div>
            <div class="col-md-14 aside">
                <h1 class="mb-3">
                	<?php echo Yii::t("app","my_informations"); ?>
                </h1>
                <div class="row vert-margin">
                    <div class="col-sm-9">
                        <div class="card">
                            <div class="card-body">
                                <h3>
                                	<?php 
                                		echo Yii::t("app","personal_info");
                                	?>
                                </h3>
                                <p><b><?php echo Yii::t("app","first_name") ?>:</b> <?php echo $name ?><br>
                                    <b><?php echo Yii::t("app","last_name") ?>:</b> <?php echo $last_name ?><br>
                                    <!-- <b>E-mail:</b> jennyraider@hotmail.com<br> -->
                                    <b><?php echo Yii::t("app","phone_number") ?>:</b> <?php echo $phone_number ?></p>
                                <div class="mt-2 clearfix">
                                    <a href="#" class="link-icn js-show-form" data-form="#updateDetails"><i
                                            class="icon-pencil"></i><?php echo Yii::t("app","update"); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-3 d-none" id="updateDetails">
                    <div class="card-body">
                        <h3>
                        	<?php 
                        		echo Yii::t("app","update_informations");
                        	?>
                        </h3>
                        
                        <div class="row mt-2">
                            <div class="col-sm-9">
                                <label class="text-uppercase">
                                	<?php echo Yii::t("app","first_name") ?>:
                            	</label>
                                <div class="form-group">
                                    <input type="text" id="name_account" name="name" class="form-control form-control--sm" value="<?php echo $name ?>">
                                    <input type="hidden" id="id_account" name="client_id" value="<?php echo $client_id ?>">
                                    <div class="invalid-feedback-last-empty d-none text-danger" id="name_error">
										<?php echo Yii::t('app', 'cantblank'); ?>
									</div>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <label class="text-uppercase">
                                	<?php echo Yii::t("app","last_name") ?>:
                                </label>
                                <div class="form-group">
                                    <input type="text" name="last_name" id="last_name_account" class="form-control form-control--sm" value="<?php echo $last_name ?>">
                                    <div class="invalid-feedback-last-empty d-none text-danger" id="last_name_error">
										<?php echo Yii::t('app', 'cantblank'); ?>
									</div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-9">
                                <label class="text-uppercase">
                                	<?php echo Yii::t("app","phone_number") ?>:
                                </label>
                                <div class="form-group">
                                       <input id="number_account" name="phone_number" autocomplete="off" class="form-control js-phone-mask" id="tel3" data-tel="+998" value="<?php echo $phone_number ?>">
                                        <div class="invalid-feedback-length d-none text-danger" id="number_error">
											<?php echo Yii::t('app', 'phone_number_failure'); ?>
										</div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-2">
                            <button type="reset" class="btn btn--alt js-close-form" data-form="#updateDetails">
                            	<?php echo Yii::t("app","cancel") ?>
                            </button>
                            <button id="update_account" type="submit" class="btn ml-1">
                            	<?php echo Yii::t("app","update") ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>