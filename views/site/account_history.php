<div class="holder breadcrumbs-wrap mt-0">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="index.html">
            	<?php 
            		echo Yii::t("app","home");
            	?>
            </a></li>
            <li><span>
            	<?php 
                    echo Yii::t("app","my_cabinet");
                ?>
            </span></li>
        </ul>
    </div>
</div>
<div class="holder">
    <div class="container">
        <div class="row">
            <div class="col-md-4 aside aside--left">
                <div class="list-group">
                    <a href="account-details" class="list-group-item">
                    	<?php 
                    		echo Yii::t("app","my_informations");
                    	?>
                    </a>
                    <a href="account-address" class="list-group-item">
                    	<?php 
                    		echo Yii::t("app","my_address")
                    	?>
                    </a>
                    <a href="account-wishlist" class="list-group-item">
                    	<?php 
                    		echo Yii::t("app","favorite_products");
                    	?>
                    </a>
                    <a href="account-history" class="list-group-item active">
                    	<?php 
                    		echo Yii::t("app","order_history");
                    	?>
                    </a>
                    <a href="account-referal" class="list-group-item">
                        <?php 
                            echo Yii::t("app","referal");
                        ?>
                    </a>	
                </div>
            </div>
            <div class="col-md-14 aside">
                <h1 class="mb-3">
                    <?php 
                        echo Yii::t("app","order_history");
                    ?>
                </h1>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-order-history">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">
                                <?php 
                                    echo Yii::t("app","order_number");
                                ?>
                            </th>
                            <th scope="col">
                                <?php 
                                    echo Yii::t("app","order_date");
                                ?>
                            </th>
                            <th scope="col">
                                <?php 
                                    echo Yii::t("app","status_pay");
                                ?>
                            </th>
                            <th scope="col">
                                <?php 
                                    echo Yii::t("app","total_price");
                                ?>
                            </th>
                            <th scope="col">
                                <?php 
                                    echo Yii::t("app","status_order");
                                ?>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($model as $key => $value): ?>
                            <tr>
                                <td>
                                    <?php echo $i; ?>
                                </td>
                                <td>
                                    <b><?php echo $value["order_code"] ?></b>
                                    <a href="/site/cart?order_code=<?php echo $value["order_code"] ?>" class="ml-1">
                                        <?php echo Yii::t("app","view_details") ?>
                                    </a>
                                </td>
                                <td>
                                    <?php echo date("d.m.Y",strtotime($value["created_date"])) ?>
                                </td>
                                <td>
                                    <?php 
                                        if ($value["billing_status"] == 4) { 
                                            echo Yii::t("app","success_full"); 
                                        } else if ($value["billing_status"] == 3) {
                                            echo Yii::t("app","success_full").  "  3";
                                        }  else if ($value["billing_status"] == 2) {
                                            echo Yii::t("app","success_full").  "  2";
                                        } else if ($value["billing_status"] == 1) {
                                            echo Yii::t("app","success_full").  "  1";
                                        }
                                    ?>
                                </td>
                                <td>
                                    <span class="color">
                                        <?php if (isset($_SESSION['currency']) && $_SESSION['currency'] == 'usd'): ?>
                                            <?php echo Yii::t("app","usd") ?>    
                                        <?php endif ?>
                                            <?php echo number_format($value->total,0,'',' ') ?>
                                        <?php if (!isset($_SESSION['currency']) || $_SESSION['currency'] == 'uzs'): ?>
                                        <?php echo Yii::t("app","sum") ?>    
                                    <?php endif ?>
                                    </span>
                                </td>
                                <td>
                                    <a href="#" class="prevent_btn btn btn--grey btn--sm">
                                        <?php
                                            if ($value->stage == 1) {
                                                echo Yii::t("app","accepted"); 
                                            } else if ($value->stage == 2) {
                                                echo Yii::t("app","make_order"); 
                                            } else if ($value->stage == 3) {
                                                echo Yii::t("app","order_has_arrived"); 
                                            } else if ($value->stage == 4) {
                                                echo Yii::t("app","order_has_been_delivered"); 
                                            } else if ($value->stage == 5) {
                                                echo Yii::t("app"," order_has_been_delivered"); 
                                            } 
                                        ?>
                                    </a>
                                </td>
                            </tr>
                        <?php $i++; ?>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>