<?php 
    use app\models\Products;
    use app\models\ProductDetailsCode;
?>
<div class="holder breadcrumbs-wrap mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="index.html">
                    <?php 
                        echo Yii::t("app","home");
                    ?>
                </a></li>
                <li><span>
                    <?php 
                        echo Yii::t("app","my_cabinet");
                    ?>
                </span></li>
            </ul>
        </div>
    </div>
    <div class="holder">
        <div class="container">
            <div class="row">
                <div class="col-md-4 aside aside--left">
                    <div class="list-group">
                        <a href="account-details" class="list-group-item">
                            <?php 
                                echo Yii::t("app","my_informations");
                            ?>
                        </a>
                        <a href="account-address" class="list-group-item">
                            <?php 
                                echo Yii::t("app","my_address");
                            ?>
                        </a>
                        <a href="account-wishlist" class="list-group-item active">
                            <?php 
                                echo Yii::t("app","favorite_products");
                            ?>
                        </a>
                        <a href="account-history" class="list-group-item">
                            <?php 
                                echo Yii::t("app","order_history");
                            ?>
                        </a>
                        <a href="account-referal" class="list-group-item">
                            <?php 
                                echo Yii::t("app","referal");
                            ?>
                        </a>
                    </div>
                </div>
                <div class="col-md-14 aside">
                    <h1 class="mb-3">My Wishlist</h1>
                    <div class="empty-wishlist js-empty-wishlist text-center py-3 py-sm-5 d-none" style="opacity: 0;">
                        <h3>Your Wishlist is empty</h3>
                        <div class="mt-5">
                            <a href="index.html" class="btn">Continue shopping</a>
                        </div>
                    </div>
                    <div class="prd-grid-wrap position-relative">
                        <div class="prd-grid prd-grid--wishlist data-to-show-3 data-to-show-lg-3 data-to-show-md-2 data-to-show-sm-2 data-to-show-xs-1">
                            <!-- prd prd--in-wishlist prd--style2 prd-labels--max prd-labels-shadow -->
                            <?php foreach ($products as $key => $value): ?>
                                    <?php $product_block = Products::getByDetailId($color_id,$value["id"]); ?>
                                    <div class="prd prd--style2 prd-labels--max prd-labels-shadow ">
                                        <div class="prd-inside">
                                            <div class="prd-img-area">
                                                <a href="product.html" class="prd-img image-hover-scale image-container">
                                                    <img src="data:/uploads/<?php echo $value["img"] ?>" data-src="/uploads/<?php echo $value["img"] ?>" alt="Oversized Cotton Blouse" class="js-prd-img lazyload fade-up">
                                                    <div class="foxic-loader"></div>
                                                    <div class="prd-big-squared-labels">
                                                        
                                                        <?php if ($value["status"] == 3 or $value["status"] == 4): ?>
                                                            <div class="label-new">
                                                                <span>
                                                                    <?php if (Yii::$app->language == 'uz'): ?>
                                                                        Yangi
                                                                    <?php else: ?>
                                                                        Новый
                                                                    <?php endif ?>
                                                                </span>
                                                            </div>
                                                        <?php endif ?>
                                                        <?php $sale_product = $value->sale; ?>
                                                        <?php if (isset($sale_product) and !empty($sale_product)): ?>
                                                            
                                                            <div class="label-sale">
                                                                <span><?php echo "-".round($sale_product["sale_pracent"])."%" ?>
                                                                    <span class="sale-text">
                                                                        <?php if (Yii::$app->language == 'uz'): ?>
                                                                            Chegirma
                                                                        <?php else: ?>
                                                                            Скидка
                                                                        <?php endif ?>
                                                                    </span>
                                                                </span>
                                                                
                                                                <div class="countdown-circle">
                                                                    <div class="countdown js-countdown" data-countdown="2021/07/01"></div>
                                                                </div>
                                                            </div>
                                                        <?php endif ?>
                                                    </div>
                                                </a>
                                                <div class="prd-circle-labels">
                                                    <a href="#" data-id="<?php echo $value->id ?>" id="left_from_wishlist" class="circle-label-compare circle-label-wishlist js-remove-wishlist" title="Remove From Wishlist">
                                                        <i class="icon-recycle"></i>
                                                    </a>
                                                    <a href="#" class="circle-label-qview js-prd-quickview prd-hide-mobile" data-src="ajax/ajax-quickview.html">
                                                        <i class="icon-eye"></i><span>QUIsK VIEW</span>
                                                    </a>
                                                    <?php if (count($product_block) > 1): ?>
                                                        <div class="colorswatch-label colorswatch-label--variants js-prd-colorswatch">
                                                            <i class="icon-palette"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                                                            <ul>
                                                                <?php foreach ($product_block as $key2 => $value2): ?>
                                                                    <?php $product_img = ProductDetailsCode::MultiDetails($value2["product_id"],$value2["multi_detail_id"]); ?>
                                                                    <li data-image="/uploads/<?php if (isset($product_img)): ?><?php echo $product_img->img ?><?php endif ?>">
                                                                        <a style="background: <?php echo $value2->multi_one->color_name; ?>;" class="js-color-toggle" data-toggle="tooltip" data-placement="left" title="Color Name">
                                                                            <!-- <img src="asset/images/colorswatch/color-green.png" alt=""> -->
                                                                        </a>
                                                                    </li>
                                                                <?php endforeach ?>
                                                                
                                                            </ul>
                                                        </div>
                                                    <?php endif ?>
                                                </div>

                                                
                                                <ul class="list-options color-swatch">
                                                    <?php $i = 1; ?>
                                                    <?php foreach ($value->imgs as $key2 => $value2): ?>
                                                        <?php if ($i == 1): ?>
                                                            <li data-image="/uploads/<?php echo$value2["img"]  ?>" class="active"><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:/uploads/<?php echo$value2["img"]  ?>" data-src="/uploads/<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                                                        <?php else: ?>
                                                            <li data-image="/uploads/<?php echo$value2["img"]  ?>" class=><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:/uploads/<?php echo$value2["img"]  ?>" data-src="/uploads/<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>

                                                        <?php endif ?>
                                                        <?php $i++; ?>
                                                    <?php endforeach ?>

                                                </ul>
                                            </div>
                                            <div class="prd-info">
                                                <div class="prd-info-wrap">
                                                    <div class="prd-info-top">
                                                        <div class="prd-rating"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                                    </div>
                                                    <?php $stars =  $value->stars; ?>
                                                    <div class="prd-rating justify-content-center">
                                                        <?php for ($i = 1; $i <= $stars; $i++): ?>
                                                            <i class="icon-star-fill fill"></i>
                                                        <?php endfor ?>
                                                    </div>
                                                    <div class="prd-tag"><a href="#">
                                                        <?php if (Yii::$app->language == "uz"): ?>
                                                                <?php echo $value->subCategory->title_uz ?>
                                                            <?php else: ?>
                                                                <?php echo $value->subCategory->title_ru ?>
                                                        <?php endif ?>
                                                    </a></div>
                                                    <h2 class="prd-title"><a href="product.html">
                                                        <?php if (Yii::$app->language == "uz"): ?>
                                                            <?php echo $value["title_uz"] ?>
                                                        <?php else: ?>
                                                            <?php echo $value["title_ru"] ?>
                                                        <?php endif ?>
                                                    </a></h2>
                                                    <div class="prd-description">
                                                        Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante sed lacinia.
                                                    </div>
                                                    <div class="prd-action">
                                                        <form action="#">
                                                            <button class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                                                <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="prd-hovers">
                                                    <div class="prd-circle-labels">
                                                        <div><a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a></div>
                                                        <div class="prd-hide-mobile"><a href="#" class="circle-label-qview js-prd-quickview" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUICK VIEW</span></a></div>
                                                    </div>
                                                    <div class="prd-price">
                                                        <?php $product_sale = $value->sale; ?>
                                                        <?php if (isset($product_sale) and !empty($product_sale)): ?>
                                                            <div class="price-old">
                                                                    <?php if (isset($_SESSION['currency']) && $_SESSION['currency'] == 'usd'): ?>
                                                                            <?php echo Yii::t("app","usd") ?>    
                                                                    <?php endif ?>
                                                                            <?php echo number_format($value['price'],0,'',' ') ?>
                                                                    <?php if (!isset($_SESSION['currency']) || $_SESSION['currency'] == 'uzs'): ?>
                                                                    <?php echo Yii::t("app","sum") ?>    
                                                                <?php endif ?>
                                                            </div>
                                                            <div class="price-new">
                                                                <?php if (isset($_SESSION['currency']) && $_SESSION['currency'] == 'usd'): ?>
                                                                        <?php echo Yii::t("app","usd") ?>    
                                                                <?php endif ?>
                                                                    <?php echo number_format($product_sale["sale_price"],0,'',' ')  ?>
                                                                <?php if (!isset($_SESSION['currency']) || $_SESSION['currency'] == 'uzs'): ?>
                                                                    <?php echo Yii::t("app","sum") ?>    
                                                                <?php endif ?>
                                                            </div>
                                                            
                                                            <?php else: ?>
                                                            
                                                            <div class="price-new">
                                                                <?php if (isset($_SESSION['currency']) && $_SESSION['currency'] == 'usd'): ?>
                                                                        <?php echo Yii::t("app","usd") ?>    
                                                                <?php endif ?>
                                                                        <?php echo number_format($value['price'],0,'',' ') ?>
                                                                <?php if (!isset($_SESSION['currency']) || $_SESSION['currency'] == 'uzs'): ?>
                                                                    <?php echo Yii::t("app","sum") ?>    
                                                                <?php endif ?>
                                                            </div>
                                                        <?php endif ?>
                                                    </div>
                                                    <div class="prd-action">
                                                        <div class="prd-action-left">
                                                            <form action="#">
                                                                <button class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                                                <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>