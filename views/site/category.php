<?php 
    use app\models\Products;
    use app\models\ProductDetailsCode;
    // echo $session["list"]["sql"];
    
    $session = Yii::$app->session;
    
 ?>
<div class="page-content">
	<div class="holder breadcrumbs-wrap mt-0">
    	<div class="container">
    		<ul class="breadcrumbs">
    			<li><a href="index.html">Home</a></li>
    			<li><span>Category</span></li>
    		</ul>
    	</div>
    </div>
    <div class="holder holder-mt-medium">
    	<div class="container-fluid">
    		<div class="row vert-margin-small">
                <?php foreach ($category as $key => $value): ?>
                    <div class="col-sm">
                        <a type="button" class="collection-grid-3-item image-hover-scale">
                            <div class="collection-grid-3-item-img image-container" style="padding-bottom: 68.22%">
                                <img src="data:site/<?php echo $value["imgs"] ?>" data-src="site/<?php echo $value["imgs"] ?>" class="lazyload fade-up" alt="Banner">
                                <div class="foxic-loader"></div>
                            </div>
                            <div class="collection-grid-3-caption-bg">
                                <h3 class="collection-grid-3-title">
                                    <?php if (Yii::$app->language == "uz"): ?>
                                        <?php echo $value["title_uz"] ?>
                                    <?php else: ?>
                                        <?php echo $value["title_ru"] ?>
                                    <?php endif ?>
                                </h3>
                                <!-- <h4 class="collection-grid-3-subtitle">The&nbsp;Best&nbsp;Look&nbsp</h4> -->
                            </div>
                        </a>
                    </div>    
                <?php endforeach ?>
    		</div>
    	</div>
    </div>
    <div class="holder">
        <div class="container">
            <!-- Two columns -->
            <!-- Page Title -->
            <div class="page-title text-center">
                <h1>WOMEN’S</h1>
            </div>
            <!-- /Page Title -->
            <!-- Filter Row -->
            <div class="filter-row">
                <div class="row">
                    <div class="items-count">
                        <?php if (isset($session["list"]["product"]) and !empty($session["list"]["product"])): ?>
                            <?php echo count($session["list"]["product"])." item(s)" ?>
                        <?php endif ?>
                    </div>
                    <div class="select-wrap d-none d-md-flex">
                        <div class="select-label">SORT:</div>
                        <div class="select-wrapper select-wrapper-xxs">
                            <select class="form-control input-sm" id="type">
                                <?php $session = Yii::$app->session; ?>
                                <?php 
                                    $first = '';
                                    $second = '';
                                    $thirst = '';
                                    if (isset($session["list"]["select"]) and !empty($session["list"]["select"])) {
                                        if ($session["list"]["select"] == 1) {
                                            $first = "selected";
                                        } else if ($session["list"]["select"] == 2) {
                                            $second = 'selected';
                                        } else if ($session["list"]["select"] == 3) {
                                            $thirst = 'selected';
                                        }
                                    } else {
                                        $first = "selected";
                                    }
                                ?>
                            
                                <option <?php echo $first ?> value="new">
                                    <?php echo Yii::t('app', 'new_products'); ?>
                                </option>
                                <option <?php echo $second ?> value="trend">
                                    <?php echo Yii::t('app', 'trend_products'); ?>
                                </option>
                                <option <?php echo $thirst ?> value="price">
                                    <?php echo Yii::t('app', 'price_products'); ?>
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="select-wrap d-none d-md-flex">
                        <div class="select-label">VIEW:</div>
                        <div class="select-wrapper select-wrapper-xxs">
                            <select class="form-control input-sm" id="limit">
                                <option value="12">12</option>
                                <option value="36">36</option>
                                <option value="100">100</option>
                            </select>
                        </div>
                    </div>
                    <div class="viewmode-wrap">
                        <div class="view-mode">
                            <span class=" js-horview d-none d-lg-inline-flex">
                                <i id="tab" class="color color-icon icon-grid"></i>
                            </span>
                            <span class="js-gridview">
                                <i id="tab1" class="color-icon icon-grid"></i>
                            </span>
                            <span class="js-listview">
                                <i id="tab2" class="color-icon icon-list"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Filter Row -->
            <div class="row">
                <!-- Left column -->
                <div class="product-listing data-to-show-3 data-to-show-md-3 data-to-show-sm-2 js-category-grid prd-horgrid"
                     data-grid-tab-content>
                    <form class="current_selected">
                        <div class="filter-col-content filter-mobile-content">
                            <div class="sidebar-block"> 
                                    <div class="sidebar-block_title">
                                        <span>Current selection</span>
                                    </div>
                                    <div class="sidebar-block_content sidebar_block">
                                        <div class="selected-filters-wrap">
                                            <ul class="selected-filters" id="ul_selected" style="width:300px!important;">
                                                  
                                            </ul>
                                            <div class="d-flex flex-wrap align-items-center">
                                                <a  class="clear-filters" id="clear_filter"><span>Clear All</span></a>
                                                <div class="selected-filters-count ml-auto d-none d-lg-block">Selected <span>6 items</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="sidebar-block d-filter-mobile">
                                <h3 class="mb-1">SORT BY</h3>
                                <div class="select-wrapper select-wrapper-xs">
                                    <select class="form-control">
                                        <option value="featured">Featured</option>
                                        <option value="rating">Rating</option>
                                        <option value="price">Price</option>
                                    </select>
                                </div>
                            </div>
                            <div class="sidebar-block filter-group-block open">
                                <div class="sidebar-block_title category_close">
                                    <span>Categories</span>
                                    <span class="toggle-arrow"><span></span><span></span></span>
                                </div>
                                <div class="sidebar-block_content sidebar_block">
                                    <ul class="category-list">
                                        <?php foreach ($category as $key => $value): ?>
                                            <li class="active">
                                                <a type="button" title="Casual" class="open" id="category" data-id="<?php echo $value["id"] ?>">
                                                    <?php if (Yii::$app->language == "uz"): ?>
                                                         <?php echo $value["title_uz"] ?>
                                                     <?php else: ?>
                                                        <?php echo $value["title_uz"] ?>
                                                     <?php endif ?> &nbsp;
                                                    <span>
                                                        (
                                                            <?php echo $value->count ?>
                                                        )
                                                    </span>
                                                </a>
                                                <div class="toggle-category js-toggle-category">
                                                    <span>
                                                        <i class="category_close icon-angle-down"></i>
                                                    </span>
                                                </div>
                                                <?php foreach ($value->subtwo as $key2 => $value2): ?>
                                                    <ul id="list_sub" class="category-list category-list">
                                                        <?php if (isset($value2->subcategory) and !empty($value2->subcategory)): ?>
                                                            <ul class="category-list" style="position: relative; padding:4px 0 0 0!important;">
                                                                <a type="button" title="Casual" class="open" id="sub" data-id="<?php echo $value2["id"] ?>">
                                                                    <?php if (Yii::$app->language == "uz"): ?>
                                                                         <?php echo $value2["title_uz"] ?>
                                                                     <?php else: ?>
                                                                        <?php echo $value2["title_uz"] ?>
                                                                     <?php endif ?> &nbsp;
                                                                    <span>
                                                                        (<?php echo $value2->count ?>)
                                                                    </span>
                                                                </a>
                                                                <div class="toggle-category js-toggle-category">
                                                                    <span>
                                                                        <i class="category_close icon-angle-down"></i>
                                                                    </span>
                                                                </div>
                                                                <ul id="list_sub" class="category-list category-list">
                                                                    <?php foreach ($value2->subcategory as $key3 => $value3): ?>
                                                                        <li>
                                                                            <a type="button" title="Men" id="sub_secondary" data-id="<?php echo $value3["id"] ?>">
                                                                                <?php if (Yii::$app->language == "uz"): ?>
                                                                                    <?php echo $value3["title_uz"] ?>
                                                                                <?php else: ?>
                                                                                    <?php echo $value3["title_ru"] ?>
                                                                                <?php endif ?> &nbsp;<span>(<?php echo $value3->count; ?>)</span></a>
                                                                        </li>
                                                                    <?php endforeach ?>
                                                                </ul>
                                                            </ul>                                            
                                                        <?php else: ?>
                                                            <li>
                                                                <a type="button" title="Men" id="sub" data-id="<?php echo $value2["id"] ?>">
                                                                    <?php if (Yii::$app->language == "uz"): ?>
                                                                        <?php echo $value2["title_uz"] ?>
                                                                    <?php else: ?>
                                                                        <?php echo $value2["title_ru"] ?>
                                                                    <?php endif ?> &nbsp;<span>( <?php echo $value2->count ?> )</span></a>
                                                            </li>
                                                        <?php endif ?>

                                                    </ul>
                                                <?php endforeach ?>
                                            </li>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            </div>
                            <?php foreach ($details as $key => $value): ?>
                                <div class="sidebar-block filter-group-block collapsed ">
                                    <div class="sidebar-block_title category_open">
                                        <span>
                                            <?php if (Yii::$app->language == "uz"): ?>
                                                <?php echo $value["title_uz"] ?>
                                            <?php else: ?>
                                                <?php echo $value["title_ru"] ?>
                                            <?php endif ?>
                                        </span>
                                        <span class="toggle-arrow"><span></span><span></span></span>
                                    </div>
                                    <div class="sidebar-block_content sidebar_block ">
                                        <ul class="color-list two-column">
                                            <?php foreach ($value->multiDetails as $key2 => $value2): ?>
                                                <li class="active">
                                                    <a type="button" data-tooltip="Dark Red" title="Dark Red" id="multi_detail" data-id="<?php echo $value2["id"] ?>">
                                                        <?php if ($value["type"] == 3): ?> 
                                                            <span class="value" style="width: 20px;height: 20px;background: <?php echo $value2["color_name"]; ?>;">
                                                                
                                                            </span>
                                                            <span class="colorname">
                                                                <?php if (Yii::$app->language == "uz"): ?>
                                                                    <?php echo $value2["title_uz"] ?>
                                                                <?php else: ?>
                                                                    <?php echo $value2["title_ru"] ?>
                                                                <?php endif ?>
                                                            </span>
                                                        <?php else: ?>

                                                            <?php if (Yii::$app->language == "uz"): ?>
                                                                <?php echo $value2["title_uz"] ?>
                                                            <?php else: ?>
                                                                <?php echo $value2["title_ru"] ?>
                                                            <?php endif ?>
                                                        <?php endif ?>
                                                    </a>
                                                </li>
                                            <?php endforeach ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php endforeach ?>
                            

                            <a href="https://bit.ly/3eJX5XE" class="bnr image-hover-scale bnr--bottom bnr--left"
                               data-fontratio="3.95">
                                <div class="bnr-img">
                                    <img src="/asset/images/banners/banner-collection-aside.png" alt="">
                                </div>
                            </a>
                        </div>
                    </form>
                </div>
                <!-- filter toggle -->
              
                <!-- /Left column -->
                <!-- Center column -->
                <div class="col-lg aside">
                    <div class="prd-grid-wrap">
                        <!-- Products Grid -->
                        <div class="loader_swap">
                            <div id="tab_box" class="product-listing data-to-show-3 data-to-show-md-3 data-to-show-sm-2 js-category-grid prd-horgrid"   data-grid-tab-content>
                                <?php if (isset($session["list"]["product"]) and !empty($session["list"]["product"])): ?>
                                   <?php foreach ($session["list"]["product"] as $key => $value) { ?>
                                        <?php $product = Products::getByDetailId($color_id,$value->id); ?>
                                        <div class="prd prd--style2 prd-labels--max prd-labels-shadow ">
                                            <div class="prd-inside">
                                                <div class="prd-img-area">
                                                    <a href="product.html" class="prd-img image-hover-scale image-container">
                                                        <img src="<?php echo $value->img ?>" data-src="<?php echo $value->img ?>" alt="Midi Dress with Belt" class="js-prd-img lazyload fade-up">
                                                        <div class="foxic-loader"></div>
                                                        <div class="prd-big-squared-labels">
                                                            
                                                        </div>
                                                    </a>
                                                    <div class="prd-circle-labels">
                                                        <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                                        <a href="#" class="circle-label-qview js-prd-quickview prd-hide-mobile" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUIsK VIEW</span></a>
                                                        <?php if (count($product) > 1): ?>
                                                            <div class="colorswatch-label colorswatch-label--variants js-prd-colorswatch">
                                                                <i class="icon-palette"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                                                                <ul>
                                                                    <?php foreach ($product as $key2 => $value2): ?>
                                                                        <?php $product_img = ProductDetailsCode::MultiDetails($value2["product_id"],$value2["multi_detail_id"]); ?>
                                                                        <li data-image="<?php if (isset($product_img)): ?><?php echo $product_img->img ?><?php endif ?>">
                                                                            <a style="background: <?php echo $value2->multi_one->color_name; ?>;" class="js-color-toggle" data-toggle="tooltip" data-placement="left" title="Color Name">
                                                                                <!-- <img src="asset/images/colorswatch/color-green.png" alt=""> -->
                                                                            </a>
                                                                        </li>
                                                                    <?php endforeach ?>
                                                                    
                                                                </ul>
                                                            </div>
                                                        <?php endif ?>
                                                    </div>
                                                    
                                                    <ul class="list-options color-swatch">
                                                        <?php $i = 1; ?>
                                                        <?php foreach ($value->imgs as $key2 => $value2): ?>
                                                            <?php if ($i == 1): ?>
                                                                <li data-image="<?php echo$value2["img"]  ?>" class="active"><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                                                            <?php else: ?>
                                                                <li data-image="<?php echo$value2["img"]  ?>" class=><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>

                                                            <?php endif ?>
                                                            <?php $i++; ?>
                                                        <?php endforeach ?>
                                                    </ul>
                                                    
                                                </div>
                                                <div class="prd-info">
                                                    <div class="prd-info-wrap">
                                                        <div class="prd-info-top">
                                                            <div class="prd-rating"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                                        </div>
                                                        <div class="prd-rating justify-content-center"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                                        <div class="prd-tag"><a href="#">Seiko</a></div>
                                                        <h2 class="prd-title"><a href="product.html">
                                                            <?php if (Yii::$app->language == "uz"): ?>
                                                                <?php echo $value["title_uz"] ?>
                                                            <?php else: ?>
                                                                <?php echo $value["title_ru"] ?>
                                                            <?php endif ?>

                                                        </a></h2>
                                                        <div class="prd-description">
                                                            Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante sed lacinia.
                                                        </div>
                                                        <div class="prd-action">
                                                            <button class="btn js-prd-addtocart" product-id="<?php echo $value["id"] ?>">
                                                                <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="prd-hovers">
                                                        <div class="prd-circle-labels">
                                                            <div>
                                                                <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class
                                                                    ="icon-heart-stroke"></i>
                                                                </a>
                                                                <a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                                            </div>
                                                            <div class="prd-hide-mobile"
                                                            ><a href="#" class="circle-label-qview js-prd-quickview" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUICK VIEW</span></a></div>
                                                        </div>
                                                        <div class="prd-price">
                                                            
                                                            <div class="price-new"><?php price($value["price"]) ?></div>
                                                        </div>
                                                        <div class="prd-action">
                                                            <div class="prd-action-left">
                                                                <button class="btn js-prd-addtocart" product-id="<?php echo $value["id"] ?>">
                                                                    <?php echo Yii::t('app', 'add_to_cart'); ?>                 
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php endif ?>
                            </div>

                            <div id="tab_box" class="product-listing data-to-show-3 data-to-show-md-3 data-to-show-sm-2 js-category-grid prd-horgrid search_block_<?php echo $session["list"]["last_id"]; ?>"
                                 data-grid-tab-content></div>
                        </div>
                        <div class="loader-horizontal-sm js-loader-horizontal-sm d-none" data-loader-horizontal
                             style="opacity: 0;">
                             <span></span>
                        </div>
                        <div class="circle-loader-wrap">
                            <div class="circle-loader">
                                <a id="load_more_category" data-id="<?php if (isset($session["list"]["last_id"])): ?><?php echo $session["list"]["last_id"] ?><?php endif ?>">
                                    <svg id="svg_d" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="50%" cy="50%" r="63" fill="transparent"></circle>
                                        <circle class="js-circle-bar" cx="50%" cy="50%" r="63"
                                                fill="transparent"></circle>
                                    </svg>
                                    <svg id="svg_m" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="50%" cy="50%" r="50" fill="transparent"></circle>
                                        <circle class="js-circle-bar" cx="50%" cy="50%" r="50"
                                                fill="transparent"></circle>
                                    </svg>
                                    <div class="circle-loader-text">Load More</div>
                                    <div class="circle-loader-text-alt"><span class="js-circle-loader-start"></span>&nbsp;out
                                        of&nbsp;<span class="js-circle-loader-end"></span></div>
                                </a>
                            </div>
                        </div>
                        <!-- /Products Grid -->
                        <!--<div class="mt-2">-->
                        <!--<button class="btn" onclick="THEME.loaderHorizontalSm.open()">Show Small Loader</button>-->
                        <!--<button class="btn" onclick="THEME.loaderHorizontalSm.close()">Hide Small Loader</button>-->
                        <!--</div>-->
                        <!--<div class="mt-2">-->
                        <!--<button class="btn" onclick="THEME.loaderCategory.open()">Show Opacity</button>-->
                        <!--<button class="btn" onclick="THEME.loaderCategory.close()">Hide Opacity</button>-->
                        <!--</div>-->
                    </div>
                </div>
                <!-- /Center column -->
            </div>
            <!-- /Two columns -->
        </div>
    </div>
    <div class="holder">
        <div class="container">
            <div class="title-wrap text-center">
                <h2 class="h1-style">
                    <?php echo Yii::t("app","like_product") ?>
                </h2>
                <div class="carousel-arrows carousel-arrows--center"></div>
            </div>
            <div class="prd-grid prd-carousel js-prd-carousel slick-arrows-aside-simple slick-arrows-mobile-lg data-to-show-4 data-to-show-md-3 data-to-show-sm-3 data-to-show-xs-2"
                 data-slick='{"slidesToShow": 4, "slidesToScroll": 2, "responsive": [{"breakpoint": 992,"settings": {"slidesToShow": 3, "slidesToScroll": 1}},{"breakpoint": 768,"settings": {"slidesToShow": 2, "slidesToScroll": 1}},{"breakpoint": 480,"settings": {"slidesToShow": 2, "slidesToScroll": 1}}]}'>
                <?php foreach ($product_trend as $key => $value): ?>
                        <?php $product_block = Products::getByDetailId($color_id,$value["id"]); ?>
                        <div class="prd prd--style2 prd-labels--max prd-labels-shadow ">
                            <div class="prd-inside">
                                <div class="prd-img-area">
                                    <a href="product.html" class="prd-img image-hover-scale image-container">
                                        <img src="data:<?php echo $value["img"] ?>" data-src="<?php echo $value["img"] ?>" alt="Oversized Cotton Blouse" class="js-prd-img lazyload fade-up">
                                        <div class="foxic-loader"></div>
                                        <div class="prd-big-squared-labels">
                                            
                                            <?php if ($value["status"] == 3 or $value["status"] == 4): ?>
                                                <div class="label-new">
                                                    <span>
                                                        <?php if (Yii::$app->language == 'uz'): ?>
                                                            Yangi
                                                        <?php else: ?>
                                                            Новый
                                                        <?php endif ?>
                                                    </span>
                                                </div>
                                            <?php endif ?>
                                            <?php $sale_product = $value->sale; ?>
                                            <?php if (isset($sale_product) and !empty($sale_product)): ?>
                                                
                                                <div class="label-sale">
                                                    <span><?php echo "-".round($sale_product["sale_pracent"])."%" ?>
                                                        <span class="sale-text">
                                                            <?php if (Yii::$app->language == 'uz'): ?>
                                                                Chegirma
                                                            <?php else: ?>
                                                                Скидка
                                                            <?php endif ?>
                                                        </span>
                                                    </span>
                                                    
                                                    <div class="countdown-circle">
                                                        <div class="countdown js-countdown" data-countdown="2021/07/01"></div>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    </a>
                                    <div class="prd-circle-labels">
                                        <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                        <a href="#" class="circle-label-qview js-prd-quickview prd-hide-mobile" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUIsK VIEW</span></a>
                                        <?php if (count($product_block) > 1): ?>
                                            <div class="colorswatch-label colorswatch-label--variants js-prd-colorswatch">
                                                <i class="icon-palette"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                                                <ul>
                                                    <?php foreach ($product_block as $key2 => $value2): ?>
                                                        <?php $product_img = ProductDetailsCode::MultiDetails($value2["product_id"],$value2["multi_detail_id"]); ?>
                                                        <li data-image="<?php if (isset($product_img)): ?><?php echo $product_img->img ?><?php endif ?>">
                                                            <a style="background: <?php echo $value2->multi_one->color_name; ?>;" class="js-color-toggle" data-toggle="tooltip" data-placement="left" title="Color Name">
                                                                <!-- <img src="asset/images/colorswatch/color-green.png" alt=""> -->
                                                            </a>
                                                        </li>
                                                    <?php endforeach ?>
                                                    
                                                </ul>
                                            </div>
                                        <?php endif ?>
                                    </div>

                                    
                                    <ul class="list-options color-swatch">
                                        <?php $i = 1; ?>
                                        <?php foreach ($value->imgs as $key2 => $value2): ?>
                                            <?php if ($i == 1): ?>
                                                <li data-image="<?php echo$value2["img"]  ?>" class="active"><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                                            <?php else: ?>
                                                <li data-image="<?php echo$value2["img"]  ?>" class=><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>

                                            <?php endif ?>
                                            <?php $i++; ?>
                                        <?php endforeach ?>

                                    </ul>
                                </div>
                                <div class="prd-info">
                                    <div class="prd-info-wrap">
                                        <div class="prd-info-top">
                                            <div class="prd-rating"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                        </div>
                                        <?php $stars =  $value->stars; ?>
                                        <div class="prd-rating justify-content-center">
                                            <?php for ($i = 1; $i <= $stars; $i++): ?>
                                                <i class="icon-star-fill fill"></i>
                                            <?php endfor ?>
                                        </div>
                                        <div class="prd-tag"><a href="#">
                                            <?php if (Yii::$app->language == "uz"): ?>
                                                    <?php echo $value->subCategory->title_uz ?>
                                                <?php else: ?>
                                                    <?php echo $value->subCategory->title_ru ?>
                                            <?php endif ?>
                                        </a></div>
                                        <h2 class="prd-title"><a href="product.html">
                                            <?php if (Yii::$app->language == "uz"): ?>
                                                <?php echo $value["title_uz"] ?>
                                            <?php else: ?>
                                                <?php echo $value["title_ru"] ?>
                                            <?php endif ?>
                                        </a></h2>
                                        <div class="prd-description">
                                            Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante sed lacinia.
                                        </div>
                                        <div class="prd-action">
                                                <button class="btn js-prd-addtocart" product-id="<?php echo $value["id"] ?>">
                                                    <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                </button>
                                        </div>
                                    </div>
                                    <div class="prd-hovers">
                                        <div class="prd-circle-labels">
                                            <div><a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a></div>
                                            <div class="prd-hide-mobile"><a href="#" class="circle-label-qview js-prd-quickview" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUICK VIEW</span></a></div>
                                        </div>
                                        <div class="prd-price">
                                            <?php $product_sale = $value->sale; ?>
                                            <?php if (isset($product_sale) and !empty($product_sale)): ?>
                                                <div class="price-old"><?php echo $value["price"] ?></div>
                                                
                                                <div class="price-new"><?php echo $product_sale["sale_price"] ?></div>
                                                
                                                <?php else: ?>
                                                <div class="price-new"><?php echo $value["price"] ?></div>
                                            <?php endif ?>
                                        </div>
                                        <div class="prd-action">
                                            <div class="prd-action-left">
                                                <button product-id="<?php echo $value['id'] ?>" class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                                   <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
