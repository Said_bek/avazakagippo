<?php 
    use app\models\Products;
    use app\models\ProductDetailsCode;
?>

<div class="loader_swap">
    <div id="tab_box" class="product-listing data-to-show-3 data-to-show-md-3 data-to-show-sm-2 js-category-grid prd-horgrid"   data-grid-tab-content>
        <?php if (isset($product) and !empty($product)): ?>
           <?php foreach ($product as $key => $value) { ?>
                <?php $product = Products::getByDetailId($color_id,$value->id); ?>
                <div class="prd prd--style2 prd-labels--max prd-labels-shadow " style="opacity: 1!important;">
                    <div class="prd-inside">
                        <div class="prd-img-area">
                            <a href="product.html" class="prd-img image-hover-scale image-container">
                                <img src="<?php echo $value->img ?>" data-src="<?php echo $value->img ?>" alt="Midi Dress with Belt" class="js-prd-img lazyload fade-up">
                                <div class="foxic-loader"></div>
                                <div class="prd-big-squared-labels">
                                    
                                </div>
                            </a>
                            <div class="prd-circle-labels">
                                <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                <a href="#" class="circle-label-qview js-prd-quickview prd-hide-mobile" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUIsK VIEW</span></a>
                                <?php if (count($product) > 1): ?>
                                    <div class="colorswatch-label colorswatch-label--variants js-prd-colorswatch">
                                        <i class="icon-palette"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                                        <ul>
                                            <?php foreach ($product as $key2 => $value2): ?>
                                                <?php $product_img = ProductDetailsCode::MultiDetails($value2["product_id"],$value2["multi_detail_id"]); ?>
                                                <li data-image="<?php if (isset($product_img)): ?><?php echo $product_img->img ?><?php endif ?>">
                                                    <a style="background: <?php echo $value2->multi_one->color_name; ?>;" class="js-color-toggle" data-toggle="tooltip" data-placement="left" title="Color Name">
                                                        <!-- <img src="asset/images/colorswatch/color-green.png" alt=""> -->
                                                    </a>
                                                </li>
                                            <?php endforeach ?>
                                            
                                        </ul>
                                    </div>
                                <?php endif ?>
                            </div>
                            
                            <ul class="list-options color-swatch">
                                <?php $i = 1; ?>
                                <?php foreach ($value->imgs as $key2 => $value2): ?>
                                    <?php if ($i == 1): ?>
                                        <li data-image="<?php echo$value2["img"]  ?>" class="active"><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                                    <?php else: ?>
                                        <li data-image="<?php echo$value2["img"]  ?>" class=><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>

                                    <?php endif ?>
                                    <?php $i++; ?>
                                <?php endforeach ?>
                            </ul>
                            
                        </div>
                        <div class="prd-info">
                            <div class="prd-info-wrap">
                                <div class="prd-info-top">
                                    <div class="prd-rating"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                </div>
                                <div class="prd-rating justify-content-center"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                <div class="prd-tag"><a href="#">Seiko</a></div>
                                <h2 class="prd-title"><a href="product.html">
                                    <?php if (Yii::$app->language == "uz"): ?>
                                        <?php echo $value["title_uz"] ?>
                                    <?php else: ?>
                                        <?php echo $value["title_ru"] ?>
                                    <?php endif ?>

                                </a></h2>
                                <div class="prd-description">
                                    Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante sed lacinia.
                                </div>
                                <div class="prd-action">
                                    <button class="btn js-prd-addtocart" product-id="<?php echo $value["id"] ?>">
                                        <?php echo Yii::t('app', 'add_to_cart'); ?>
                                    </button>
                                </div>
                            </div>
                            <div class="prd-hovers">
                                <div class="prd-circle-labels">
                                    <div>
                                        <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class
                                            ="icon-heart-stroke"></i>
                                        </a>
                                        <a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                    </div>
                                    <div class="prd-hide-mobile"
                                    ><a href="#" class="circle-label-qview js-prd-quickview" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUICK VIEW</span></a></div>
                                </div>
                                <div class="prd-price">
                                    
                                    <div class="price-new"><?php price($value["price"]) ?></div>
                                </div>
                                <div class="prd-action">
                                    <div class="prd-action-left">
                                        <button class="btn js-prd-addtocart" product-id="<?php echo $value["id"] ?>">
                                            <?php echo Yii::t('app', 'add_to_cart'); ?>                 
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php endif ?>
    </div>

    <div id="tab_box" class="product-listing data-to-show-3 data-to-show-md-3 data-to-show-sm-2 js-category-grid prd-horgrid search_block_<?php echo $last_id; ?>"
         data-grid-tab-content></div>
</div>
<div class="loader-horizontal-sm js-loader-horizontal-sm d-none" data-loader-horizontal
     style="opacity: 0;">
     <span></span>
</div>
<div class="circle-loader-wrap">
    <div class="circle-loader">
        <a id="load_more_category" data-id="<?php if (isset($last_id)): ?><?php echo $last_id ?><?php endif ?>">
            <svg id="svg_d" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <circle cx="50%" cy="50%" r="63" fill="transparent"></circle>
                <circle class="js-circle-bar" cx="50%" cy="50%" r="63"
                        fill="transparent"></circle>
            </svg>
            <svg id="svg_m" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <circle cx="50%" cy="50%" r="50" fill="transparent"></circle>
                <circle class="js-circle-bar" cx="50%" cy="50%" r="50"
                        fill="transparent"></circle>
            </svg>
            <div class="circle-loader-text">Load More</div>
            <div class="circle-loader-text-alt"><span class="js-circle-loader-start"></span>&nbsp;out
                of&nbsp;<span class="js-circle-loader-end"></span></div>
        </a>
    </div>
</div>