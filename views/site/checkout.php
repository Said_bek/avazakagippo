<?php  
	use yii\helpers\Url;
?>
<div class="holder breadcrumbs-wrap mt-0">
	<div class="container">
		<ul class="breadcrumbs">
			<li>
				<a href="<?php echo Url::toRoute('site/index')?>">
					<?php echo Yii::t('app', 'home'); ?>
				</a>
			</li>
			<li>
				<span>
					<?php echo Yii::t('app', 'checkout'); ?>	
				</span>
			</li>
		</ul>
	</div>
</div>
<div class="holder">
	<div class="container">
		<h1 class="text-center">
			<?php echo Yii::t('app', 'order_checkout'); ?>	
		</h1>
		<div class="row">
			<div class="col-md-9">
				<div class="card">
					<div class="card-body">
						<h2>
							<?php echo Yii::t('app', 'my_address'); ?>	
						</h2>
						<div class="row">
							<div class="col">
								<?php if (isset($client_address) && !empty($client_address)): ?>
									<?php foreach ($client_address as $key => $value): ?>
										<div class="clearfix border p-1">
											<input id="formcheckoutRadio<?php echo $value->id ?>" value="<?php echo $value->id ?>" name="address" type="radio" class="address">
											<label for="formcheckoutRadio<?php echo $value->id ?>" class="w-100">
												<p class="m-0">
													<?php echo $value->address ?>
												</p>
												<small class="text-muted">
													<?php if (Yii::$app->language == 'uz'): ?>
														<?php echo $value->region->title_uz ?>	
													<?php else: ?>
														<?php echo $value->region->title_ru ?>
													<?php endif ?>
												</small>
											</label>
										</div>
										<br>
									<?php endforeach ?>
								<?php else: ?>
									<?php echo Yii::t('app', 'not_address'); ?>	
								<?php endif ?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9 mt-2 mt-md-0">
				<div class="card">
					<div class="card-body">
						<h2>
							<?php echo Yii::t('app', 'address'); ?>	
						</h2>
						<label>
							<?php echo Yii::t('app', 'districts'); ?>:
						</label>
						<div class="form-group select-wrapper">
							<select class="form-control form-control--sm" id="region">
								<?php if (isset($regions) && !empty($regions)): ?>
									<?php foreach ($regions as $key => $value): ?>
										<option value="<?php echo $value->id ?>">
											<?php if (Yii::$app->language == 'uz'): ?>
												<?php echo $value->title_uz ?>
											<?php else: ?>
												<?php echo $value->title_ru ?>
											<?php endif ?>
										</option>		
									<?php endforeach ?>
								<?php endif ?>
								
							</select>
						</div>
						<div class="mt-2"></div>
						<label>
							<?php echo Yii::t('app', 'address2'); ?>:
						</label>
						<div class="form-group">
							<input type="text" class="form-control form-control--sm" id="new_address">
						</div>
						<div class="clearfix">
							<label for="formcheckoutCheckbox1">
								<?php echo Yii::t('app', 'add_to_my_address'); ?>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="mt-3"></div>
		<h2 class="custom-color">
			<?php echo Yii::t('app', 'order_summary'); ?>	
		</h2>
		<div class="row">
			<div class="col-md-12">
				<div class="cart-table cart-table--sm pt-3 pt-md-0">
					<div class="cart-table-prd cart-table-prd--head py-1 d-none d-md-flex">
						<div class="cart-table-prd-image text-center">
							<?php echo Yii::t('app', 'img'); ?>
						</div>
						<div class="cart-table-prd-content-wrap">
							<div class="cart-table-prd-info">
								<?php echo Yii::t('app', 'name_product'); ?>
							</div>
							<div class="cart-table-prd-qty">
								<?php echo Yii::t('app', 'qty'); ?>
							</div>
							<div class="cart-table-prd-price">
								<?php echo Yii::t('app', 'price'); ?>
							</div>
						</div>
					</div>

					<?php if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?>
						<?php foreach ($_SESSION['cart'] as $key => $value): ?>
							<div class="cart-table-prd">
								<div class="cart-table-prd-image">
									<a href="#" class="prd-img">
										<img class="lazyload fade-up" src="<?php echo $value['img'] ?>" data-src="<?php echo $value['img'] ?>" alt="">
									</a>
								</div>
								<div class="cart-table-prd-content-wrap">
									<div class="cart-table-prd-info">
										<h2 class="cart-table-prd-name">
											<a href="product.html">
												<?php if (Yii::$app->language == "uz"): ?>
													<?php echo $value['title_uz'] ?>
												<?php else: ?>
													<?php echo $value['title_ru'] ?>
												<?php endif ?>
											</a>
										</h2>
									</div>
									<div class="cart-table-prd-qty">
										<div class="qty qty-changer">
											<input disabled readonly type="text" class="qty-input disabled" value="<?php echo $value['count'] ?>">
										</div>
									</div>
									<div class="cart-table-prd-price-total">
										<?php echo price($value['total_price']) ?>
									</div>
								</div>
							</div>
						<?php endforeach ?>
					<?php endif ?>
					
					
				</div>
			</div>
			<div class="col-md-6 mt-2 mt-md-0">
				<div class="card">
					<div class="card-body">
						<h2>
							<?php echo Yii::t('app', 'payment_method'); ?>	
						</h2>
						<div class="clearfix">
							<input id="formcheckoutRadio4" value="1" name="payment" type="radio" class="pay" checked="checked">
							<label for="formcheckoutRadio4">Click</label>
						</div>
					</div>
				</div>
				<div class="mt-2"></div>
				<div class="cart-total-sm">
					<span>
						<?php echo Yii::t("app","sub_total") ?>
					</span>
					<span class="card-total-price">
						<?php echo price($_SESSION['cart_tl_price']) ?>
					</span>
				</div>
				<div class="clearfix mt-2">
					<button id="place_order" type="submit" class="btn btn--lg w-100">
						<?php echo Yii::t('app', 'place_order'); ?>	
					</button>
				</div>
				<div class="clearfix mt-2">
					<a href="<?php echo Url::toRoute('site/cart')?>" class="btn btn--grey btn-block">
						<?php echo Yii::t("app","back_cart") ?>
					</a>
				</div>
				
			</div>
		</div>
	</div>
</div>
