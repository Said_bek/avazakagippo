<?php $session = Yii::$app->session; ?>
<?php if (isset($session["list"]["selected"])): ?>
    <?php foreach ($session["list"]["selected"] as $key => $value): ?>
        <?php foreach ($value as $key2 => $value2): ?>
            <li>
                <a data-name="<?php echo $key ?>"  data-id="<?php echo $key2 ?>" id="delete_session" href="#">
                    <?php if (Yii::$app->language == "uz"): ?>
                            <?php  echo $value2["title_uz"]; ?>
                    <?php else: ?>
                        <?php echo $value2["title_ru"] ?>
                    <?php endif ?>
                    
                </a>
            </li>
        <?php endforeach ?>
    <?php endforeach ?>
<?php endif ?>