<div class="modal-info-heading">
    <div class="mb-1"><i class="icon-envelope"></i></div>
    <h2><?php echo Yii::t("app","ask_question"); ?></h2>
</div>
<form class="contact_form">
    <div class="form-group">
        <input id="name_contact" type="text" name="Contact[name]" class="form-control form-control--sm"
               placeholder="<?php echo Yii::t("app","name") ?>">
        <p class="name_error d-none" style="color:red;"><?php echo Yii::t("app","name_error") ?></p>
    </div>
    <div class="form-group">
<!--                                         <input  type="text" name="Contact[phone]" class="form-control form-control--sm"
               placeholder="<?php echo Yii::t("app",'phone_number') ?>"> -->
        <input name="Contact[phone]" autocomplete="off" class="form-control js-phone-mask" id="phone_number_contact" data-tel="+998" value="+998" placeholder="<?php echo Yii::t('app', 'phone_number'); ?>">
        <p class="phone_number_error d-none" style="color:red;"><?php echo Yii::t("app","phone_number_error") ?></p>
    </div>
    <div class="form-group">
        <textarea id="message_contact" class="form-control textarea--height-170" name="Contact[body]" placeholder="<?php echo Yii::t("app","question_message") ?>" required=""></textarea>
        <p class="message_error d-none" style="color:red;"><?php echo Yii::t("app","message_error") ?></p>
    </div>
    <a class="btn send_question"><?php echo Yii::t("app","send_sms") ?></a>
    <p class="p--small mt-15 mb-0"><?php echo Yii::t("app","contacting") ?></p><input
        name="recaptcha-v3-token" type="hidden">
</form>