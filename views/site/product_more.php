<?php 

    use app\models\Products;
    use app\models\ProductDetailsCode;

?>
<?php foreach ($product_search as $key => $value): ?>
    <?php $product_block = Products::getByDetailId($color_id,$value["id"]); ?>
    <div class="prd prd--style2 prd-labels--max prd-labels-shadow" style="opacity:1;">
        <div class="prd-inside">
            <div class="prd-img-area">
                <a href="product.html" class="prd-img image-hover-scale image-container">
                    <img src="data:<?php echo $value["img"] ?>" data-src="<?php echo $value["img"] ?>" alt="Oversized Cotton Blouse" class="js-prd-img lazyload fade-up">
                    <div class="foxic-loader"></div>
                    <div class="prd-big-squared-labels">
                        
                        <?php if ($value["status"] == 3 or $value["status"] == 4): ?>
                            <div class="label-new">
                                <span>
                                    <?php if (Yii::$app->language == 'uz'): ?>
                                        Yangi
                                    <?php else: ?>
                                        Новый
                                    <?php endif ?>
                                </span>
                            </div>
                        <?php endif ?>
                        <?php $sale_product = $value->sale; ?>
                        <?php if (isset($sale_product) and !empty($sale_product)): ?>
                            
                            <div class="label-sale">
                                <span><?php echo "-".round($sale_product["sale_pracent"])."%" ?>
                                    <span class="sale-text">
                                        <?php if (Yii::$app->language == 'uz'): ?>
                                            Chegirma
                                        <?php else: ?>
                                            Скидка
                                        <?php endif ?>
                                    </span>
                                </span>
                                
                                <div class="countdown-circle">
                                    <div class="countdown js-countdown" data-countdown="2021/07/01"></div>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </a>
                <div class="prd-circle-labels">
                    <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                    <a href="#" class="circle-label-qview js-prd-quickview prd-hide-mobile" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUIsK VIEW</span></a>
                    <?php if (count($product_block) > 1): ?>
                        <div class="colorswatch-label colorswatch-label--variants js-prd-colorswatch">
                            <i class="icon-palette"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                            <ul>
                                <?php foreach ($product_block as $key2 => $value2): ?>
                                    <?php $product_img = ProductDetailsCode::MultiDetails($value2["product_id"],$value2["multi_detail_id"]); ?>
                                    <li data-image="<?php if (isset($product_img)): ?><?php echo $product_img->img ?><?php endif ?>">
                                        <a style="background: <?php echo $value2->multi_one->color_name; ?>;" class="js-color-toggle" data-toggle="tooltip" data-placement="left" title="Color Name">
                                            <!-- <img src="asset/images/colorswatch/color-green.png" alt=""> -->
                                        </a>
                                    </li>
                                <?php endforeach ?>
                                
                            </ul>
                        </div>
                    <?php endif ?>
                </div>

                
                <ul class="list-options color-swatch">
                    <?php $i = 1; ?>
                    <?php foreach ($value->imgs as $key2 => $value2): ?>
                        <?php if ($i == 1): ?>
                            <li data-image="<?php echo$value2["img"]  ?>" class="active"><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                        <?php else: ?>
                            <li data-image="<?php echo$value2["img"]  ?>" class=><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>

                        <?php endif ?>
                        <?php $i++; ?>
                    <?php endforeach ?>

                </ul>
            </div>
            <div class="prd-info">
                <div class="prd-info-wrap">
                    <div class="prd-info-top">
                        <div class="prd-rating"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                    </div>
                    <?php $stars =  $value->stars; ?>
                    <div class="prd-rating justify-content-center">
                        <?php for ($i = 1; $i <= $stars; $i++): ?>
                            <i class="icon-star-fill fill"></i>
                        <?php endfor ?>
                    </div>
                    <div class="prd-tag"><a href="#">
                        <?php if (Yii::$app->language == "uz"): ?>
                                <?php echo $value->subCategory->title_uz ?>
                            <?php else: ?>
                                <?php echo $value->subCategory->title_ru ?>
                        <?php endif ?>
                    </a></div>
                    <h2 class="prd-title"><a href="product.html">
                        <?php if (Yii::$app->language == "uz"): ?>
                            <?php echo $value["title_uz"] ?>
                        <?php else: ?>
                            <?php echo $value["title_ru"] ?>
                        <?php endif ?>
                    </a></h2>
                    <div class="prd-description">
                        Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante sed lacinia.
                    </div>
                    <div class="prd-action">
                        <form action="#">
                            <button class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                <?php echo Yii::t('app', 'add_to_cart'); ?>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="prd-hovers">
                    <div class="prd-circle-labels">
                        <div><a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a></div>
                        <div class="prd-hide-mobile"><a href="#" class="circle-label-qview js-prd-quickview" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUICK VIEW</span></a></div>
                    </div>
                    <div class="prd-price">
                        <?php $product_sale = $value->sale; ?>
                        <?php if (isset($product_sale) and !empty($product_sale)): ?>
                            <div class="price-old"><?php echo $value["price"] ?></div>
                            
                            <div class="price-new"><?php echo $product_sale["sale_price"] ?></div>
                            
                            <?php else: ?>
                            <div class="price-new"><?php echo $value["price"] ?></div>
                        <?php endif ?>
                    </div>
                    <div class="prd-action">
                        <div class="prd-action-left">
                            <form action="#">
                                <button class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                <?php echo Yii::t('app', 'add_to_cart'); ?>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>