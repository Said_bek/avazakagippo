<!--//header-->
<?php 
    
    use app\models\Products;
    use app\models\ProductDetailsCode;
    
?>
<div class="page-content">
    <div class="holder breadcrumbs-wrap mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="index.html">Home</a></li>
                <li><a href="category.html">Women</a></li>
                <li>
                    <span>
                        <?php if (Yii::$app->language == "uz"): ?>
                            <?php echo $product["title_uz"] ?>
                            <?php echo $product["title_uz"] ?>
                            
                        <?php else: ?>
                            <?php echo $product["title_ru"] ?>
                        <?php endif ?>
                        
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="holder">
        <div class="container js-prd-gallery" id="prdGallery">
            <div class="row prd-block prd-block-under prd-block--prv-bottom">
                <div class="col">
                    <div class="js-prd-d-holder">
                        <div class="prd-block_title-wrap">
                            <div class="prd-block_reviews" data-toggle="tooltip" data-placement="top"
                                 title="Scroll To Reviews"><i class="icon-star-fill fill"></i><i
                                    class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i
                                    class="icon-star-fill fill"></i><i class="icon-star"></i>
                                <span class="reviews-link"><a href="productReviews" class="js-reviews-link"> (<?php echo count($reviews) ?> <?php echo Yii::t("app","reviews_word") ?>)</a></span>
                            </div>
                            <h1 class="prd-block_title">
                                <?php if (Yii::$app->language == "uz"): ?>
                                        <?php echo $product["title_uz"] ?>
                                    <?php else: ?>
                                        <?php echo $product["title_ru"] ?>
                                <?php endif ?>
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-auto prd-block-prevnext-wrap">
                    <div class="prd-block-prevnext">
                        <a href="#"><span class="prd-img"><img class="lazyload fade-up"
                                                               src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                               data-src="/asset/images/skins/fashion/products/product-02-1.jpg"
                                                               alt=""><i class="icon-arrow-left"></i></span></a>
                        <a href="#"><span class="prd-img"><img class="lazyload fade-up"
                                                               src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                               data-src="/asset/images/skins/fashion/products/product-01-1.jpg"
                                                               alt=""></span></a>
                        <a href="#"><span class="prd-img"><img class="lazyload fade-up"
                                                               src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                               data-src="/asset/images/skins/fashion/products/product-15-1.jpg"
                                                               alt=""><i class="icon-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="row prd-block prd-block--prv-bottom">
                <div class="col-md-8 col-lg-8 col-xl-8 aside--sticky js-sticky-collision">
                    <div class="aside-content">
                        <!-- Product Gallery -->
                        <div class="mb-2 js-prd-m-holder"></div>
                        <div class="prd-block_main-image">
                            <div class="prd-block_main-image-holder" id="prdMainImage">
                                <div class="product-main-carousel js-product-main-carousel" data-zoom-position="inner">
                                    <?php foreach ($product_img as $key => $value): ?>
                                        <?php if ($value["multi_detail_id"] != 0): ?>
                                            <div data-value="<?php echo $value->color->color_name ?>">
                                                <!-- <span class="prd-img"> -->
                                                    <div class="target">
                                                        <img src="<?php echo $value["img"] ?>"
                                                        data-src="<?php echo $value["img"] ?>" alt=""/>
                                                    </div>
                                                <!-- </span> -->
                                            </div>
                                        <?php else: ?>
                                            <div>
                                                <!-- <span class="prd-img"> -->
                                                    <div class="target">
                                                        <img src="<?php echo $value["img"] ?>" data-src="<?php echo $value["img"] ?>"/>
                                                    </div>
                                                <!-- </span> -->
                                            </div>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                    
                                </div>
                                <div class="prd-block_label-sale-squared justify-content-center align-items-center">
                                    <span>Sale</span></div>
                            </div>
                            <div class="prd-block_main-image-links">
                                <a data-fancybox data-width="900" href="https://www.youtube.com/watch?v=Zk3kr7J_v3Q"
                                   class="prd-block_video-link"><i class="icon-video"></i></a>
                                <a href="/asset/images/products//product-01.jpg" class="prd-block_zoom-link"><i
                                        class="icon-zoom-in"></i></a>
                            </div>
                        </div>
                        <div class="product-previews-wrapper">
                            <div class="product-previews-carousel js-product-previews-carousel">
                                <?php foreach ($product_img as $key => $value): ?>
                                    <a href="#" data-value="Beige">
                                        <span class="prd-img">
                                            <img
                                            src="<?php echo $value["img"] ?>"
                                            data-src="<?php echo $value["img"] ?>"
                                            class="lazyload fade-up" alt=""/>
                                        </span>
                                    </a>
                                <?php endforeach ?>
                            </div>
                        </div>
                        <!-- /Product Gallery -->
                    </div>
                </div>
                <div class="col-md-10 col-lg-10 col-xl-10 mt-1 mt-md-0">
                    <div class="prd-block_info prd-block_info--style1"
                         data-prd-handle="/products/copy-of-suede-leather-mini-skirt">
                        <div class="prd-block_info-top prd-block_info_item order-0 order-md-2">
                            <div class="prd-block_price prd-block_price--style2">
                                <?php if (isset($product_sale) and !empty($product_sale)): ?>
                                    <div class="prd-block_price--actual">
                                        <?php price($product_sale->sale_price) ?>
                                    </div>
                                    <div class="prd-block_price-old-wrap">
                                        <span class="prd-block_price--old">
                                            <?php price($product->price); ?>
                                        </span>
                                        <?php 
                                            $save_money = $product->price - $product_sale->sale_price;
                                        ?>
                                        <span class="prd-block_price--text">
                                            <?php echo Yii::t('app', 'save_money'); ?>: <?php price($save_money)  ?>
                                            (  <?php  echo $product_sale->sale_pracentx ?>% )
                                        </span>
                                    </div>
                                <?php else: ?>
                                    <div class="prd-block_price--actual">
                                        <?php price($product->price); ?>
                                        
                                    </div>
                                <?php endif ?>
                            </div>
                            <div class="prd-block_viewed-wrap d-none d-md-flex">
                                <div class="prd-block_viewed">
                                    <i class="icon-time"></i>
                                    <span>
                                        <?php echo Yii::t('app', 'its_product'); ?>
                                        <?php echo $product->view ?>
                                        <?php echo Yii::t('app', 'views_product'); ?>         
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="prd-block_description prd-block_info_item ">
                            <h3>
                                <?php echo Yii::t('app', 'short_description'); ?>
                            </h3>
                            <p>
                                <?php if (Yii::$app->language == "uz"): ?>
                                        <?php echo $product["description_uz"] ?>
                                    <?php else: ?>
                                        <?php echo $product["description_ru"] ?>
                                <?php endif ?>
                            </p>
                            <div class="mt-1"></div>
                            <!-- <div class="row vert-margin-less">
                                <div class="col-sm">
                                    <ul class="list-marker">
                                        <li>100% Polyester</li>
                                        <li>Lining:100% Viscose</li>
                                    </ul>
                                </div>
                                <div class="col-sm">
                                    <ul class="list-marker">
                                        <li>Do not dry clean</li>
                                        <li>Only non-chlorine</li>
                                    </ul>
                                </div>
                            </div> -->
                        </div>
                        <div class="prd-progress prd-block_info_item" data-left-in-stock="">
                            <div class="prd-progress-text">
                                <?php echo Yii::t("app",'hurry'); ?> <span class="prd-progress-text-left js-stock-left"><?php echo $product["quantity"] ?></span><?php echo Yii::t("app",'left'); ?>
                            </div>
                            <div class="prd-progress-text-null"></div>
                            <div class="prd-progress-bar-wrap progress">
                                <div class="prd-progress-bar progress-bar active"
                                     data-stock="50, 10, 30, 25, 1000, 15000" style="width: 53%;"></div>
                            </div>
                        </div>
                        <div class="prd-block_countdown js-countdown-wrap prd-block_info_item countdown-init">
                            <div class="countdown-box-full-text w-md">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-sm-auto text-center">
                                        <div class="countdown js-countdown" data-countdown="2021/07/01"></div>
                                    </div>
                                    <div class="col">
                                        <div class="countdown-txt"> TIME IS RUNNING OUT!</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="prd-block_order-info prd-block_info_item " data-order-time="" data-locale="en">
                            <i class="icon-box-2"></i>
                            <div>Order in the next <span class="prd-block_order-info-time countdownCircleTimer"
                                                         data-time="8:00:00, 15:30:00, 23:59:59"><span><span>04</span>:</span><span><span>46</span>:</span><span><span>24</span></span></span>
                                to get it by <span data-date="">Tuesday, September 08, 2020</span></div>
                        </div>
                        <div class="prd-block_info_item prd-block_info-when-arrives d-none" data-when-arrives>
                            <div class="prd-block_links prd-block_links m-0 d-inline-flex">
                                <i class="icon-email-1"></i>
                                <div><a href="#" data-follow-up="" data-name="Oversize Cotton Dress"
                                        class="prd-in-stock" data-src="#whenArrives">Inform me when the item arrives</a>
                                </div>
                            </div>
                        </div>
                        <div class="prd-block_info-box prd-block_info_item">
                            <div class="two-column"><p><?php echo Yii::t('app', 'sale'); ?>:
                                <span class="prd-in-stock" data-stock-status=""><?php echo Yii::t('app', 'has'); ?></span></p>
                                <p><?php echo Yii::t('app', 'collection'); ?>: <span> <a href="collections.html" data-toggle="tooltip"
                                                         data-placement="top"
                                                         data-original-title="View all">
                                                         <?php if (Yii::$app->language == "uz"): ?>
                                                                <?php echo $product->category->title_uz ?>
                                                              <?php else: ?>
                                                                <?php echo $product->category->title_ru ?>
                                                         <?php endif ?>
                                                             
                                                         </a></span></p>
                                <p>Sku: <span data-sku=""><?php echo $product->sku; ?></span></p>
                                <p><?php echo Yii::t("app","brend") ?>: <span><?php echo $product["made"] ?></span></p>
                                <p><span></span></p></div>
                        </div>
                        <div class="order-0 order-md-100">
                            <form method="post" action="#">
                                <div class="prd-block_options">
                                    <div class="prd-color swatches">
                                        <div class="option-label">Color:</div>
                                        <select class="form-control hidden single-option-selector-modalQuickView"
                                                id="SingleOptionSelector-0" data-index="option1">
                                            <option value="Beige" selected="selected">Beige</option>
                                            <option value="Black">Black</option>
                                            <option value="Red">Red</option>
                                        </select>
                                        <ul class="images-list js-size-list" data-select-id="SingleOptionSelector-0">
                                            <?php foreach ($product_img as $key => $value): ?>
                                                <?php if ($value["multi_detail_id"] != 0): ?>
                                                    <li class="active">
                                                        <a href="#" data-value="<?php echo $value->color->color_name ?>" data-toggle="tooltip" data-placement="top" data-original-title="<?php echo $value->color->color_name ?>">
                                                            <span class="image-container image-container--product">
                                                                <img src="<?php echo $value["img"] ?>"
                                                                alt="">
                                                            </span>
                                                        </a>
                                                    </li>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        </ul>
                                    </div>
                                    <div class="prd-size swatches">
                                        <div class="option-label">Size:</div>
                                        <select class="form-control hidden single-option-selector-modalQuickView"
                                                id="SingleOptionSelector-1" data-index="option2">
                                            <option value="Small" selected="selected">Small</option>
                                            <option value="Medium">Medium</option>
                                            <option value="Large">Large</option>
                                        </select>
                                        <ul class="size-list js-size-list" data-select-id="SingleOptionSelector-1">
                                            <li class="active"><a href="#" data-value="Small"><span
                                                    class="value">Small</span></a></li>
                                            <li><a href="#" data-value="Medium"><span class="value">Medium</span></a>
                                            </li>
                                            <li><a href="#" data-value="Large"><span class="value">Large</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="prd-block_actions prd-block_actions--wishlist">
                                    <div class="prd-block_qty">
                                        <div class="qty qty-changer">
                                            <button class="decrease js-qty-button"></button>
                                            <input type="number" class="qty-input" name="quantity" value="1"
                                                   data-min="1" data-max="1000">
                                            <button class="increase js-qty-button"></button>
                                        </div>
                                    </div>
                                    <div class="btn-wrap">
                                        <button class="btn btn--add-to-cart js-trigger-addtocart js-prd-addtocart" product-id="<?php echo $product->id ?>"
                                                data-product='{"name":  "Leather Pegged Pants ",  "url ": "product.html",  "path ": "<?php echo $product["img"] ?>",  "aspect_ratio ": "0.78"}'>
                                            <?php echo Yii::t("app","card"); ?>
                                        </button>
                                    </div>
                                    <div class="btn-wishlist-wrap">
                                        <a href="#"
                                           class="btn-add-to-wishlist ml-auto btn-add-to-wishlist--add js-add-wishlist"
                                           title="Add To Wishlist"><i class="icon-heart-stroke"></i></a>
                                        <a href="#"
                                           class="btn-add-to-wishlist ml-auto btn-add-to-wishlist--off js-remove-wishlist"
                                           title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                    </div>
                                </div>
                            </form>
                            <div class="prd-block_agreement prd-block_info_item order-0 order-md-100 text-right"
                                 data-agree>
                                <input id="agreementCheckboxProductPage" class="js-agreement-checkbox"
                                       data-button=".shopify-payment-agree" name="agreementCheckboxProductPage"
                                       type="checkbox">
                                <label for="agreementCheckboxProductPage"><a  href="#" data-fancybox
                                                                             class="modal-info-link"
                                                                             data-src="#agreementInfo">I agree to the
                                    terms of service</a></label>
                            </div>
                        </div>
                        <div class="prd-block_info_item">
                            <ul class="prd-block_links list-unstyled">
                                <li><i class="icon-size-guide"></i><a href="#" data-fancybox class="modal-info-link"
                                                                      data-src="#sizeGuide">
                                                                          <?php echo Yii::t("app","size_guide"); ?>
                                                                      </a></li>
                                <li><i class="icon-delivery-1"></i><a href="#" data-fancybox class="modal-info-link"
                                                                      data-src="#deliveryInfo">
                                                                        <?php echo Yii::t("app","delivery_return") ?>
                                                                      </a>
                                </li>
                                <li><i class="icon-email-1"></i><a id="open_contact_form" href="#" data-fancybox class="modal-info-link"
                                                                   data-src="#contactModal"><?php echo Yii::t("app","send_question_product") ?></a>
                                </li>
                            </ul>
                            <div id="sizeGuide" style="display: none;" class="modal-info-content modal-info-content-lg">
                                <div class="modal-info-heading">
                                    <div class="mb-1"><i class="icon-size-guide"></i></div>
                                    <h2>Size Guide</h2>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-borderless text-center">
                                        <thead>
                                        <tr>
                                            <th>
                                                <?php echo Yii::t("app","turkish_sizes");?>
                                            </th>
                                            <th>
                                                <?php echo Yii::t("app","turkish_sizes"); ?>
                                            </th>
                                            <th>
                                                <?php echo Yii::t("app","uzbek_sizes"); ?>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                S
                                            </td>
                                            <td>
                                                36
                                            </td>
                                            <td>
                                                42
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                M
                                            </td>
                                            <td>
                                                38
                                            </td>
                                            <td>
                                                44
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                L
                                            </td>
                                            <td>
                                                40
                                            </td>
                                            <td>
                                                46
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>
                                                XL
                                            </td>
                                            <td>42</td>
                                            <td>
                                                48 - 59
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                XXL
                                            </td>
                                            <td>44</td>
                                            <td>
                                                52 - 54
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="deliveryInfo" style="display: none;"
                                 class="modal-info-content modal-info-content-lg">
                                <div class="modal-info-heading">
                                    <div class="mb-1"><i class="icon-delivery-1"></i></div>
                                    <h2>Delivery and Return</h2>
                                </div>
                                <br>
                                <h5>Our parcel courier service</h5>
                                <p>Foxic is proud to offer an exceptional international parcel shipping service. It is
                                    straightforward and very easy to organise international parcel shipping. Our
                                    customer service team works around the clock to make sure that you receive high
                                    quality courier service from start to finish.</p>
                                <p>Sending a parcel with us is simple. To start the process you will first need to get a
                                    quote using our free online quotation service. From this, you’ll be able to navigate
                                    through the online form to book a collection date for your parcel, selecting a
                                    shipping day suitable for you.</p>
                                <br>
                                <h5>Shipping Time</h5>
                                <p>The shipping time is based on the shipping method you chose.<br>
                                    EMS takes about 5-10 working days for delivery.<br>
                                    DHL takes about 2-5 working days for delivery.<br>
                                    DPEX takes about 2-8 working days for delivery.<br>
                                    JCEX takes about 3-7 working days for delivery.<br>
                                    China Post Registered Mail takes 20-40 working days for delivery.</p>
                            </div>
                            <div id="contactModal" style="display: none; background: #fafcf9;"
                                 class="modal-info-content modal-info-content-sm">
                                
                            </div>
                        </div>
                        <div class="prd-block_info_item">
                            <img class="img-responsive lazyload d-none d-sm-block"
                                 src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                 data-src="/asset/images/payment/safecheckout.png" alt="">
                            <img class="img-responsive lazyload d-sm-none"
                                 src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                 data-src="/asset/images/payment/safecheckout-m.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="holder prd-block_links-wrap-bg d-none d-md-block">
        <div class="prd-block_links-wrap prd-block_info_item container mt-2 mt-md-5 py-1">
            <div class="prd-block_link"><span><i class="icon-call-center"></i>24/7 Support</span></div>
            <div class="prd-block_link">
                <!-- <span>Use promocode  FOXIC to get 15% discount!</span></div> -->
            <!-- <div class="prd-block_link"><span><i class="icon-delivery-truck"></i> Fast Shipping</span></div> -->
        </div>
    </div>
    <div class="holder mt-3 mt-md-5">
        <div class="container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs product-tab">
                <li class="nav-item"><a href="#Tab2" class="nav-link" data-toggle="tab"><?php echo Yii::t("app","table_size"); ?>
                    <span class="toggle-arrow"><span></span><span></span></span>
                </a></li>
                <li class="nav-item"><a href="#Tab5" class="nav-link" data-toggle="tab"><?php echo Yii::t("app","reviews_word") ?>
                    <span class="toggle-arrow"><span></span><span></span></span>
                </a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade" id="Tab2">
                    <h3>
                        <?php echo Yii::t("app","sizes") ?>
                    </h3>
                    <table class="table table-striped">
                        <tr>
                            <th scope="row">
                                <?php echo Yii::t("app","turkish_sizes") ?>
                            </th>
                            <td>
                                S
                            </td>
                            <td>
                                M
                            </td>
                            <td>
                                L
                            </td>
                            <td>
                                XL
                            </td>
                            <td>
                                XXL
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <?php echo Yii::t("app","turkish_sizes") ?>
                            </th>
                            <td>
                                36
                            </td>
                            <td>
                                38
                            </td>
                            <td>
                                40
                            </td>
                            <td>
                                42
                            </td>
                            <td>
                                44
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <?php echo Yii::t("app","uzbek_sizes") ?>
                            </th>
                            <td>
                                42
                            </td>
                            <td>
                                44
                            </td>
                            <td>
                                46
                            </td>
                            <td>
                                48 - 59
                            </td>
                            <td>
                                52 - 54
                            </td>
                        </tr>
                    </table>
                </div>
                
                <div role="tabpanel" class="tab-pane fade" id="Tab5">
                    <div id="productReviews">
                        <div class="row align-items-center">
                            <div class="col"><h2><?php echo Yii::t("app","customer_reviews") ?></h2></div>
                            <div class="col-18 col-md-auto mb-3 mb-md-0"><a data-id="<?php echo $product["id"] ?>"  id="open_review_form" href="#" data-fancybox="" class="review-write-link modal-info-link" data-src="#contactModal"><i
                                    class="icon-pencil"></i><?php echo Yii::t("app","write_review") ?></a></div>
                        </div>
                        <?php if (isset($reviews) and !empty($reviews)): ?>
                            <?php foreach ($reviews as $key => $value): ?>
                                <div id="productReviewsBottom">
                                    <div class="review-item">
                                        <div class="review-item_rating">
                                            <?php for ($i=1; $i <= $value["star"]; $i++) : ?>
                                                <i class="icon-star-fill fill"></i>
                                            <?php endfor ?>
                                        </div>
                                        <div class="review-item_top row align-items-center">
                                            <div class="col"><h5 class="review-item_author"><?php echo $value["customer_name"] ?> on <?php echo date("M d Y", strtotime($value["created_date"])) ?></h5></div>
                                            <div class="col-auto"></div>
                                        </div>
                                        <div class="review-item_content">
                                            <h4><?php echo $value["customer_title"] ?></h4>
                                            <p><?php echo $value["customer_post"] ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        <?php else: ?>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="holder">
        <div class="container">
            <div class="title-wrap text-center">
                <h2 class="h1-style">
                    <?php echo Yii::t("app","like_product") ?>
                </h2>
                <div class="carousel-arrows carousel-arrows--center"></div>
            </div>
            <div class="prd-grid prd-carousel js-prd-carousel slick-arrows-aside-simple slick-arrows-mobile-lg data-to-show-4 data-to-show-md-3 data-to-show-sm-3 data-to-show-xs-2"
                 data-slick='{"slidesToShow": 4, "slidesToScroll": 2, "responsive": [{"breakpoint": 992,"settings": {"slidesToShow": 3, "slidesToScroll": 1}},{"breakpoint": 768,"settings": {"slidesToShow": 2, "slidesToScroll": 1}},{"breakpoint": 480,"settings": {"slidesToShow": 2, "slidesToScroll": 1}}]}'>
                <?php foreach ($product_trend as $key => $value): ?>
                        <?php $product_block = Products::getByDetailId($color_id,$value["id"]); ?>
                        <div class="prd prd--style2 prd-labels--max prd-labels-shadow ">
                            <div class="prd-inside">
                                <div class="prd-img-area">
                                    <a href="product.html" class="prd-img image-hover-scale image-container">
                                        <img src="data:<?php echo $value["img"] ?>" data-src="<?php echo $value["img"] ?>" alt="Oversized Cotton Blouse" class="js-prd-img lazyload fade-up">
                                        <div class="foxic-loader"></div>
                                        <div class="prd-big-squared-labels">
                                            
                                            <?php if ($value["status"] == 3 or $value["status"] == 4): ?>
                                                <div class="label-new">
                                                    <span>
                                                        <?php if (Yii::$app->language == 'uz'): ?>
                                                            Yangi
                                                        <?php else: ?>
                                                            Новый
                                                        <?php endif ?>
                                                    </span>
                                                </div>
                                            <?php endif ?>
                                            <?php $sale_product = $value->sale; ?>
                                            <?php if (isset($sale_product) and !empty($sale_product)): ?>
                                                
                                                <div class="label-sale">
                                                    <span><?php echo "-".round($sale_product["sale_pracent"])."%" ?>
                                                        <span class="sale-text">
                                                            <?php if (Yii::$app->language == 'uz'): ?>
                                                                Chegirma
                                                            <?php else: ?>
                                                                Скидка
                                                            <?php endif ?>
                                                        </span>
                                                    </span>
                                                    
                                                    <div class="countdown-circle">
                                                        <div class="countdown js-countdown" data-countdown="2021/07/01"></div>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    </a>
                                    <div class="prd-circle-labels">
                                        <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                        <a href="#" class="circle-label-qview js-prd-quickview prd-hide-mobile" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUIsK VIEW</span></a>
                                        <?php if (count($product_block) > 1): ?>
                                            <div class="colorswatch-label colorswatch-label--variants js-prd-colorswatch">
                                                <i class="icon-palette"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                                                <ul>
                                                    <?php foreach ($product_block as $key2 => $value2): ?>
                                                        <?php $product_img = ProductDetailsCode::MultiDetails($value2["product_id"],$value2["multi_detail_id"]); ?>
                                                        <li data-image="<?php if (isset($product_img)): ?><?php echo $product_img->img ?><?php endif ?>">
                                                            <a style="background: <?php echo $value2->multi_one->color_name; ?>;" class="js-color-toggle" data-toggle="tooltip" data-placement="left" title="Color Name">
                                                                <!-- <img src="asset/images/colorswatch/color-green.png" alt=""> -->
                                                            </a>
                                                        </li>
                                                    <?php endforeach ?>
                                                    
                                                </ul>
                                            </div>
                                        <?php endif ?>
                                    </div>

                                    
                                    <ul class="list-options color-swatch">
                                        <?php $i = 1; ?>
                                        <?php foreach ($value->imgs as $key2 => $value2): ?>
                                            <?php if ($i == 1): ?>
                                                <li data-image="<?php echo$value2["img"]  ?>" class="active"><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                                            <?php else: ?>
                                                <li data-image="<?php echo$value2["img"]  ?>" class=><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>

                                            <?php endif ?>
                                            <?php $i++; ?>
                                        <?php endforeach ?>

                                    </ul>
                                </div>
                                <div class="prd-info">
                                    <div class="prd-info-wrap">
                                        <div class="prd-info-top">
                                            <div class="prd-rating"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                        </div>
                                        <?php $stars =  $value->stars; ?>
                                        <div class="prd-rating justify-content-center">
                                            <?php for ($i = 1; $i <= $stars; $i++): ?>
                                                <i class="icon-star-fill fill"></i>
                                            <?php endfor ?>
                                        </div>
                                        <div class="prd-tag"><a href="#">
                                            <?php if (Yii::$app->language == "uz"): ?>
                                                    <?php echo $value->subCategory->title_uz ?>
                                                <?php else: ?>
                                                    <?php echo $value->subCategory->title_ru ?>
                                            <?php endif ?>
                                        </a></div>
                                        <h2 class="prd-title"><a href="product.html">
                                            <?php if (Yii::$app->language == "uz"): ?>
                                                <?php echo $value["title_uz"] ?>
                                            <?php else: ?>
                                                <?php echo $value["title_ru"] ?>
                                            <?php endif ?>
                                        </a></h2>
                                        <div class="prd-description">
                                            Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante sed lacinia.
                                        </div>
                                        <div class="prd-action">
                                            <form action="#">
                                                <button class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                                    <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="prd-hovers">
                                        <div class="prd-circle-labels">
                                            <div><a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a></div>
                                            <div class="prd-hide-mobile"><a href="#" class="circle-label-qview js-prd-quickview" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUICK VIEW</span></a></div>
                                        </div>
                                        <div class="prd-price">
                                            <?php $product_sale = $value->sale; ?>
                                            <?php if (isset($product_sale) and !empty($product_sale)): ?>
                                                <div class="price-old"><?php echo $value["price"] ?></div>
                                                
                                                <div class="price-new"><?php echo $product_sale["sale_price"] ?></div>
                                                
                                                <?php else: ?>
                                                <div class="price-new"><?php echo $value["price"] ?></div>
                                            <?php endif ?>
                                        </div>
                                        <div class="prd-action">
                                            <div class="prd-action-left">
                                                <form action="#">
                                                    <button class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                                    <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<div class="footer-sticky">
    <div class="sticky-addcart js-stickyAddToCart closed">
        <div class="container">
            <div class="row">
                <div class="col-auto sticky-addcart_image">
                    <a href="product.html">
                        <img src="<?php echo $product->img ?>" alt=""/>
                    </a>
                </div>
                <div class="col col-sm-5 col-lg-4 col-xl-5 sticky-addcart_info">
                    <h1 class="sticky-addcart_title">
                        <?php if (Yii::$app->language == "uz"): ?>
                                <?php echo $product["title_uz"] ?>
                            <?php else: ?>
                                <?php echo $product["title_ru"] ?>
                        <?php endif ?>
                    </h1>
                    <?php $sale_product = $product->sale; ?>
                    <?php if (isset($sale_product) and !empty($sale_product)): ?>
                        <div class="sticky-addcart_price">
                            <span class="sticky-addcart_price--actual">
                                <?php echo number_format($sale_product["sale_price"],0," ",".") ?>
                                <?php echo Yii::t('app', 'sum'); ?>
                            </span>
                            <span class="sticky-addcart_price--old">
                                <?php echo number_format($product["price"],0," ",".") ?>
                                <?php echo Yii::t('app', 'sum'); ?>
                            </span>
                        </div>
                    <?php endif ?>
                </div>
                <div class="col-auto sticky-addcart_options  prd-block prd-block_info--style1">
                    <div class="select-wrapper">
                        <select class="form-control form-control--sm">
                            <option value="">--Please choose an option--</option>
                        </select>
                    </div>
                </div>
                <div class="col-auto sticky-addcart_actions">
                    <div class="prd-block_qty">
                        <span class="option-label">
                            <?php echo Yii::t("app",'quantity') ?>:
                         </span>
                        <div class="qty qty-changer">
                            <button class="decrease"></button>
                            <input type="number" class="qty-input" value="1" data-min="1" data-max="1000">
                            <button class="increase"></button>
                        </div>
                    </div>
                    <div class="btn-wrap">
                        <button class="btn js-prd-addtocart one" product-id="<?php echo $product->id ?>" > <?php echo Yii::t("app","card") ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--added to cart-->
    <div class="popup-addedtocart js-popupAddToCart closed" data-close="50000">
        <div class="container">
            <div class="row">
                <div class="popup-addedtocart-close js-popupAddToCart-close"><i class="icon-close"></i></div>
                <div class="popup-addedtocart-cart js-open-drop" data-panel="#dropdnMinicart"><i class="icon-basket"></i></div>
                <div class="col-auto popup-addedtocart_logo">
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="/asset/images/logo-white-sm.png" class="lazyload fade-up" alt="">
                </div>
                <div class="col popup-addedtocart_info">
                    <div class="row">
                        <a href="product.html" class="col-auto popup-addedtocart_image">
                            <span class="image-container w-100">
                                <img src="/asset/images/skins/fashion/products/product-01-1.jpg" alt=""/>
                            </span>
                        </a>
                        <div class="col popup-addedtocart_text">
                            <a href="product.html" class="popup-addedtocart_title"></a>
                            <span class="popup-addedtocart_message">Added to <a href="cart.html" class="underline">Cart</a></span>
                            <span class="popup-addedtocart_error_message"></span>
                        </div>
                    </div>
                </div>
                <div class="col-auto popup-addedtocart_actions">
                    <span>You can continue</span> <a href="#" class="btn btn--grey btn--sm js-open-drop" data-panel="#dropdnMinicart"><i class="icon-basket"></i><span>Check Cart</span></a> <span>or</span> <a href="checkout.html" class="btn btn--invert btn--sm"><i class="icon-envelope-1"></i><span>Check out</span></a>
                </div>
            </div>
        </div>
    </div>
    <!--  select options -->
    <div class="sticky-addcart popup-selectoptions js-popupSelectOptions closed" data-close="500000">
        <div class="container">
            <div class="row">
                <div class="popup-selectoptions-close js-popupSelectOptions-close"><i class="icon-close"></i></div>
                <div class="col-auto sticky-addcart_image sticky-addcart_image--zoom">
                    <a>
                        <span class="image-container"><img alt=""/></span>
                    </a>
                </div>
                <div class="col col-sm-5 col-lg-4 col-xl-5 sticky-addcart_info">
                    <h1 class="sticky-addcart_title"><a href="#">&nbsp;</a></h1>
                    <div class="sticky-addcart_price">
                        <span class="sticky-addcart_price--actual"></span>
                        <span class="sticky-addcart_price--old"></span>
                    </div>
                    <div class="sticky-addcart_error_message">Error Message</div>
                </div>
                <div class="col-auto sticky-addcart_options prd-block prd-block_info--style1">
                    <div class="select-wrapper">
                        <select class="form-control form-control--sm sticky-addcart_options_select">
                            <option value="none">Select Option please..</option>
                        </select>
                        <div class="invalid-feedback">Can't be blank</div>
                    </div>
                </div>
                <div class="col-auto sticky-addcart_actions">
                    <div class="prd-block_qty">
                        <span class="option-label">
                            <?php echo Yii::t("app",'quantity') ?>:
                         </span>
                        <div class="qty qty-changer">
                            <button class="decrease"></button>
                            <input type="number" class="qty-input" value="2" data-min="1" data-max="10000">
                            <button class="increase"></button>
                        </div>
                    </div>
                    <div class="btn-wrap">
                        <button class="btn js-prd-addtocart" product-id="<?php echo $product->id ?>"><?php echo Yii::t("app","card") ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- back to top -->
    <a class="back-to-top js-back-to-top compensate-for-scrollbar" href="#" title="Scroll To Top">
        <i class="icon icon-angle-up"></i>
    </a>
    <!-- loader -->
    <div class="loader-horizontal js-loader-horizontal">
        <div class="progress">
            <div class="progress-bar progress-bar-striped progress-bar-animated" style="width: 100%"></div>
        </div>
    </div>
</div>
<!-- payment note -->
