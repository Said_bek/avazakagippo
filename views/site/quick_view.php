<div class="owl-carousel owl_carousel owl-theme owl-loaded owl-drag">
    <?php if (isset($product_imgs) and !empty($product_imgs)): ?>
        <?php $i = 0; ?>
        <?php foreach ($product_imgs as $key => $value): ?>
            <?php if (!empty($multi_detail_id[$i])): ?>
                <?php $id = $multi_detail_id[$i] ?>
            <?php endif ?>
            <div class="item" id="small_carusel_<?php echo $id ?>">
                <a data-px='123456' href="#" data-value="Sariq">
                    <img width="100" src="<?php echo $value ?>" alt="">
                </a>
            </div>
        <?php $i++; ?>
        <?php endforeach ?>    
    <?php endif ?>
</div>
<script>
    $(function() {
        $('.owl_carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            autoplay:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:3
                }
            }
        })
    })
</script>