<div class="owl-carousel owl_carusel owl-theme owl-loaded owl-drag">
    <?php if (isset($product_imgs) and !empty($product_imgs)): ?>
        <?php $i = 0; ?>
        <?php foreach ($product_imgs as $key => $value): ?>
            <div class="item active_item_big_carousel active_b_c_<?php echo $multi_detail_id[$i] ?>">
                <a href="#" data-value="Sariq">
                    <div class="target">

                      <img src="<?php echo $value ?>" alt="Product Image">

                    </div>
                </a>
            </div>
            <?php $i++; ?>
        <?php endforeach ?>    
    <?php endif ?>
</div>
<script>
    $(function() {
        $('.owl_carusel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            autoplay:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        })
    })
</script>
<script>
    $(function(){
        $('.target').izoomify();
    });

</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<script>
    try {
        fetch(new Request("https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js", { method: 'HEAD', mode: 'no-cors' })).then(function(response) {
        return true;
        }).catch(function(e) {
        var carbonScript = document.createElement("script");
        carbonScript.src = "//cdn.carbonads.com/carbon.js?serve=CK7DKKQU&placement=wwwjqueryscriptnet";
        carbonScript.id = "_carbonads_js";
        document.getElementById("carbon-block").appendChild(carbonScript);
        });
    } catch (error) {
        console.log(error);
    }
</script>