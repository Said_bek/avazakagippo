<div class="modal-info-heading">
    <div class="mb-1"><i class="icon-envelope"></i></div>
    <h2><?php echo Yii::t("app","send_review"); ?></h2>
</div>
<form class="comment_form">
    <div class="form-group">
        <input id="customer_name" type="text" name="Comment[name]" class="form-control form-control--sm"
               placeholder="<?php echo Yii::t("app","name") ?>">
        <p class="name_error d-none" style="color:red;"><?php echo Yii::t("app","name_error") ?></p>
    </div>
    <div class="form-group">
        <input id="title_customer" type="text" name="Comment[title]" class="form-control form-control--sm"
               placeholder="<?php echo Yii::t("app","title_review") ?>">
        <input id="product_id" type="hidden" name="Comment[product_id]" value="<?php echo $product_id ?>" class="form-control form-control--sm">
        <p class="title_error d-none" style="color:red;"><?php echo Yii::t("app","title_error") ?></p>
    </div>
    
    <div class="form-group">
        <textarea id="message_customer" class="form-control textarea--height-170" name="Comment[body]" placeholder="<?php echo Yii::t("app","message_review") ?>" required=""></textarea>
        <p class="message_error d-none" style="color:red;"><?php echo Yii::t("app","camment_error") ?></p>
    </div>
    <div class="form-group">
        <i class="icon-star-fill fill" id="star" data-id="1"></i>
        <i class="icon-star-fill fill" id="star" data-id="2"></i>
        <i class="icon-star-fill fill" id="star" data-id="3"></i>
        <i class="icon-star-fill fill" id="star" data-id="4"></i>
        <i class="icon-star-fill fill" id="star" data-id="5"></i>
    </div>
    <a class="btn send_comment"><?php echo Yii::t("app","send_comment") ?></a>
    <p class="p--small mt-15 mb-0"><?php echo Yii::t("app","contacting") ?></p><input
        name="recaptcha-v3-token" type="hidden">
</form>