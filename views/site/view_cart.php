<?php  
	use yii\helpers\Url;
?>
<div class="dropdn-close">
	<span class="js-dropdn-close">
		<?php echo Yii::t("app","close") ?>
	</span>
</div>
<div class="minicart-drop-content js-dropdn-content-scroll">
	

	<?php if (!isset($_SESSION['cart']) || empty($_SESSION['cart'])): ?>
		<div class="minicart-empty js-minicart-empty ">
			<div class="minicart-empty-text">
				<?php echo Yii::t("app","empty_cart") ?>
			</div>
			<div class="minicart-empty-icon">
				<i class="icon-shopping-bag"></i>
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 306 262" style="enable-background:new 0 0 306 262;" xml:space="preserve"><path class="st0" d="M78.1,59.5c0,0-37.3,22-26.7,85s59.7,237,142.7,283s193,56,313-84s21-206-69-240s-249.4-67-309-60C94.6,47.6,78.1,59.5,78.1,59.5z"/></svg>
			</div>
		</div>
	<?php endif ?>
	<?php if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?>
		<?php foreach ($_SESSION['cart'] as $key => $value): ?>
			<div class="minicart-prd row">
				<div class="minicart-prd-image image-hover-scale-circle col">
					<a href="product.html"><img class="lazyload fade-up" src="<?php echo $value['img'] ?>" data-src="<?php echo $value['img'] ?>" alt=""></a>
				</div>
				<div class="minicart-prd-info col">
					<div class="minicart-prd-tag">
						<?php if (Yii::$app->language == 'uz'): ?>
							<?php echo $value['sub_category_uz'] ?>	
						<?php else: ?>
							<?php echo $value['sub_category_ru'] ?>
						<?php endif ?>
						
					</div>
					<h2 class="minicart-prd-name">
						<a href="">
							<?php if (Yii::$app->language == 'uz'): ?>
								<?php echo $value['title_uz'] ?>	
							<?php else: ?>
								<?php echo $value['title_ru'] ?>
							<?php endif ?>
						</a>
					</h2>
					<div class="minicart-prd-qty">
						<span class="minicart-prd-qty-label">
							<?php echo Yii::t("app","quantity") ?>:
						</span>
						<span class="minicart-prd-qty-value">
							<?php echo $value['count'] ?>
						</span>
					</div>
					<div class="minicart-prd-price prd-price flex-column">
						<?php if ($value['sale_price'] != 0): ?>
							<div class="price-old">
								<?php echo price($value['price']) ?>
							</div>
							<div class="price-new">
								<?php echo price($value['sale_price']) ?>
							</div>
						<?php else: ?>
							<div class="price-new">
								<?php echo price($value['price']) ?>
							</div>
						<?php endif ?>
						
					</div>
				</div>
				<div class="minicart-prd-action">
					<a product-id="<?php echo $value['id'] ?>" class="js-product-remove" data-line-number="1"><i class="icon-recycle"></i></a>
				</div>
			</div>
		<?php endforeach ?>
		
	<?php endif ?>
</div>
<div class="minicart-drop-fixed js-hide-empty">
	
	<?php if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?>
		<div class="minicart-drop-info d-none d-md-block">
			<div class="shop-feature-single row no-gutters align-items-center">
				<div class="shop-feature-icon col-auto mr-0"><i class="icon-truck"></i></div>
				<div class="shop-feature-text col">
					<b>
						<?php echo Yii::t("app","shipping") ?>
					</b> 
					<?php echo Yii::t("app","shipping_text") ?>
				</div>
			</div>
		</div>
		<div class="loader-horizontal-sm js-loader-horizontal-sm" data-loader-horizontal=""><span></span></div>
		<div class="minicart-drop-total js-minicart-drop-total row no-gutters align-items-center">
			<div class="minicart-drop-total-txt col-auto heading-font">
				<?php echo Yii::t("app","sub_total") ?>
			</div>
			<div class="minicart-drop-total-price col" data-header-cart-total="">
				<?php echo price($_SESSION['cart_tl_price']) ?>
			</div>
		</div>
		<div class="minicart-drop-actions">
			<a href="<?php echo Url::toRoute('site/cart')?>" class="btn btn--md btn--grey font-weight-bold" style="font-size: 12px;"><i class="icon-basket"></i><span>
				<?php echo Yii::t("app","cart") ?> 
			</span></a>
			<a href="<?php echo Url::toRoute('site/checkout')?>" class="btn btn--md font-weight-bold" style="font-size: 12px;"><i class="icon-checkout"></i><span>
				<?php echo Yii::t("app","checkout") ?> 
			</span></a>
		</div>
		<ul class="payment-link mb-2">
			<li><img width="100px" src="click.png" alt=""></li>
		</ul>
	<?php endif ?>
</div>